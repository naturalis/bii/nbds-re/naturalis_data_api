package nl.naturalis.nba.dao.util;

import static nl.naturalis.nba.common.json.JsonUtil.deserialize;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.common.json.JsonUtil;
import org.junit.Test;

public class NameResolutionQueryHelperTest {

  @Test
  public void processCoLResponse_01() {
    // https://api.catalogue.life/nameusage/search?status=synonym&rank=subspecies&limit=3&q=larus
    Map<String, Object> testData =
        deserialize(
            getClass()
                .getResourceAsStream("NameResolutionQueryHelper__processCoLResponse_01.json"));

    List<ScientificName> expected = new ArrayList<>();
    ScientificName sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("hebridensis");
    sn.setInfraspecificEpithet("larus");
    expected.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("sylvaticus");
    expected.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("hebridensis");
    sn.setInfraspecificEpithet("larus");
    expected.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("sylvaticus");
    expected.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("hebridensis");
    sn.setInfraspecificEpithet("larus");
    expected.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Apodemus");
    sn.setSpecificEpithet("sylvaticus");
    expected.add(sn);

    List<ScientificName> actual = NameResolutionQueryHelper.processCoLResponse(testData);
    System.out.println(JsonUtil.toPrettyJson(actual));
    assertEquals(expected, actual);
  }
}
