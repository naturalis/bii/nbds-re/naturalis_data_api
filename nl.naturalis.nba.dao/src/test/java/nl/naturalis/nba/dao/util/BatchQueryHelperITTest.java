package nl.naturalis.nba.dao.util;

import java.io.InputStream;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import org.junit.BeforeClass;
import org.junit.Test;
import com.fasterxml.jackson.core.type.TypeReference;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DaoTestBase;
import nl.naturalis.nba.dao.SpecimenDao;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BatchQueryHelperITTest extends DaoTestBase {

  @BeforeClass // Will execute after @BeforeClass of DaoTestBase (so we're good)
  public static void beforClass() {

    // Make sure build fails if BatchQueryHelper.URL_ENCODE is true
    if (BatchQueryHelper.URL_ENCODE) {
      throw new RuntimeException("BatchQueryHelper.URL_ENCODE must be false when building");
    }

    /*
     * BatchQueryHelper.json contains 25 specimens with Genus identified as
     * "Bombus": https://api.biodiversitydata.nl/v2/specimen/query/?identifications.
     * defaultClassification.genus=Bombus&_size=25
     */
    InputStream in = BatchQueryHelperTest.class.getResourceAsStream("BatchQueryHelper.json");
    QueryResult<Specimen> result =
        JsonUtil.deserialize(in, new TypeReference<QueryResult<Specimen>>() {});
    Specimen[] specimens = result.stream().map(QueryResultItem::getItem).toArray(Specimen[]::new);
    save(specimens);
  }

  @Test
  public void test01() throws InvalidQueryException {
    QuerySpec qs = new QuerySpec();
    qs.setSize(10);
    SpecimenDao dao = new SpecimenDao();
    BatchQueryHelper<Specimen> helper = new BatchQueryHelper<>(dao);
    BatchQueryResult<Specimen> result = helper.callFirst(qs);
    assertEquals("01", 10, result.size());
    BatchQueryResult<Specimen> res = result;
    assertTrue(res.getToken() != null);
    String token = res.getToken();
    result = helper.callNext(token);
    assertEquals("02", 10, result.size());
    res = result;
    assertTrue(res.getToken() != null);
    token = res.getToken();
    token = URLDecoder.decode(token, StandardCharsets.US_ASCII);
    result = helper.callNext(token);
    assertEquals("03", 5, result.size());
  }

  @Test
  public void test02() throws InvalidQueryException {
    QuerySpec qs = new QuerySpec();
    qs.addCondition(new QueryCondition("sex", "=", "male")); // 11 documents
    qs.setSize(10);
    SpecimenDao dao = new SpecimenDao();
    BatchQueryHelper<Specimen> helper = new BatchQueryHelper<>(dao);
    BatchQueryResult<Specimen> result = helper.callFirst(qs);
    assertEquals("01", 10, result.size());
    assertTrue(result.getToken() != null);
    String token = result.getToken();
    token = URLDecoder.decode(token, StandardCharsets.US_ASCII);
    result = helper.callNext(token);
    assertEquals("02", 1, result.size());
  }

  @Test // Does it work around the boundaries?
  public void test03() throws InvalidQueryException {
    QuerySpec qs = new QuerySpec();
    qs.addCondition(new QueryCondition("sex", "=", "male")); // 11 documents
    qs.setSize(11);
    SpecimenDao dao = new SpecimenDao();
    BatchQueryHelper<Specimen> helper = new BatchQueryHelper<>(dao);
    BatchQueryResult<Specimen> result = helper.callFirst(qs);
    assertEquals("01", 11, result.size());
  }
}
