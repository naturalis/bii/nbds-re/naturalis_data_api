package nl.naturalis.nba.dao.format.calc;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.OffsetDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import org.junit.Before;
import org.junit.Test;

//TODO: Include java documentation
public class ConcatenateCalculatorTest {

  @Before
  public void init() {}

  @Test(expected = CalculatorInitializationException.class)
  public void testCalculateValue_01() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "gatheringEvent.altitude";
    args.put("fields", fields);
    args.put("separator", " ");

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Specimen.class, args);
  }


  @Test(expected = CalculatorInitializationException.class)
  public void testCalculateValue_02() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "|gatheringEvent.altitude";
    args.put("fields", fields);
    args.put("separator", " ");

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Specimen.class, args);
  }

  @Test(expected = CalculatorInitializationException.class)
  public void testCalculateValue_03() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "gatheringEvent.altitude|gatheringEvent.test";
    args.put("fields", fields);
    args.put("separator", " ");

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Specimen.class, args);
  }

  @Test
  public void testCalculateValue_04() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "gatheringEvent.altitude|gatheringEvent.altitudeUnifOfMeasurement|gatheringEvent.depth|gatheringEvent.code";
    args.put("fields", fields);
    args.put("separator", "|");

    Specimen specimen = new Specimen();
    GatheringEvent event = new GatheringEvent();
    event.setAltitude("one");
    event.setDepth("two");
    event.setCode("three");
    specimen.setGatheringEvent(event);
    EntityObject entity = new EntityObject(specimen);

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Specimen.class, args);
    String calculatedStr = calculator.calculateValue(entity);
    String expectedStr = "one|two|three";
    assertEquals("01", expectedStr, calculatedStr);
  }

  @Test
  public void testCalculateValue_05() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "gatheringEvent.altitude|gatheringEvent.altitudeUnifOfMeasurement|gatheringEvent.depth|gatheringEvent.code";
    args.put("fields", fields);
    args.put("separator", "|");

    Specimen specimen = new Specimen();
    GatheringEvent event = new GatheringEvent();
    event.setAltitude("one");
    event.setDepth("two");
    specimen.setGatheringEvent(event);
    EntityObject entity = new EntityObject(specimen);

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Specimen.class, args);
    String calculatedStr = calculator.calculateValue(entity);
    String expectedStr = "one|two";
    assertEquals("01", expectedStr, calculatedStr);
  }


  @Test
  public void testCalculateValue_06() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "modified|serviceAccessPoints.accessUri";
    args.put("fields", fields);
    args.put("separator", " : ");

    MultiMediaObject multiMediaObject = new MultiMediaObject();
    ServiceAccessPoint sap = new ServiceAccessPoint();
    try {
      sap.setAccessUri(new URI("https://en.wikipedia.org/wiki/Uniform_Resource_Identifier"));
    } catch (URISyntaxException e) {
      // ignored
    }
    multiMediaObject.addServiceAccessPoint(sap);
    multiMediaObject.setDateLastEdited(OffsetDateTime.parse("2015-09-15T19:00:38+00:00"));
    EntityObject entity = new EntityObject(multiMediaObject);

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(MultiMediaObject.class, args);
    String calculatedStr = calculator.calculateValue(entity);
    String expectedStr = "2015-09-15T19:00:38Z : https://en.wikipedia.org/wiki/Uniform_Resource_Identifier";
    assertEquals("01", expectedStr, calculatedStr);
  }

  @Test
  public void testCalculateValue_07() throws CalculatorInitializationException {

    Map<String, String> args = new LinkedHashMap<>();
    String fields = "acceptedName.authorshipVerbatim|defaultClassification.domain";
    args.put("fields", fields);
    args.put("separator", ": ");

    Taxon taxon = new Taxon();
    DefaultClassification classification = new DefaultClassification();
    classification.setDomain("Animalia");
    taxon.setDefaultClassification(classification);
    ScientificName scientificName = new ScientificName();
    scientificName.setAuthorshipVerbatim("Foo, 2021");
    taxon.setAcceptedName(scientificName);
    EntityObject entity = new EntityObject(taxon);

    ConcatenateCalculator calculator = new ConcatenateCalculator();
    calculator.initialize(Taxon.class, args);
    String calculatedStr = calculator.calculateValue(entity);
    String expectedStr = "Foo, 2021: Animalia";
    assertEquals("01", expectedStr, calculatedStr);
  }

}
