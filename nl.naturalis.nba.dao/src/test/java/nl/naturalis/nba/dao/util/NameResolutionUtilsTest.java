package nl.naturalis.nba.dao.util;

import static nl.naturalis.nba.dao.util.NameResolutionUtils.createExtraQueryCondition;
import static nl.naturalis.nba.dao.util.NameResolutionUtils.extractNames;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import nl.naturalis.nba.api.NameResolutionRequest;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.QueryCondition;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.QueryResultItem;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.common.json.JsonUtil;
import org.junit.Test;

public class NameResolutionUtilsTest {

  @Test
  public void convertToQuerySpec_01() {
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setNameTypes(Set.of(NameType.SYNONYM, NameType.VERNACULAR_NAME));
    nrr.setSearchString("Malus sylv");
    nrr.setSize(13);

    QuerySpec expected = new QuerySpec();
    QueryCondition c = new QueryCondition("synonyms.fullScientificName", "CONTAINS", "Malus sylv");
    c.or("vernacularNames.name", "CONTAINS", "Malus sylv");
    expected.addCondition(c);
    expected.setSize(13);

    assertEquals(expected, NameResolutionUtils.convertToQuerySpec(nrr));
  }

  @Test
  public void createExtraSpecimenCondition_01() {
    QueryResult<Taxon> res = new QueryResult<>();
    List<QueryResultItem<Taxon>> taxa = new ArrayList<>();
    res.setResultSet(taxa);

    Taxon t = new Taxon();
    taxa.add(new QueryResultItem<Taxon>(t, 0));

    ScientificName sn = new ScientificName();
    sn.setGenusOrMonomial("Foo");
    sn.setSpecificEpithet("bar");
    sn.setInfraspecificEpithet("baz");
    t.setAcceptedName(sn);

    QueryCondition actual = createExtraQueryCondition(extractNames(res));

    QueryCondition expected =
        new QueryCondition("identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Foo")
            .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "bar")
            .and("identifications.scientificName.infraspecificEpithet", "EQUALS_IC", "baz");

    assertNotNull("01", actual.getOr());
    assertEquals("02", 1, actual.getOr().size());
    assertEquals("03", expected, actual.getOr().get(0));
  }

  @Test
  public void createExtraSpecimenCondition_02() {
    QueryResult<Taxon> res = new QueryResult<>();
    List<QueryResultItem<Taxon>> taxa = new ArrayList<>();
    res.setResultSet(taxa);

    Taxon t = new Taxon();
    taxa.add(new QueryResultItem<Taxon>(t, 0));

    ScientificName sn = new ScientificName();
    sn.setGenusOrMonomial("Foo");
    sn.setSpecificEpithet("bar");
    sn.setInfraspecificEpithet("baz");
    t.setAcceptedName(sn);

    sn = new ScientificName();
    sn.setGenusOrMonomial("Larus");
    sn.setSpecificEpithet("fuscus");
    t.setSynonyms(List.of(sn));

    QueryCondition actual = createExtraQueryCondition(extractNames(res));

    Set<QueryCondition> expected =
        Set.of(
            new QueryCondition("identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Foo")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "bar")
                .and("identifications.scientificName.infraspecificEpithet", "EQUALS_IC", "baz"),
            new QueryCondition(
                    "identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Larus")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "fuscus"));

    assertNotNull("01", actual.getOr());
    assertEquals("02", 2, actual.getOr().size());
    assertEquals("03", expected, Set.copyOf(actual.getOr()));
  }

  @Test
  public void createExtraSpecimenCondition_03() {
    QueryResult<Taxon> res = new QueryResult<>();
    List<QueryResultItem<Taxon>> taxa = new ArrayList<>();
    res.setResultSet(taxa);

    Taxon t = new Taxon();
    taxa.add(new QueryResultItem<Taxon>(t, 0));

    ScientificName sn = new ScientificName();
    sn.setGenusOrMonomial("Foo");
    sn.setSpecificEpithet("bar");
    sn.setInfraspecificEpithet("baz");
    t.setAcceptedName(sn);

    List<ScientificName> syns = new ArrayList<>();
    sn = new ScientificName();
    sn.setGenusOrMonomial("Larus");
    sn.setSpecificEpithet("fuscus");
    syns.add(sn);
    sn = new ScientificName();
    sn.setGenusOrMonomial("Malus");
    sn.setSpecificEpithet("sylvestris");
    sn.setInfraspecificEpithet("bar");
    syns.add(sn);

    t.setSynonyms(syns);

    QueryCondition actual = createExtraQueryCondition(extractNames(res));

    Set<QueryCondition> expected =
        Set.of(
            new QueryCondition("identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Foo")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "bar")
                .and("identifications.scientificName.infraspecificEpithet", "EQUALS_IC", "baz"),
            new QueryCondition(
                    "identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Larus")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "fuscus"),
            new QueryCondition(
                    "identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Malus")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "sylvestris")
                .and("identifications.scientificName.infraspecificEpithet", "EQUALS_IC", "bar"));

    System.out.println("EXPECTED: " + JsonUtil.toJson(expected));
    System.out.println("ACTUAL: " + JsonUtil.toJson(actual.getOr()));
    assertNotNull("01", actual.getOr());
    assertEquals("02", 3, actual.getOr().size());
    assertEquals("03", expected, Set.copyOf(actual.getOr()));
  }

  @Test
  public void createExtraSpecimenCondition_04() {
    QueryResult<Taxon> res = new QueryResult<>();
    List<QueryResultItem<Taxon>> taxa = new ArrayList<>();
    res.setResultSet(taxa);

    Taxon t0 = new Taxon();
    taxa.add(new QueryResultItem<Taxon>(t0, 0));

    Taxon t1 = new Taxon();
    taxa.add(new QueryResultItem<Taxon>(t1, 0));

    ScientificName sn = new ScientificName();
    sn.setGenusOrMonomial("Foo");
    sn.setSpecificEpithet("bar");
    sn.setInfraspecificEpithet("baz");
    t0.setAcceptedName(sn);

    sn = new ScientificName();
    sn.setGenusOrMonomial("Larus");
    sn.setSpecificEpithet("fuscus");
    t1.setAcceptedName(sn);

    QueryCondition actual = createExtraQueryCondition(extractNames(res));

    Set<QueryCondition> expected =
        Set.of(
            new QueryCondition("identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Foo")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "bar")
                .and("identifications.scientificName.infraspecificEpithet", "EQUALS_IC", "baz"),
            new QueryCondition(
                    "identifications.scientificName.genusOrMonomial", "EQUALS_IC", "Larus")
                .and("identifications.scientificName.specificEpithet", "EQUALS_IC", "fuscus"));

    assertNotNull("01", actual.getOr());
    assertEquals("02", 2, actual.getOr().size());
    assertEquals("03", expected, Set.copyOf(actual.getOr()));
  }
}
