package nl.naturalis.nba.dao;

import static nl.naturalis.nba.dao.DaoTestUtil.containsId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.LogicalOperator;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.NameResolutionRequest;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.QueryCondition;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.SpecimenIdentification;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.api.model.VernacularName;
import nl.naturalis.nba.common.json.JsonUtil;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("NewClassNamingConvention")
public class SpecimenDao_NameResolutionTest extends DaoTestBase {

  @Before
  public void before() {
    indexTestData();
  }

  @Test
  public void test_01() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    query.setNameResolutionRequest(nrr);
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    assertEquals("01", 2, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "1"));
    assertTrue("03", containsId(result, "2"));
  }

  @Test
  public void test_02() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("FOO");
    nrr.setNameTypes(Set.of(NameType.ACCEPTED_NAME));
    query.setNameResolutionRequest(nrr);
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    assertEquals("01", 0, result.getTotalSize().intValue());
  }

  @Test
  public void test_03() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("sapien");
    query.setNameResolutionRequest(nrr);
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    System.out.println(JsonUtil.toPrettyJson(result));
    assertEquals("01", 3, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "3"));
    assertTrue("03", containsId(result, "4"));
    assertTrue("03", containsId(result, "5"));
  }

  @Test
  public void test_04() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("sapien");
    query.setNameResolutionRequest(nrr);
    // Now also add a condition for the specimen index itself
    query.addCondition(
        new QueryCondition(
            "identifications.scientificName.fullScientificName", "CONTAINS", "argentatus"));
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    System.out.println(JsonUtil.toPrettyJson(result));
    assertEquals("01", 2, result.getTotalSize().intValue());
    assertTrue("02", containsId(result, "3"));
    assertTrue("02", containsId(result, "4"));
  }

  @Test(expected = InvalidQueryException.class)
  public void test_05() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("sapien");
    query.setNameResolutionRequest(nrr);
    query.addCondition(
        new QueryCondition(
            "identifications.scientificName.fullScientificName", "CONTAINS", "argentatus"));
    query.addCondition(
        new QueryCondition("identifications.scientificName.genusOrMonomials", "CONTAINS", "Oops"));
    SpecimenDao dao = new SpecimenDao();
    dao.queryWithNameResolution(query);
  }

  @Test
  public void test_06() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("sapien");
    query.setNameResolutionRequest(nrr);
    query.setLogicalOperator(LogicalOperator.AND);
    query.addCondition(
        new QueryCondition(
            "identifications.scientificName.fullScientificName", "CONTAINS", "argentatus"));
    query.addCondition(
        new QueryCondition("identifications.scientificName.genusOrMonomial", "CONTAINS", "Oops"));
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    assertEquals("01", 0, result.getTotalSize().intValue());
  }

  @Test // Make sure higher taxa do not contribute to the result
  public void test_07() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Aves"); // present but ignored!
    query.setNameResolutionRequest(nrr);
    // If it's up to the plain specimen query, everything should come back
    query.addCondition(new QueryCondition(true));
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    System.out.println(JsonUtil.toPrettyJson(result));
    assertEquals("01", 0, result.getTotalSize().intValue());
  }

  @Test // Make sure higher taxa do not contribute to the result
  public void test_08() throws InvalidQueryException {
    NameResolutionQuerySpec query = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Desmodus rotondus");
    nrr.setUseCoL(true);
    Set<NameType> nameTypes = Stream.of(
            NameType.ACCEPTED_NAME,
            NameType.SYNONYM
    ).collect(Collectors.toCollection(HashSet::new));
    nrr.setNameTypes(nameTypes);

    query.setNameResolutionRequest(nrr);
    SpecimenDao dao = new SpecimenDao();
    QueryResult<Specimen> result = dao.queryWithNameResolution(query);
    System.out.println(JsonUtil.toPrettyJson(result));
    //assertEquals("01", 1, result.getTotalSize().intValue());
  }


  public void indexTestData() {

    /* ***************** Scientific names ***************** */

    ScientificName malusSylvestris = new ScientificName();
    malusSylvestris.setFullScientificName("Malus sylvestris");
    malusSylvestris.setGenusOrMonomial("Malus");
    malusSylvestris.setSpecificEpithet("sylvestris");

    ScientificName larusFuscusArgentatus = new ScientificName();
    larusFuscusArgentatus.setFullScientificName("Larus fuscus argentatus");
    larusFuscusArgentatus.setGenusOrMonomial("Larus");
    larusFuscusArgentatus.setSpecificEpithet("fuscus");
    larusFuscusArgentatus.setInfraspecificEpithet("argentatus");

    ScientificName homoSapiensSapiens = new ScientificName();
    homoSapiensSapiens.setFullScientificName("Homo sapiens sapiens");
    homoSapiensSapiens.setGenusOrMonomial("Homo");
    homoSapiensSapiens.setSpecificEpithet("sapiens");
    homoSapiensSapiens.setInfraspecificEpithet("sapiens");

    ScientificName desmodusRotondus = new ScientificName();
    desmodusRotondus.setFullScientificName("Desmodus rotondus");
    desmodusRotondus.setGenusOrMonomial("Desmodus");
    desmodusRotondus.setSpecificEpithet("rotondus");

    ScientificName aves = new ScientificName();
    aves.setFullScientificName("Aves");
    aves.setGenusOrMonomial("Aves");

    ScientificName fooBar = new ScientificName();
    fooBar.setFullScientificName("Foo bar");
    fooBar.setGenusOrMonomial("Foo");
    fooBar.setSpecificEpithet("bar");

    /* ***************** Vernacular names ***************** */

    VernacularName seagull = new VernacularName();
    seagull.setName("Sea gull");

    VernacularName zeemeeuw = new VernacularName();
    zeemeeuw.setName("Zeemeeuw");

    VernacularName mens = new VernacularName();
    mens.setName("mens");

    VernacularName appleTree = new VernacularName();
    appleTree.setName("Apple tree");

    VernacularName foo = new VernacularName();
    foo.setName("foo");

    VernacularName bird = new VernacularName();
    bird.setName("bird");

    /* ***************** Specimens ***************** */

    // Malus sylvestris + Larus fuscus argentatus
    Specimen s = new Specimen();
    s.setId("1");
    SpecimenIdentification si1 = new SpecimenIdentification();
    SpecimenIdentification si2 = new SpecimenIdentification();
    SpecimenIdentification si3;
    s.setIdentifications(List.of(si1, si2));
    si1.setScientificName(malusSylvestris);
    si2.setScientificName(larusFuscusArgentatus);
    save(s);

    // Malus sylvestris
    s.setId("2");
    si1 = new SpecimenIdentification();
    s.setIdentifications(List.of(si1));
    si1.setScientificName(malusSylvestris);
    save(s);

    // Larus fuscus argentatus + Homo sapiens sapiens
    s.setId("3");
    si1 = new SpecimenIdentification();
    si2 = new SpecimenIdentification();
    si3 = new SpecimenIdentification();
    s.setIdentifications(List.of(si1, si2));
    si1.setScientificName(larusFuscusArgentatus);
    si2.setScientificName(homoSapiensSapiens);
    si3.setScientificName(desmodusRotondus);
    save(s);

    // No identifications at all
    s.setId("4");
    save(s);

    // Homo sapiens sapiens
    s.setId("5");
    si1 = new SpecimenIdentification();
    s.setIdentifications(List.of(si1));
    si1.setScientificName(homoSapiensSapiens);
    save(s);

    /* ***************** Taxa ***************** */

    Taxon t = new Taxon();
    t.setSourceSystem(SourceSystem.NSR);
    t.setId("A");
    t.setAcceptedName(malusSylvestris);
    t.setSynonyms(List.of(fooBar));
    t.setVernacularNames(List.of(foo, appleTree));
    save(t);

    t = new Taxon();
    t.setSourceSystem(SourceSystem.COL);
    t.setId("B");
    t.setAcceptedName(malusSylvestris);
    t.setSynonyms(List.of(fooBar));
    t.setVernacularNames(List.of(appleTree));
    save(t);

    t = new Taxon();
    t.setId("C");
    t.setAcceptedName(homoSapiensSapiens);
    t.setVernacularNames(List.of(mens));
    save(t);

    t = new Taxon();
    t.setSourceSystem(SourceSystem.NSR);
    t.setId("D");
    t.setAcceptedName(larusFuscusArgentatus);
    t.setVernacularNames(List.of(seagull, zeemeeuw));
    save(t);

    t = new Taxon();
    t.setSourceSystem(SourceSystem.COL);
    t.setId("E");
    t.setAcceptedName(larusFuscusArgentatus);
    save(t);

    t = new Taxon();
    t.setId("F");
    t.setAcceptedName(aves);
    t.setVernacularNames(List.of(bird, foo));
    save(t);
  }
}
