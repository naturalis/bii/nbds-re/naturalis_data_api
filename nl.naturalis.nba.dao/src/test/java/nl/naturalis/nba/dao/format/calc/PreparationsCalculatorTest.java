package nl.naturalis.nba.dao.format.calc;

import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PreparationsCalculatorTest {

    private static final PreparationsCalculator calculator = new PreparationsCalculator();

    static {
        try {
            calculator.initialize(Specimen.class, null);
        } catch (CalculatorInitializationException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    @Before
    public void init() {}

    @Test
    public void testCalculatorTest_01() {

        Specimen specimen = new Specimen();
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "";
        assertEquals("01", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_02() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("aaa bbb");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "aaa bbb";
        assertEquals("02", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_03() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Not applicable");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.NOT_APPLICABLE;
        assertEquals("03", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_04() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Unknown");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.UNKNOWN;
        assertEquals("04", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_05() {

        Specimen specimen = new Specimen();
        specimen.setPreparationType("ccc ddd");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "(ccc ddd)";
        assertEquals("05", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_06() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("aaa bbb");
        specimen.setPreparationType("ccc ddd");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "aaa bbb (ccc ddd)";
        assertEquals("06", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_07() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Not Applicable");
        specimen.setPreparationType("ccc ddd");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "(ccc ddd)";
        assertEquals("07", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_08() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Unknown");
        specimen.setPreparationType("ccc ddd");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "(ccc ddd)";
        assertEquals("08", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_09() {

        Specimen specimen = new Specimen();
        specimen.setPreparationType("Not Applicable");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.NOT_APPLICABLE;
        assertEquals("09", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_10() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("aaa bbb");
        specimen.setPreparationType("Not Applicable");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "aaa bbb";
        assertEquals("10", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_11() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Not Applicable");
        specimen.setPreparationType("Not Applicable");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.NOT_APPLICABLE;
        assertEquals("11", expectedStr, calculatedStr);
    }
    @Test
    public void testCalculatorTest_12() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Unknown");
        specimen.setPreparationType("Not Applicable");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.UNKNOWN;
        assertEquals("12", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_13() {

        Specimen specimen = new Specimen();
        specimen.setPreparationType("Unknown");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.UNKNOWN;
        assertEquals("13", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_14() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("aaa bbb");
        specimen.setPreparationType("unknown");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = "aaa bbb";
        assertEquals("14", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_15() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Not Applicable");
        specimen.setPreparationType("Unknown");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.UNKNOWN;
        assertEquals("15", expectedStr, calculatedStr);
    }

    @Test
    public void testCalculatorTest_16() {

        Specimen specimen = new Specimen();
        specimen.setKindOfUnit("Unknown");
        specimen.setPreparationType("unknown");
        EntityObject entity = new EntityObject(specimen);

        String calculatedStr = calculator.calculateValue(entity);
        String expectedStr = PreparationsCalculator.UNKNOWN;
        assertEquals("16", expectedStr, calculatedStr);
    }

}
