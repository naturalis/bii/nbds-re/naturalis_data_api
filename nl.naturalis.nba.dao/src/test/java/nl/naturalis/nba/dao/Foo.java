package nl.naturalis.nba.dao;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.net.URLEncoder;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.NameResolutionRequest;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.NameResolutionRequest.SearchType;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.QueryCondition;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.common.json.JsonUtil;

public class Foo {

  public static void main(String[] args) {
    // nameResolution();
    batchQuery();
  }

  private static void batchQuery() {
    QuerySpec qs = new QuerySpec();
    qs.setFields(
        List.of("unitID", "unitGUID", "sourceSystem.name", "collectionType")
            .stream()
            .map(Path::new)
            .collect(Collectors.toList()));
    qs.addCondition(
        new QueryCondition("sourceSystem.code", "=", "BRAHMS")
            .or("sourceSystem.code", "=", "NSR")
            .or("collectionType", "EQUALS_IC", "AVES"));
    qs.setSize(10);
    String urlEncoded = URLEncoder.encode(JsonUtil.toJson(qs), UTF_8);
    System.out.println("http://localhost:8080/v2/specimen/batchQuery/?_querySpec=" + urlEncoded);
  }

  @SuppressWarnings("unused")
  private static void nameResolution() {
    NameResolutionQuerySpec qs = new NameResolutionQuerySpec();
    NameResolutionRequest nrr = new NameResolutionRequest();
    nrr.setSearchString("Larus fuscus");
    nrr.setSearchType(SearchType.EXACT);
    nrr.setNameTypes(EnumSet.allOf(NameType.class));
    nrr.setUseCoL(true);
    qs.setNameResolutionRequest(nrr);
    String urlEncoded = URLEncoder.encode(JsonUtil.toJson(qs), UTF_8);
    System.out.println(
        "http://localhost:8080/v2/specimen/queryWithNameResolution/?__explain&__level=debug&_querySpec="
            + urlEncoded);
  }
}
