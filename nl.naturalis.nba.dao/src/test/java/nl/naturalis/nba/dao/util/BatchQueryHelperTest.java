package nl.naturalis.nba.dao.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import nl.naturalis.nba.api.QueryCondition;
import org.junit.Test;

public class BatchQueryHelperTest {

  @Test
  public void createAndParseToken() throws IOException {
    BatchQuerySpec in = new BatchQuerySpec();

    in.addFields("unitID", "unitGUID", "identifications.scientificName.fullScientificName");

    QueryCondition qc =
        new QueryCondition("identifications.defaultClassification.rank", "=", "Family");
    qc.or("identifications.defaultClassification.rank", "=", "Genus");
    in.addCondition(qc);

    qc = new QueryCondition("sourceSystem.code", "=", "BRAHMS");
    in.addCondition(qc);

    qc =
        new QueryCondition(
            "theme", "IN", new String[] {"Sybold", "Sobold", "Aves", "Cholaeoptera"});
    in.addCondition(qc);

    in.setSize(1200);

    in.setSearchAfter(new Object[] {"1234567"});

    String token = BatchQueryHelper.createResumptionToken(in);

    BatchQuerySpec out = BatchQueryHelper.parseResumptionToken(token);

    // System.out.println(JsonUtil.toJson(in));
    // System.out.println(JsonUtil.toJson(out));

    assertEquals(in, out);
  }
}
