package nl.naturalis.nba.dao.util;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.Logger;
import com.google.common.annotations.VisibleForTesting;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.check.Check;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.NameResolutionRequest.NameType;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.NbaDao;
import nl.naturalis.nba.dao.TaxonDao;
import nl.naturalis.nba.dao.exception.DaoException;
import static java.util.stream.Collectors.joining;
import static nl.naturalis.common.ObjectMethods.ifEmpty;
import static nl.naturalis.common.check.CommonChecks.notEmpty;
import static nl.naturalis.nba.api.LogicalOperator.AND;
import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.dao.util.NameResolutionUtils.convertToColPlusRequest;
import static nl.naturalis.nba.dao.util.NameResolutionUtils.convertToQuerySpec;
import static nl.naturalis.nba.dao.util.NameResolutionUtils.createExtraQueryCondition;
import static nl.naturalis.nba.dao.util.NameResolutionUtils.extractNames;

/**
 * Class reponsible for the actual execution of the name resolution services.
 *
 * @author Ayco Holleman
 */
public class NameResolutionQueryHelper {

  static final String ERR_COLPLUS_CALL_FAILED = "Error while calling ColPlus";

  private static final Logger logger = getLogger(NameResolutionQueryHelper.class);

  private final NameResolutionQuerySpec query;
  private final NbaDao<?> dao;

  public NameResolutionQueryHelper(NbaDao<?> dao, NameResolutionQuerySpec query) {
    this.dao = Check.notNull(dao, "dao").ok();
    this.query = Check.notNull(query, "query").ok();
  }

  @SuppressWarnings("unchecked")
  public <T extends IDocumentObject> QueryResult<T> executeQuery() throws InvalidQueryException {
    NameResolutionRequest nrr = query.getNameResolutionRequest();
    Check.notNull(InvalidQueryException::new, nrr, "nameResolutionRequest")
        .has(NameResolutionRequest::getSearchString, "searchString", notEmpty());
    // We are going to add an extra condition to the original QuerySpec and it
    // *must* be AND-ed to any pre-existing conditions on the QuerySpec
    if (query.getConditions() != null
        && query.getConditions().size() > 1
        && query.getLogicalOperator() != AND) {
      throw new InvalidQueryException(
          "Name resolution query requires logical operator to be set to AND");
    }
    if (logger.isDebugEnabled()) {
      String search = nrr.getSearchString();
      String types =
          ifEmpty(nrr.getNameTypes(), EnumSet.allOf(NameType.class))
              .stream()
              .map(s -> s + "s")
              .collect(joining(", "));
      logger.debug("Resolving \"{}\" against {}", search, types);
    }
    List<ScientificName> names = query.getNameResolutionRequest().useCoL() ? callCoL() : callNba();
    if (names.isEmpty()) {
      return new QueryResult<>();
    }
    if (logger.isDebugEnabled()) {
      String search = nrr.getSearchString();
      String index = dao.getDocumentType().getIndexInfo().getName();
      logger.debug("Found {} name(s) matching \"{}\"", names.size(), search);
      logger.debug("Converting names into extra query condition for {} index", index);
    }
    QueryCondition extraCondition = createExtraQueryCondition(names);
    if (extraCondition.getOr() != null && extraCondition.getOr().size() >= 1023) {
      // indices.query.bool.max_clause_count is 1024
      String fmt =
          "Too many names matching \"%s\". Decrease nameResolutionRequest.size and try again";
      String msg = String.format(fmt, nrr.getSearchString());
      throw new InvalidQueryException(msg);
    }
    extraCondition.setConstantScore(true);
    QuerySpec enrichedQuery = new QuerySpec(query);
    enrichedQuery.setLogicalOperator(AND);
    logger.debug("Adding extra query condition to original query");
    enrichedQuery.addCondition(extraCondition);
    if (logger.isDebugEnabled()) {
      String index = dao.getDocumentType().getIndexInfo().getName();
      logger.debug("Executing enriched query against {} index", index);
    }
    return (QueryResult<T>) dao.query(enrichedQuery);
  }

  private List<ScientificName> callNba() throws InvalidQueryException {
    QuerySpec taxonQuery = convertToQuerySpec(query.getNameResolutionRequest());
    logger.debug("Querying taxon index");
    QueryResult<Taxon> result = new TaxonDao().query(taxonQuery);
    logger.debug("Processing query result");
    return extractNames(result);
  }

  @VisibleForTesting
  List<ScientificName> callCoL() {
    try {
      URI uri = convertToColPlusRequest(query.getNameResolutionRequest());
      if (logger.isDebugEnabled()) {
        logger.debug("Calling Catalogue of Life: {}", uri);
      }
      Map<String, Object> response = getCoLResponse(uri);
      logger.debug("Processing response from Catalogue of Life");
      return processCoLResponse(response);
    } catch (Exception e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

  private static Map<String, Object> getCoLResponse(URI uri) {
    try (CloseableHttpClient client = HttpClients.createDefault()) {
      HttpGet request = new HttpGet(uri);
      HttpResponse response = client.execute(request);
      if (response.getStatusLine().getStatusCode() == 200) {
        Map<String, Object> data = JsonUtil.deserialize(response.getEntity().getContent());
        return data;
      }
      throw new DaoException(
          ERR_COLPLUS_CALL_FAILED + ": " + response.getStatusLine().getReasonPhrase());
    } catch (IOException e) {
      throw new DaoException(ERR_COLPLUS_CALL_FAILED, e);
    }
  }

  /*
   * A me-know-nothing way of processing the response coming back from ColPlus:
   * any Name object in it is added to the list of scientific names to search for
   * in the specimen index. The more names the better. The risk of false positives
   * seems rather small.
   */
  @VisibleForTesting
  @SuppressWarnings("unchecked")
  static List<ScientificName> processCoLResponse(Map<String, Object> response) {
    List<ScientificName> names = new ArrayList<>();
    List<Map<String, Object>> result = (List<Map<String, Object>>) response.get("result");
    if (result != null) {
      result.forEach(
          e -> {
            Map<String, Object> usage = (Map<String, Object>) e.get("usage");
            if (usage != null) {
              Map<String, Object> name = (Map<String, Object>) usage.get("name");
              addCoLName(names, name);
              Map<String, Object> accepted = (Map<String, Object>) usage.get("accepted");
              if (accepted != null) {
                name = (Map<String, Object>) accepted.get("name");
                addCoLName(names, name);
              }
            }
          });
    }
    return names;
  }

  private static void addCoLName(List<ScientificName> result, Map<String, Object> name) {
    String genus = (String) name.get("genus");
    String species = (String) name.get("specificEpithet");
    if (genus != null && species != null) {
      ScientificName sn = new ScientificName();
      result.add(sn);
      sn.setGenusOrMonomial(genus);
      sn.setSpecificEpithet(species);
      String infra = (String) name.get("infraspecificEpithet");
      if (infra != null) {
        sn.setInfraspecificEpithet(infra);
      }
    }
  }
}
