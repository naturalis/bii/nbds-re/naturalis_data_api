package nl.naturalis.nba.dao.format.dwca;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import nl.naturalis.nba.api.NoSuchDataSetException;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.format.DataSetConfigurationException;
import nl.naturalis.nba.dao.format.DataSetWriteException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GenerateDwcaCache {

  private static final Logger logger = LogManager.getLogger(GenerateDwcaCache.class);
  private static final String DWCA_DIR = "/data/nba/";

  /**
   * This utility class can be used to create a DwC Archive
   * from a data set ready to be ingested.
   *
   * Usage:
   * - download the data set from the Colander server (e.g. from /data/infuser/archive/files
   * - run nl.naturalis.nba.dao.format.dwca.GenerateDwcaCache <set name> <entity>
   *
   * E.g.:
   * - run nl.naturalis.nba.dao.format.dwca.GenerateDwcaCache botany specimen
   * - run nl.naturalis.nba.dao.format.dwca.GenerateDwcaCache observations specimen
   *
   * NOTE:
   * - put the files (specimen and multimedia, or taxon and multimedia) ending with `.jsonl`
   *   in `/data/nba`
   * - make sure to update the DWCA config (`git pull`). Check the location in nba.properties (`nba.api.install.dir=/some/path/`)
   *
   */
  public static void main(String[] args) throws FileNotFoundException {

    String dataSetName = args[0];
    DwcaDataSetType dataSetType = DwcaDataSetType.valueOf(args[1].toUpperCase());
    logger.info("Creating DwC Archive file for data set {} of type {}", dataSetName, dataSetType);
    try {
      DwcaConfig dwcaConfig = new DwcaConfig(dataSetName, dataSetType);
      String path = DWCA_DIR.concat(dataSetName.toLowerCase()).concat(".dwca.zip");
      FileOutputStream fos = new FileOutputStream(path);
      JsonDataSourceDwcaWriter writer = new JsonDataSourceDwcaWriter(dwcaConfig, fos);
      writer.writeDwcaForDataSet();

    } catch (DataSetConfigurationException | NoSuchDataSetException | DataSetWriteException e) {
      throw new DaoException(e);
    }





  }
}
