package nl.naturalis.nba.dao.translate;

import static nl.naturalis.common.ObjectMethods.doIfNotNull;
import static nl.naturalis.nba.api.LogicalOperator.OR;
import static nl.naturalis.nba.common.json.JsonUtil.toPrettyJson;
import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.dao.DaoUtil.prune;
import static nl.naturalis.nba.dao.translate.ConditionTranslatorFactory.getTranslator;
import static nl.naturalis.nba.dao.util.es.ESUtil.newCountRequest;
import static nl.naturalis.nba.dao.util.es.ESUtil.newSearchRequest;
import static nl.naturalis.nba.utils.CollectionUtil.hasElements;
import static org.elasticsearch.index.query.QueryBuilders.constantScoreQuery;

import java.util.ArrayList;
import java.util.List;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.NoSuchFieldException;
import nl.naturalis.nba.common.es.map.MappingInfo;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.util.BatchQuerySpec;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import static nl.naturalis.common.ObjectMethods.*;
import static nl.naturalis.common.NumberMethods.*;

/**
 * A {@code QuerySpecTranslator} translates a {@link QuerySpec} object into an Elasticsearch {@link
 * SearchRequestBuilder query}.
 *
 * @author Ayco Holleman
 */
public class QuerySpecTranslator {

  private static final Logger logger = getLogger(QuerySpecTranslator.class);

  private QuerySpec spec;
  private DocumentType<?> dt;

  /**
   * Creates a translator for the specified {@link QuerySpec} object generating a query for the
   * specified document type.
   *
   * @param querySpec
   * @param documentType
   */
  public QuerySpecTranslator(QuerySpec querySpec, DocumentType<?> documentType) {
    this.spec = querySpec;
    this.dt = documentType;
  }

  /**
   * Translates the {@link QuerySpec} object into an Elasticsearch query.
   *
   * @param fetchSource
   * @return
   * @throws InvalidQueryException
   */
  public SearchRequest translate() throws InvalidQueryException {
    if (logger.isTraceEnabled()) {
      logger.trace("Translating QuerySpec:\n{}", toPrettyJson(prune(spec)));
    }
    SearchRequest request = newSearchRequest(dt);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    if (spec.getConditions() != null && !spec.getConditions().isEmpty()) {
      overrideNonScoringIfNecessary();
      QueryBuilder query;
      if (spec.isConstantScore()) {
        query = constantScoreQuery(translateConditions());
      } else {
        query = translateConditions();
      }
      searchSourceBuilder.query(query);
    }

    /*
     * NB If fields is not null, then just show all fields. If it is a non-empty
     * list, then only show the fields in the list. If it is en ampty list (or the
     * list only contains "id", then don't show any fields at all.
     */
    if (spec.getFields() != null) {
      String[] includes = getIncludedFields();
      if (includes.length == 0) {
        searchSourceBuilder.fetchSource(false);
      } else {
        searchSourceBuilder.fetchSource(includes, null);
      }
    }

    searchSourceBuilder.from(n2e(spec.getFrom()));
    searchSourceBuilder.size(ifNull(spec.getSize(), QuerySpec.DEFAULT_SIZE));

    if (spec.getSortFields() != null) {
      SortFieldsTranslator sfTranslator = new SortFieldsTranslator(spec, dt);
      for (SortBuilder<?> sortBuilder : sfTranslator.translate()) {
        searchSourceBuilder.sort(sortBuilder);
      }
    }

    if (spec instanceof BatchQuerySpec) {
      searchSourceBuilder.trackTotalHits(false);
      BatchQuerySpec batchQuery = (BatchQuerySpec) spec;
      doIfNotNull(batchQuery.getSearchAfter(), searchSourceBuilder::searchAfter);
    } else {
      searchSourceBuilder.trackTotalHits(true);
    }

    request.source(searchSourceBuilder);
    return request;
  }

  /**
   * A CountRequest does not allow "from" and "size". It only use the conditions of the query.
   *
   * @return CountRequest
   * @throws InvalidConditionException
   */
  public CountRequest translateCountRequest() throws InvalidConditionException {
    if (logger.isDebugEnabled() && spec != null) {
      logger.debug("Translating QuerySpec to CountRequest:\n{}", toPrettyJson(prune(spec)));
    }
    CountRequest request = newCountRequest(dt);
    if (spec != null && spec.getConditions() != null && !spec.getConditions().isEmpty()) {
      QueryBuilder query = translateConditions();
      request.query(query);
    }
    return request;
  }

  private String[] getIncludedFields() throws InvalidQueryException {
    MappingInfo<?> mappingInfo = new MappingInfo<>(dt.getMapping());
    List<String> includes = new ArrayList<>(spec.getFields().size());
    for (Path field : spec.getFields()) {
      if (!field.toString().equals("id")) {
        /*
         * "id" is a special field that can be used to retrieve the Elasticsearch
         * document ID, which is not part of the document itself. But it _is_ an allowed
         * field, populated through SearchHit.getId() rather than through document data.
         */
        try {
          mappingInfo.getField(field);
        } catch (NoSuchFieldException e) {
          throw new InvalidQueryException(e.getMessage());
        }
        includes.add(field.toString());
      }
    }
    return includes.toArray(new String[includes.size()]);
  }

  private QueryBuilder translateConditions() throws InvalidConditionException {
    List<QueryCondition> conditions = spec.getConditions();
    if (conditions.size() == 1) {
      QueryCondition condition = conditions.iterator().next();
      // Check for a single condition
      if (!hasElements(condition.getOr()) && !hasElements(condition.getAnd())) {
        return getTranslator(condition, dt).singleCondition().translate();
      }
      return getTranslator(condition, dt).translate();
    } else if (spec.getLogicalOperator() == OR) {
      QueryCondition qc = new QueryCondition(false);
      for (QueryCondition c : conditions) {
        qc.or(c);
      }
      if (logger.isDebugEnabled()) {
        logger.debug("Rewritten query to:\n" + JsonUtil.toPrettyJson(qc));
      }
      return getTranslator(qc, dt).translate();
    } else {
      QueryCondition qc = new QueryCondition(true);
      for (QueryCondition c : conditions) {
        qc.and(c);
      }
      if (logger.isDebugEnabled()) {
        logger.debug("Rewritten query to:\n" + JsonUtil.toPrettyJson(qc));
      }
      return getTranslator(qc, dt).translate();
    }
  }

  /*
   * This will set the nonScoring field of individual conditions within a
   * QuerySpec to false if the QuerySpec as a whole is non-scoring or if the
   * condition is negated. If we are dealing with a non-scoring search the
   * Elasticsearch query generated from all conditions together is wrapped into
   * one big constant_score query. The queries generated from the individual
   * conditions should then not also be wrapped into a constant_score query.
   * Negated conditions are intrinsically non-scoring, so do not need to be
   * wrapped into a constant_score query.
   */
  private void overrideNonScoringIfNecessary() {
    if (spec.isConstantScore()) {
      for (QueryCondition c : spec.getConditions()) {
        resetToScoring(c);
      }
    }
  }

  private static void resetToScoring(QueryCondition condition) {
    if (condition.isConstantScore()) {
      condition.setConstantScore(false);
      if (logger.isDebugEnabled()) {
        String field = condition.getField().toString();
        String msg =
            "constantScore field for Condition on field {} "
                + "reset to false because one of its ancestors "
                + "already has its constantScore attribute set to true";
        logger.debug(msg, field);
      }
    }
    if (condition.getAnd() != null) {
      for (QueryCondition c : condition.getAnd()) {
        resetToScoring(c);
      }
    }
    if (condition.getOr() != null) {
      for (QueryCondition c : condition.getOr()) {
        resetToScoring(c);
      }
    }
  }
}
