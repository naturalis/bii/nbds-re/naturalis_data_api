package nl.naturalis.nba.dao.aggregation;

import java.util.List;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.api.SortField;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.dao.DocumentType;

/**
 * Abstract class representing aggregation queries on a specific field
 */
public abstract class GetDistinctValuesAggregation<T extends IDocumentObject, U>
    extends AggregationQuery<T, U> {

  String field;
  int aggSize;
  int from;

  GetDistinctValuesAggregation(DocumentType<T> dt, String field, QuerySpec querySpec)
      throws InvalidQueryException {
    super(dt, querySpec);
    this.field = field;
    validateSortFields();
  }

  void validateSortFields() throws InvalidQueryException {
    if (querySpec != null) {
      List<SortField> sortFields = querySpec.getSortFields();
      if (sortFields == null) return; // no sorting
      if (sortFields.size() > 1) {
        // too much sort fields; just one is allowed
        throw new InvalidQueryException(
            String.format(
                "When aggregating on field '%s', you can only sort on this field, not on any other",
                field));
      }
      String sortField = sortFields.get(0).getPath().toString();
      if (!field.equals(sortField)) {
        throw new InvalidQueryException(
            String.format(
                "When aggregating on field '%s, you cannot sort on field '%s' '",
                field, sortField));
      }
    }
  }
}
