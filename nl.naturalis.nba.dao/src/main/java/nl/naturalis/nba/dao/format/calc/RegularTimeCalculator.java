package nl.naturalis.nba.dao.format.calc;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import nl.naturalis.nba.api.NoSuchFieldException;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.InvalidPathException;
import nl.naturalis.nba.common.PathUtil;
import nl.naturalis.nba.common.PathValueReader;
import nl.naturalis.nba.common.es.map.MappingFactory;
import nl.naturalis.nba.common.es.map.MappingInfo;
import nl.naturalis.nba.dao.format.CalculationException;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * RegularTimeCalculator is a calculator that extracts the time from a any DateTime field
 * and returns it in the format HH:mm. The DateTime field to be examined needs to passed
 * as a parameter in the config file.
 *
 * <p>NOTE: since the DateTime field may be part of a nested document, this calculator
 * will only consider the given field in the first nested document. Values in any
 * other nested documents will be ignored.
 */
public class RegularTimeCalculator implements ICalculator {

  private PathValueReader pathValueReader;

  private static final Logger logger = LogManager.getLogger(RegularTimeCalculator.class);

  @SuppressWarnings("DuplicatedCode")
  @Override
  public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
      throws CalculatorInitializationException {

    String dateTimeField = args.get(null);
    Path path = new Path(getPathToField(docType, dateTimeField));
    try {
      PathUtil.validate(path, MappingFactory.getMapping(docType));
    } catch (InvalidPathException e) {
      String msg = format("Entity %s: %s", dateTimeField, e.getMessage());
      throw new CalculatorInitializationException(msg);
    }
    pathValueReader = new PathValueReader(path);
  }

  @Override
  public String calculateValue(EntityObject entity) throws CalculationException {
    try {
      Object obj = pathValueReader.read(entity.getDocument());
      if (obj != null) {
        String str = obj.toString();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        OffsetDateTime dateTime = OffsetDateTime.parse(str);
        return dateTime.format(dtf);
      }
    } catch (InvalidPathException e1) {
      // A check for the validity of this field has been done in the initializer,
      // so we do not need have to deal with errors here again.
      logger.error(e1.getMessage());
    } catch (IllegalArgumentException e2) {
      // Should not be possible, but anyway ...
      logger.error("Record contains illegal date value: " + e2.getMessage());
    }
    return "";
  }

  /**
   * getPathToField will return a the full path to the first instance of the given field.
   * E.g.: identifications.defaultClassification.subgenus will be converted to
   * identifications.0.defaultClassification.subgenus This is because identifications
   * is of type array and we're only interested in the first item of the array.
   *
   * @param docType  the documentType of the documents
   * @param field  the field the calculator will use to collect the value from
   * @return the path to (the first instance of) the field
   */
  @SuppressWarnings({"SpellCheckingInspection", "DuplicatedCode"})
  private static Path getPathToField(Class<? extends IDocumentObject> docType, String field) {
    MappingInfo<? extends IDocumentObject> mapping =
        new MappingInfo<>(MappingFactory.getMapping(docType));
    try {
      Path parent = new Path(mapping.getNestedPath(field));
      String path = field.substring(parent.toString().length() + 1);
      if (PathUtil.isArray(parent, MappingFactory.getMapping(docType))) {
        // path = fieldname - parent
        return getPathToField(docType, parent.toString()).append("0").append(path);
      }
      // path = fieldname - parent
      return getPathToField(docType, parent.toString()).append(path);
    } catch (NullPointerException | NoSuchFieldException e) {
      return new Path(field);
    }
  }
}
