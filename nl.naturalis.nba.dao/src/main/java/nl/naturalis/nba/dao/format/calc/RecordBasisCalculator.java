package nl.naturalis.nba.dao.format.calc;

import static nl.naturalis.nba.dao.format.FormatUtil.EMPTY_STRING;

import java.util.Arrays;
import java.util.Map;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.format.CalculationException;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;

/**
 * A calculator for the recordBasis field in a DarwinCore archive for
 * specimens. Assumes the {@link EntityObject entity object} is a plain
 * {@link Specimen} document.
 *
 * @author Ayco Holleman
 *
 */
public class RecordBasisCalculator implements ICalculator {

  @Override
  public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
      throws CalculatorInitializationException {}

  @Override
  public Object calculateValue(EntityObject entity) throws CalculationException {
    Specimen specimen = (Specimen) entity.getDocument();
    String recordBasisStr = specimen.getRecordBasis();
    if (recordBasisStr == null) {
      return EMPTY_STRING;
    }
    String[] strsAllowed = {
        "PreservedSpecimen",
        "FossilSpecimen",
        "LivingSpecimen",
        "MaterialSample",
        "Event",
        "HumanObservation",
        "MachineObservation",
        "Taxon",
        "Occurrence",
        "OtherSpecimen"
    };
    if (Arrays.asList(strsAllowed).contains(recordBasisStr)) {
      return recordBasisStr;
    }
    if (recordBasisStr.contains("photo(copy) of herbarium sheet")
        || recordBasisStr.contains("Illustration")
        || recordBasisStr.contains("Photographs, negatives")
        || recordBasisStr.contains("DNA sample from sheet")
        || recordBasisStr.contains("Slides")
        || recordBasisStr.contains("Observation")) {
      return EMPTY_STRING;
    }
    return "PreservedSpecimen";
  }
}
