package nl.naturalis.nba.dao.format.calc;

import static java.lang.String.format;
import static nl.naturalis.nba.dao.format.calc.CalculatorUtils.getPathToField;

import java.util.ArrayList;
import java.util.Map;
import nl.naturalis.common.ObjectMethods;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.InvalidPathException;
import nl.naturalis.nba.common.PathUtil;
import nl.naturalis.nba.common.PathValueReader;
import nl.naturalis.nba.common.es.map.MappingFactory;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;

/**
 * The ConcatenateCalculator can be used to concatenate the values of multiple fields
 * and return them as one value.
 *
 * <p>The calculator needs to be initialised with at least two field names, otherwise
 * a CalculatorInitializationException will be thrown. The field names must be passed
 * as one named argument separated by the |-character (pipe).
 *
 * <p>Optional is a named argument for setting a character by which the values will be
 * separated in the string to be returned. When no separator is set, all values will be
 * concatenated as such.
 *
 * <p>Example configuration:
 *
 * <pre>
 * <calculator>
 *   <java-class>ConcatenateCalculator</java-class>
 *   <arg name="fields">fieldName01|fieldName02</arg>
 *   <arg name="separator">: </arg>
 * </calculator>
 * </pre>
 *
 * <p>NOTE: this calculator can be used with all document types.
 *
 * @author Tom Gilissen
 */
public class ConcatenateCalculator implements ICalculator {

  private ArrayList<Path> paths;
  private String separator;

  @Override
  public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
      throws CalculatorInitializationException {

    String msg = "The ConcatenateCalculator should be initialised with 2 arguments.";
    if (args == null || args.get("fields") == null) {
      throw new CalculatorInitializationException(msg);
    }

    String[] fields = args.get("fields").split("\\|");
    if (fields.length < 2) {
      throw new CalculatorInitializationException(msg);
    }
    ArrayList<Path> paths = new ArrayList<>();
    for (String field : fields) {
      try {
        Path path = new Path(getPathToField(docType, field));
        PathUtil.validate(path, MappingFactory.getMapping(docType));
        paths.add(path);
      } catch (InvalidPathException e) {
        msg = format("Entity %s: %s", field, e.getMessage());
        throw new CalculatorInitializationException(msg);
      }
    }

    String separator = args.get("separator");
    if (ObjectMethods.isEmpty(separator)) {
      separator = "";
    }

    this.paths = paths;
    this.separator = separator;
  }

  @Override
  public String calculateValue(EntityObject entity) {

    Object doc = entity.getDocument();
    String calculatedStr = "";
    for (Path path : paths) {
      String str = getValue(doc, path);
      if (calculatedStr.length() == 0) {
        calculatedStr = str;
      } else if (str.length() > 0) {
        calculatedStr = calculatedStr.concat(separator).concat(str);
      }
    }
    return calculatedStr;
  }

  private static String getValue(Object document, Path path) {
    PathValueReader pathValueReader = new PathValueReader(path);
    Object obj = pathValueReader.read(document);
    if (obj != null) {
      return obj.toString();
    }
    return "";
  }

}
