package nl.naturalis.nba.dao.translate;

import nl.naturalis.nba.api.QueryCondition;
import nl.naturalis.nba.common.es.map.MappingInfo;
import org.elasticsearch.index.query.RangeQueryBuilder;

class GTConditionTranslator extends RangeConditionTranslator {

  GTConditionTranslator(QueryCondition condition, MappingInfo<?> mappingInfo) {
    super(condition, mappingInfo);
  }

  @Override
  void setRange(RangeQueryBuilder query) {
    query.gt(condition.getValue());
  }
}
