package nl.naturalis.nba.dao;

import static nl.naturalis.common.ObjectMethods.*;
import static nl.naturalis.nba.dao.util.es.ESUtil.executeSearchRequest;
import static nl.naturalis.nba.dao.util.es.ESUtil.toDocumentObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import nl.naturalis.nba.api.*;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.dao.translate.QuerySpecTranslator;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.bucket.terms.IncludeExclude;
import org.geojson.GeoJsonObject;

/**
 * Utility class providing common functionality for classes in the dao package.
 *
 * @author Ayco Holleman
 */
public class DaoUtil {

  private static final Logger logger = getLogger(DaoUtil.class);

  private DaoUtil() {}

  /**
   * Returns a log4j logger for the specified class.
   *
   * @param cls
   * @return Logger instance
   */
  public static Logger getLogger(Class<?> cls) {
    return DaoRegistry.getInstance().getLogger(cls);
  }

  static <T extends IDocumentObject> T[] processSearchRequest(
      SearchRequest request, DocumentType<T> dt) {
    SearchResponse response = executeSearchRequest(request);
    return processQueryResponse(response, dt);
  }

  static <T extends IDocumentObject> T[] processQueryResponse(
      SearchResponse response, DocumentType<T> dt) {
    return getHits(response)
        .map(h -> toDocumentObject(h, dt))
        .toArray(i -> newDocumentArray(dt, i));
  }

  public static <T extends IDocumentObject> QueryResult<T> createSearchResult(
      QuerySpecTranslator translator, DocumentType<T> dt) throws InvalidQueryException {
    SearchRequest request = translator.translate();
    SearchResponse response = executeSearchRequest(request);
    QueryResult<T> result = new QueryResult<>();
    result.setResultSet(createItems(response, dt));
    doIfNotNull(response.getHits().getTotalHits(), x -> result.setTotalSize(x.value));
    return result;
  }

  static <T extends IDocumentObject> List<QueryResultItem<T>> createItems(
      SearchResponse response, DocumentType<T> dt) {
    if (logger.isDebugEnabled()) {
      String type = dt.getJavaType().getSimpleName();
      logger.debug("Converting search hits to {} instances", type);
    }
    SearchHit[] hits = response.getHits().getHits();
    List<QueryResultItem<T>> items = new ArrayList<>(hits.length);
    for (SearchHit hit : hits) {
      T obj = toDocumentObject(hit, dt);
      items.add(new QueryResultItem<>(obj, hit.getScore()));
    }
    return items;
  }

  @SuppressWarnings("unchecked")
  public static <T extends IDocumentObject> T[] newDocumentArray(DocumentType<T> dt, int len) {
    return (T[]) Array.newInstance(dt.getJavaType(), len);
  }

  /**
   * Creates a copy of the specified {@link QuerySpec} with conditions that only contain
   * reasonably-sized {@link QueryCondition#getValue() search terms}. Some type of search terms
   * (especially GeoJSON strings for complicated shapes) can easily become so large that even
   * printing them as part of a DEBUG message is expensive. This method is meant for debug purposes
   * only, when passing a {@code QuerySpec} object to {@code logger.debug()}.
   *
   * @param qs
   * @return
   */
  public static QuerySpec prune(QuerySpec qs) {
    if (qs == null || qs.getConditions() == null || qs.getConditions().size() == 0) {
      return qs;
    }
    QuerySpec copy = new QuerySpec();
    copy.setFields(qs.getFields());
    copy.setFrom(qs.getFrom());
    copy.setSize(qs.getSize());
    copy.setLogicalOperator(qs.getLogicalOperator());
    copy.setSortFields(qs.getSortFields());
    List<QueryCondition> siblings = new ArrayList<>(qs.getConditions().size());
    copy.setConditions(siblings);
    for (QueryCondition c : qs.getConditions()) {
      prune(c, siblings);
    }
    return copy;
  }

  /**
   * Creates a copy of the specified conditions such that the search terms in the copies are
   * reasonably-sized. See {@link #prune(QuerySpec)}.
   *
   * @param conditions
   * @return copy of condition(s)
   */
  public static QueryCondition[] prune(QueryCondition[] conditions) {
    if (conditions == null || conditions.length == 0) {
      return conditions;
    }
    List<QueryCondition> copies = new ArrayList<>(conditions.length);
    for (QueryCondition c : conditions) {
      prune(c, copies);
    }
    return copies.toArray(new QueryCondition[copies.size()]);
  }

  static final String ERR_BAD_FILTER =
      "Accept filter and Reject filter must "
          + "have the same type (regular expression or string array)";

  public static IncludeExclude translateFilter(Filter filter) throws InvalidQueryException {
    IncludeExclude ie = null;
    if (filter.getAcceptRegexp() != null || filter.getRejectRegexp() != null) {
      if (filter.getAcceptValues() != null || filter.getRejectValues() != null) {
        throw new InvalidQueryException(ERR_BAD_FILTER);
      }
      ie = new IncludeExclude(filter.getAcceptRegexp(), filter.getRejectRegexp());
    } else if (filter.getAcceptValues() != null || filter.getRejectValues() != null) {
      if (filter.getAcceptRegexp() != null || filter.getRejectRegexp() != null) {
        throw new InvalidQueryException(ERR_BAD_FILTER);
      }
      ie = new IncludeExclude(filter.getAcceptValues(), filter.getRejectValues());
    }
    return ie;
  }

  private static Stream<SearchHit> getHits(SearchResponse res) {
    return Arrays.stream(res.getHits().getHits());
  }

  private static void prune(QueryCondition condition, List<QueryCondition> siblings) {
    Object val = condition.getValue();
    if (val == null) {
      siblings.add(condition);
    } else if (val instanceof CharSequence) {
      if (((CharSequence) val).length() < 500) {
        siblings.add(condition);
      } else {
        String s = val.toString().substring(0, 500) + "... <truncated>";
        siblings.add(createCopy(condition, s));
      }
    } else if (val instanceof GeoJsonObject) {
      siblings.add(createCopy(condition, null));
    } else {
      siblings.add(condition);
    }
  }

  private static QueryCondition createCopy(QueryCondition original, String newValue) {
    QueryCondition copy = new QueryCondition(original);
    copy.setValue(newValue);
    return copy;
  }
}
