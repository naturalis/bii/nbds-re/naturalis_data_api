package nl.naturalis.nba.dao.util.es;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;

import java.io.IOException;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NbaException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.translate.QuerySpecTranslator;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

/**
 * An implementation of {@link IScroller} that uses Elasticsearch's scroll API.
 *
 * <p>Because the scroll API implements the equivalent of a database cursor, where selected data is
 * not affected by subsequent updates, this implementation is akin to what in database land is
 * called ACID. Note that when using the scroll API, the {@code from} property of the {@link
 * SearchRequest} is ignored (you can only scroll through the entire result set; not jump in at some
 * arbitrary point) and the {@code size} property has a different meaning: it specifies the size of
 * the scroll window (the number of documents to fetch per scroll request); it does not specify how
 * many documents you want. The {@code TransactionSafeScroller} class, however, still allows you to
 * specify a from and size property that work as you would expect from a regular query. The
 * advantage of using this implementation of {@link IScroller} over {@link DirtyScroller} is that
 * you can specify a sort order for the documents to iterate over. However, Elasticsearch's scroll
 * API is prone to timeouts, so you must be careful to specify a generous timeout setting using
 * {@link #setTimeout(int) setTimeout}.
 *
 * @author Ayco Holleman
 */
public class AcidScroller implements IScroller {

  private static final Logger logger = getLogger(AcidScroller.class);

  private final SearchRequest searchRequest;

  private int batchSize = 1000;
  private int timeout = 500;
  private int from = 0;
  private int size = 0;

  /**
   * Initialise the AcidScroller.
   *
   * @param querySpec  the query spec
   * @param documentType  the document type
   * @throws InvalidQueryException  when the query is incorrect / invalid
   */
  public AcidScroller(QuerySpec querySpec, DocumentType<?> documentType)
      throws InvalidQueryException {

    if (querySpec.getFrom() != null) {
      from = querySpec.getFrom();
      if (logger.isDebugEnabled()) {
        logger.debug("QuerySpec property \"from\" ({}) copied and nullified", from);
      }
      querySpec.setFrom(null);
    }

    if (querySpec.getSize() != null) {
      size = querySpec.getSize();
      if (logger.isDebugEnabled()) {
        logger.debug("QuerySpec property \"size\" ({}) copied and nullified", size);
      }
      querySpec.setSize(null);
    }

    QuerySpecTranslator qst = new QuerySpecTranslator(querySpec, documentType);
    searchRequest = qst.translate();

    if (querySpec.getSortFields() == null || querySpec.getSortFields().size() == 0) {
      SearchSourceBuilder searchSourceBuilder =
          (searchRequest.source() == null) ? new SearchSourceBuilder() : searchRequest.source();
      searchSourceBuilder.trackScores(true);
      if (size == 0 && from == 0) {
        // Scroll requests have been optimised for sort order is _doc
        searchSourceBuilder.sort(new FieldSortBuilder("_doc"));
      } else {
        // When size is important, sort order should be _score in order to get the default sort
        // order for a query
        searchSourceBuilder.sort(new FieldSortBuilder("_score").order(SortOrder.DESC));
      }
      searchSourceBuilder.size(batchSize); // NOTE: size is now the batch size of the scroll window
      searchRequest.source(searchSourceBuilder);
    }
  }

  @Override
  public void scroll(SearchHitHandler handler) throws NbaException {

    TimeValue tv = new TimeValue(timeout);
    final Scroll scroll = new Scroll(tv);
    searchRequest.scroll(scroll);

    RestHighLevelClient client = ESClientManager.getInstance().getClient();
    SearchResponse searchResponse;
    String scrollId;
    try {
      searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
      scrollId = searchResponse.getScrollId();

      int from = this.from;
      int size = this.size;
      int to = from + size;
      int i = 0;

      SCROLL_LOOP:
      do {
        // Ignore everything before the from-th document
        if (i + searchResponse.getHits().getHits().length < from) {
          i += searchResponse.getHits().getHits().length;
        }
        for (SearchHit hit : searchResponse.getHits().getHits()) {
          if (size != 0 && i >= to) {
            break SCROLL_LOOP;
          }
          if (i >= from) {
            if (!handler.handle(hit)) {
              break SCROLL_LOOP;
            }
          }
          i += 1;
        }

        SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
        scrollRequest.scroll(scroll);
        searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
        scrollId = searchResponse.getScrollId();
      } while (searchResponse.getHits().getHits().length != 0);

    } catch (IOException e) {
      throw new NbaException(e.getMessage());
    }

    // Clean up
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse;
    try {
      clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
      @SuppressWarnings("unused")
      boolean succeeded = clearScrollResponse.isSucceeded();
    } catch (IOException e) {
      throw new NbaException(e.getMessage());
    }
  }

  /**
   * Returns the size of the scroll window (the number of documents to fetch per scroll request).
   * Defaults to 10000 documents.
   *
   * @return the size of the scroll window
   */
  public int getBatchSize() {
    return batchSize;
  }

  /**
   * Sets the size of the scroll window (the {@code size} property of the {@link
   * SearchRequestBuilder}). Defaults to 10000 documents.
   *
   * @param batchSize  the desired size of the scroll window
   */
  public void setBatchSize(int batchSize) {
    this.batchSize = batchSize;
  }

  /**
   * Returns the timeout of the scroll token. Defaults to 500 milliseconds.
   *
   * @return the number of milliseconds before a timeout
   */
  @SuppressWarnings("unused")
  public int getTimeout() {
    return timeout;
  }

  /**
   * Sets the timeout of the scroll token. Defaults to 500 milliseconds.
   *
   * @param timeout  set the amount of milliseconds of the timeout
   */
  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }
}
