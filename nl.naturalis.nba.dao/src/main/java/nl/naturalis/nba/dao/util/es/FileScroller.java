package nl.naturalis.nba.dao.util.es;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import nl.naturalis.nba.dao.format.Entity;
import nl.naturalis.nba.dao.format.dwca.JsonDataSourceLineHandler;
import org.apache.logging.log4j.Logger;

public class FileScroller {

  private static final Logger logger = getLogger(FileScroller.class);
  private static final String DWCA_DIR = "/data/nba/";

  private final ArrayList<String> fileNames;

  /**
   * Utility class for scroller over source files. This is a file-variant of
   * the AcidScroller and DirtyScroller which scroll over search results.
   *
   * @param entity  an DwcA entity
   */
  public FileScroller(Entity entity) {

    fileNames = Stream.of(Objects.requireNonNull(new File(DWCA_DIR).listFiles()))
        .filter(
            file ->
                !file.isDirectory()
                    && file.getName().endsWith("jsonl")
                    && file.getName().contains(entity.getName()))
        .map(File::getName)
        .distinct()
        .collect(Collectors.toCollection(ArrayList::new));
    Collections.sort(fileNames);
  }

  /**
   * Iterates over the line in the source data files.
   */
  public void scroll(JsonDataSourceLineHandler handler) {

    for (String fileName : fileNames) {
      fileName = DWCA_DIR.concat(fileName);
      String jsonLine;
      try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
        while ((jsonLine = br.readLine()) != null) {
          handler.handle(jsonLine);
        }
      } catch (Exception e) {
        logger.error(e);
      }
    }
  }
}
