package nl.naturalis.nba.dao.format.calc;

import static java.lang.String.format;
import static nl.naturalis.nba.dao.format.calc.CalculatorUtils.getPathToField;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.common.InvalidPathException;
import nl.naturalis.nba.common.PathUtil;
import nl.naturalis.nba.common.PathValueReader;
import nl.naturalis.nba.common.es.map.MappingFactory;
import nl.naturalis.nba.dao.format.CalculationException;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * CalendarDateCalculator is a calculator that converts the value from a DateTime field to a
 * calendar date (YYYY-MM-DD). The calculator will return a datum in the format 'yyyy-MM-dd'.
 *
 * <p>Note that the date field may be part of an array or child document. This calculator will
 * return only the value of the date field of the first item / first child. When the field is
 * even further nested, this calculator will only return the value of the first of the first,
 * ...., of the first item.
 *
 */
public class CalendarDateCalculator implements ICalculator {

  private PathValueReader pathValueReader;

  private static final Logger logger = LogManager.getLogger(CalendarDateCalculator.class);

  @SuppressWarnings("DuplicatedCode")
  @Override
  public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
      throws CalculatorInitializationException {

    String dateField = args.get(null);
    Path path = new Path(getPathToField(docType, dateField));
    try {
      PathUtil.validate(path, MappingFactory.getMapping(docType));
    } catch (InvalidPathException e) {
      String msg = format("Entity %s: %s", dateField, e.getMessage());
      throw new CalculatorInitializationException(msg);
    }
    pathValueReader = new PathValueReader(path);
  }

  @Override
  public String calculateValue(EntityObject entity) throws CalculationException {
    try {
      Object obj = pathValueReader.read(entity.getDocument());
      if (obj != null) {
        String str = obj.toString();
        OffsetDateTime dt = OffsetDateTime.parse(str);
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return fmt.format(dt);
      }
    } catch (InvalidPathException e1) {
      // A check for the validity of this field has been done in the initializer,
      // so we do not need have to deal with errors here again.
      logger.error(e1.getMessage());
    } catch (IllegalArgumentException e2) {
      // Should not be possible, but anyway ...
      logger.error("Record contains illegal date value: " + e2.getMessage());
    }
    return "";
  }
}
