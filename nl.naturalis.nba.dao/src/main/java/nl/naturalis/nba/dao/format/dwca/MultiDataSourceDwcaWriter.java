package nl.naturalis.nba.dao.format.dwca;

import static nl.naturalis.nba.utils.TimeUtil.getDuration;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import nl.naturalis.common.ExceptionMethods;
import nl.naturalis.common.io.REZipOutputStream;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NbaException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.format.DataSetConfigurationException;
import nl.naturalis.nba.dao.format.DataSetWriteException;
import nl.naturalis.nba.dao.format.Entity;
import nl.naturalis.nba.dao.util.es.DirtyScroller;
import nl.naturalis.nba.dao.util.es.IScroller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Manages the assembly and creation of DarwinCore archives. Use this class if you cannot generate
 * all CSV files from a single query (each CSV file requires a new query to be executed). With this
 * class a separate query is issued for each entity (i.e. each file generated as part of the DwC
 * archive). A {@code MultiDataSourceDwcaWriter} is used for dataset configuration files where each
 * &;lt;entity&gt; element has its own &;lt;data-source&gt; element. See also the XSD for dataset
 * configuration files in src/main/resources.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 */
@SuppressWarnings("CheckStyle")
class MultiDataSourceDwcaWriter implements IDwcaWriter {

  private static final Logger logger = LogManager.getLogger(MultiDataSourceDwcaWriter.class);

  private final DwcaConfig cfg;
  private final OutputStream out;

  MultiDataSourceDwcaWriter(DwcaConfig dwcaConfig, OutputStream out) {
    this.cfg = dwcaConfig;
    this.out = out;
  }

  /**
   * It's not possible -not at the moment at least- to create a DarwinCore archive for a
   * user-defined query with more than one data source. The query the end-user provides does not
   * have to be a valid query for each of the datasets after all.
   */
  @Override
  public void writeDwcaForQuery(QuerySpec querySpec) throws InvalidQueryException {
    logger.error(
        "Method `writeDwcaForQuery(QuerySpec querySpec)` has not been implemented "
            + "and cannot be used for user defined queries!");
    throw new InvalidQueryException(
        "This method has not been implemented for user defined queries");
  }

  @SuppressWarnings("CheckStyle")
  @Override
  public void writeDwcaForDataSet() throws DataSetConfigurationException, DataSetWriteException {

    long start = System.currentTimeMillis();
    logger.info("Generating DarwinCore archive from multiple data sets");
    String nio = System.getProperty("nl.naturalis.common.io.REZipOutputStream.nio", "false");
    logger.debug("Use java.nio: " + nio.equalsIgnoreCase("true"));

    DwcaPreparator dwcaPreparator = new DwcaPreparator(cfg);
    dwcaPreparator.prepare();
    try (REZipOutputStream rezos = createREZipOutputStream()) {
      try {
        writeCsvFilesForDataSet(rezos);
        try (ZipOutputStream zos = rezos.mergeEntries()) {
          zos.putNextEntry(new ZipEntry("meta.xml"));
          zos.write(dwcaPreparator.getMetaXml());
          logger.info("Writing eml.xml ({})", cfg.getEmlFile());
          zos.putNextEntry(new ZipEntry("eml.xml"));
          zos.write(dwcaPreparator.getEml());
        }
      } catch (InvalidQueryException e) {
        throw new DataSetConfigurationException(e);
      } catch (Exception e) {
        logger.error(ExceptionMethods.getDetailedMessage(e));
        try (ZipOutputStream zos = rezos.mergeEntries()) {
          zos.putNextEntry(new ZipEntry("__ERROR__.txt"));
          e.printStackTrace(new PrintStream(zos));
        } catch (Exception e2) {
          logger.error(ExceptionMethods.getDetailedMessage(e));
        }
      }
    } catch (IOException e1) {
      throw ExceptionMethods.uncheck(e1);
    }
    String took = getDuration(start);
    logger.info("DarwinCore archive generated (took {})", took);
    logger.info("Finished writing DarwinCore archive for multiple data sets");
  }

  private void writeCsvFilesForDataSet(REZipOutputStream rezos)
      throws DataSetConfigurationException, IOException, NbaException {

    // Keeps track of whether the header for an entity has been printed already
    HashSet<String> done = new HashSet<>();
    for (Entity entity : cfg.getDataSet().getEntities()) {
      logNextEntity(entity);
      String fileName = cfg.getCsvFileName(entity);
      QuerySpec query = entity.getDataSource().getQuerySpec();
      DocumentType<?> dt = entity.getDataSource().getDocumentType();
      IScroller scroller = new DirtyScroller(query, dt);

      logger.debug(
          "DirtyScroller has been initialised with batch size: {}", scroller.getBatchSize()
      );

      MultiDataSourceSearchHitHandler handler = handler(rezos, entity, fileName);
      if (!done.contains(fileName)) {
        handler.printHeaders();
        done.add(fileName);
      }
      scroller.scroll(handler);
      handler.logStatistics();
      out.flush();
    }
  }

  private REZipOutputStream createREZipOutputStream()
      throws DataSetConfigurationException, IOException {

    Entity[] entities = cfg.getDataSet().getEntities();
    Entity firstEntity = entities[0];
    String fileName = cfg.getCsvFileName(firstEntity);
    REZipOutputStream.Builder builder = REZipOutputStream.withMainEntry(fileName, out);
    // Multiple entities may get written to the same zip entry (e.g. taxa and synonyms are both
    // written to taxa.txt). So we must make sure to create only unique zip entries.
    HashSet<String> fileNames = new HashSet<>();
    for (Entity e : entities) {
      fileName = cfg.getCsvFileName(e);
      if (fileNames.contains(fileName)) {
        continue;
      }
      fileNames.add(fileName);
      if (e.getName().equals(firstEntity.getName())) {
        continue;
      }
      builder.addEntry(fileName);
    }
    return builder.build();
  }

  private static void logNextEntity(Entity e) {
    String fmt = "Writing csv file for entity {} ({})";
    logger.info(fmt, e.getName(), e.getDataSource().getDocumentType());
  }

  private static MultiDataSourceSearchHitHandler handler(
      REZipOutputStream rezos, Entity entity, String fileName) {
    return new MultiDataSourceSearchHitHandler(entity, fileName, rezos);
  }
}
