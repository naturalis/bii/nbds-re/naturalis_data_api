package nl.naturalis.nba.dao.format.calc;

import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.format.CalculatorInitializationException;
import nl.naturalis.nba.dao.format.EntityObject;
import nl.naturalis.nba.dao.format.ICalculator;

import java.util.Map;

/**
 * A calculator for the String field preparations in a DarwinCore archive for specimen. The
 * calculator defines which value to return based on the values of the Specimen fields
 * kindOfUnit and preparationType. The class assumes the {@link EntityObject entity object}
 * to be a plain {@link Specimen} document.
 *
 */
@SuppressWarnings("unused")
public class PreparationsCalculator implements ICalculator {

    static String NOT_APPLICABLE = "not applicable";
    static String UNKNOWN = "unknown";
    @Override
    public void initialize(Class<? extends IDocumentObject> docType, Map<String, String> args)
            throws CalculatorInitializationException {}

    @Override
    public String calculateValue(EntityObject entity) {
        Specimen specimen = (Specimen) entity.getDocument();
        String kindOfUnit = cleanString(specimen.getKindOfUnit());
        String preparationType = cleanString(specimen.getPreparationType());
        if (kindOfUnit.equalsIgnoreCase(preparationType)) {
            preparationType = "";
        } else if (kindOfUnit.equals(UNKNOWN) && preparationType.equals(NOT_APPLICABLE)){
                preparationType = "";
        } else if (
                (kindOfUnit.equals(NOT_APPLICABLE) || kindOfUnit.equals(UNKNOWN)) && !preparationType.isEmpty()) {
                kindOfUnit = "";
        } else if (
                (preparationType.equals(NOT_APPLICABLE) || preparationType.equals(UNKNOWN)) && !kindOfUnit.isEmpty()) {
                preparationType = "";
        }
        return calculateReturnStr(kindOfUnit, preparationType);
    }

    private String calculateReturnStr(String strOne, String strTwo) {
        if (strOne.isEmpty() && strTwo.isEmpty()) {
            return "";
        }
        if (!strTwo.isEmpty() && !strTwo.equals(NOT_APPLICABLE) && !strTwo.equals(UNKNOWN)) {
            strTwo = "(" + strTwo + ")";
        }
        if (!strOne.isEmpty() && !strTwo.isEmpty()) {
            return strOne + " " + strTwo;
        }
        return strOne + strTwo;
    }

    private String cleanString(String str) {
        if (str == null) {
            return "";
        } else {
            str = str.strip();
            if (str.equalsIgnoreCase(NOT_APPLICABLE)) {
                str = NOT_APPLICABLE;
            }
            if (str.equalsIgnoreCase(UNKNOWN)) {
                str = UNKNOWN;
            }
            return str;
        }
    }

}
