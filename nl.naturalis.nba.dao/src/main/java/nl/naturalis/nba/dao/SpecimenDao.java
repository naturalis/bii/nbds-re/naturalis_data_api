package nl.naturalis.nba.dao;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.dao.DaoUtil.processSearchRequest;
import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;
import static nl.naturalis.nba.dao.util.es.ESUtil.executeSearchRequest;
import static nl.naturalis.nba.dao.util.es.ESUtil.newSearchRequest;
import static nl.naturalis.nba.utils.debug.DebugUtil.printCall;
import static org.elasticsearch.index.query.QueryBuilders.constantScoreQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.io.File;
import java.io.OutputStream;
import java.util.Arrays;
import nl.naturalis.nba.api.GroupByScientificNameQueryResult;
import nl.naturalis.nba.api.GroupByScientificNameQuerySpec;
import nl.naturalis.nba.api.ISpecimenAccess;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.NoSuchDataSetException;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.format.DataSetConfigurationException;
import nl.naturalis.nba.dao.format.DataSetWriteException;
import nl.naturalis.nba.dao.format.dwca.DwcaConfig;
import nl.naturalis.nba.dao.format.dwca.DwcaDataSetType;
import nl.naturalis.nba.dao.format.dwca.DwcaUtil;
import nl.naturalis.nba.dao.format.dwca.IDwcaWriter;
import nl.naturalis.nba.dao.util.GroupSpecimensByScientificNameHelper;
import nl.naturalis.nba.dao.util.NameResolutionQueryHelper;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.ConstantScoreQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

public class SpecimenDao extends NbaDao<Specimen> implements ISpecimenAccess {

  private static final Logger logger = getLogger(SpecimenDao.class);

  public SpecimenDao() {
    super(SPECIMEN);
  }

  @Override
  public boolean exists(String unitID) {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("exists", unitID));
    }
    SearchRequest request = newSearchRequest(SPECIMEN);
    TermQueryBuilder tqb = termQuery("unitID", unitID);
    ConstantScoreQueryBuilder csq = constantScoreQuery(tqb);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(csq);
    searchSourceBuilder.size(0);
    searchSourceBuilder.trackTotalHitsUpTo(1);
    request.source(searchSourceBuilder);
    SearchResponse response = executeSearchRequest(request);
    return response.getHits().getTotalHits().value != 0;
  }

  @Override
  public Specimen[] findByUnitID(String unitID) {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("findByUnitID", unitID));
    }
    SearchRequest request = newSearchRequest(SPECIMEN);
    TermQueryBuilder tqb = termQuery("unitID", unitID);
    ConstantScoreQueryBuilder csq = constantScoreQuery(tqb);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.trackTotalHits(false);
    searchSourceBuilder.query(csq);
    request.source(searchSourceBuilder);
    return processSearchRequest(request, dt);
  }

  private static String[] namedCollections;

  @Override
  public void dwcaQuery(QuerySpec querySpec, OutputStream out) throws InvalidQueryException {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("dwcaQuery", querySpec, out));
    }
    try {
      DwcaConfig config = DwcaConfig.getDynamicDwcaConfig(DwcaDataSetType.SPECIMEN);
      IDwcaWriter writer = config.getWriter(out);
      writer.writeDwcaForQuery(querySpec);
    } catch (DataSetConfigurationException | DataSetWriteException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void dwcaGetDataSet(String name, OutputStream out) throws NoSuchDataSetException {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("dwcaGetDataSet", name, out));
    }
    try {
      DwcaConfig config = new DwcaConfig(name, DwcaDataSetType.SPECIMEN);
      IDwcaWriter writer = config.getWriter(out);
      writer.writeDwcaForDataSet();
    } catch (DataSetConfigurationException | DataSetWriteException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public String[] dwcaGetDataSetNames() {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("dwcaGetDataSetNames"));
    }
    File dir = DwcaUtil.getDwcaConfigurationDirectory(DwcaDataSetType.SPECIMEN);
    File[] files =
        dir.listFiles(
            f ->
                f.isFile()
                    && !f.getName().startsWith("dynamic")
                    && f.getName().endsWith(DwcaConfig.CONF_FILE_EXTENSION));
    return Arrays.stream(files)
        .map(f -> f.getName().substring(0, f.getName().indexOf('.')))
        .sorted()
        .toArray(String[]::new);
  }

  @Override
  public GroupByScientificNameQueryResult groupByScientificName(
      GroupByScientificNameQuerySpec sngQuery) throws InvalidQueryException {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("groupByScientificName", sngQuery));
    }
    return GroupSpecimensByScientificNameHelper.groupByScientificName(sngQuery);
  }

  @Override
  public QueryResult<Specimen> queryWithNameResolution(NameResolutionQuerySpec query)
      throws InvalidQueryException {
    if (logger.isDebugEnabled()) {
      logger.debug(printCall("queryWithNameResolution", query));
    }
    return new NameResolutionQueryHelper(this, query).executeQuery();
  }

  @Override
  public String[] getUniqueKeyFields() {
    return new String[] {"unitID"};
  }
}
