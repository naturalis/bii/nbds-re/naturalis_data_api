package nl.naturalis.nba.dao.util.es;

import static nl.naturalis.nba.dao.DaoUtil.getLogger;
import static nl.naturalis.nba.dao.util.es.ESUtil.executeSearchRequest;

import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NbaException;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.dao.translate.QuerySpecTranslator;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

/**
 * Utility class for using Elasticsearch's "search after" scroll methodology. The prefix
 * "dirty" has been used because this scroller iterates over "live" data, meaning that if
 * documents are deleted/created after the query is issued they will be absent/present in
 * the result set. For the NBA this is not a problem, because once the data imports are
 * complete, the data in the indices does not change.
 *
 * <p>Note that when using this API, the {@code from} property of the {@link SearchRequest} is
 * ignored. You can only scroll through the entire result set; not jump in or out at some
 * arbitrary point. The {@code size} property has a different meaning: it specifies the size
 * of the scroll window (the number of documents to fetch per scroll request). So, it does no
 * longer specify how many documents you want to retrieve in total. The {@code DirtyScroller}
 * class, however, still allows you to specify a from and size property that work as you would
 * expect from a regular query.
 *
 * @author Ayco Holleman
 */
public class DirtyScroller implements IScroller {

  private static final Logger logger = getLogger(DirtyScroller.class);

  private final SearchRequest searchRequest;

  private int batchSize = 1000;
  private int from = 0;
  private int size = 0;

  /**
   * Initialise the DirtyScroller.
   *
   * @param querySpec  the query spec
   * @param documentType  the document type
   * @throws InvalidQueryException  when the query spec is incorrect / invalid
   */
  public DirtyScroller(QuerySpec querySpec, DocumentType<?> documentType)
      throws InvalidQueryException {

    if (querySpec.getFrom() != null) {
      this.from = querySpec.getFrom();
      if (logger.isDebugEnabled()) {
        logger.debug("QuerySpec property \"from\" ({}) copied and nullified", from);
      }
      querySpec.setFrom(null);
    }

    if (querySpec.getSize() != null) {
      this.size = querySpec.getSize();
      if (logger.isDebugEnabled()) {
        logger.debug("QuerySpec property \"size\" ({}) copied and nullified", size);
      }
      querySpec.setSize(null);
    }

    if (querySpec.getSortFields() != null) {
      logger.warn("Ignoring sort fields");
      querySpec.setSortFields(null);
    }

    QuerySpecTranslator qst = new QuerySpecTranslator(querySpec, documentType);
    searchRequest = qst.translate();

    SearchSourceBuilder searchSourceBuilder =
        (searchRequest.source() == null) ? new SearchSourceBuilder() : searchRequest.source();
    searchSourceBuilder.trackTotalHits(false);
    //searchSourceBuilder.sort(new FieldSortBuilder("sourceSystemId").order(SortOrder.ASC));
    searchSourceBuilder.sort("_id", SortOrder.DESC);
    searchSourceBuilder.size(batchSize);
    searchRequest.source(searchSourceBuilder);
  }

  @Override
  public void scroll(SearchHitHandler handler) throws NbaException {

    int from = this.from;
    int size = this.size;
    int to = from + size;
    int i = 0;

    SCROLL_LOOP:
    do {
      SearchResponse response = executeSearchRequest(searchRequest);
      SearchHit[] hits = response.getHits().getHits();
      if (hits.length == 0) {
        break;
      }
      if (i + hits.length < from) {
        i += hits.length;
      }
      for (SearchHit hit : hits) {
        if (size != 0 && i >= to) {
          break SCROLL_LOOP;
        }
        if (i >= from) {
          if (!handler.handle(hit)) {
            break SCROLL_LOOP;
          }
        }
        i += 1;
      }

      String lastId = hits[hits.length - 1].getId();
      SearchSourceBuilder searchSourceBuilder = searchRequest.source();
      searchSourceBuilder.searchAfter(new Object[] {lastId});
      searchRequest.source(searchSourceBuilder);
    } while (true);
  }

  /**
   * Returns the size of the scroll window (the number of documents to fetch per scroll request).
   * Defaults to 1000 documents.
   *
   * @return the batch size
   */
  public int getBatchSize() {
    return batchSize;
  }

  /**
   * Sets the size of the scroll window (the {@code size} property of the {@link
   * SearchRequestBuilder}). Defaults to 10000 documents.
   *
   * @param batchSize  the size of a batch set
   */
  public void setBatchSize(int batchSize) {
    this.batchSize = batchSize;
  }
}
