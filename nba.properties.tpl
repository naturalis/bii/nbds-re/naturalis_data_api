# NBA Configuration file. This file is used for the ETL module and
# the REST service. Some settings are only used by the ETL module, 
# some only by the REST service.

# You must make 2 copies of this file: nba.properties and
# nba-test.properties. Then modify the settings as appropriate
# for production and testing respectively.

# IMPORTANT: For nba-test.properties you MUST modify the index
# names. Otherwise you will erase data from the production indexes
# when maven starts the integration tests. Choose any name you
# like, e.g. specimen_integration_test.

# The top directory for configuration files and other assets used
# by the NBA REST service. This directory will contain the "dwca"
# "metadata" subdirectories required by the REST service.
nba.api.install.dir=~/nba

# The top directory for configuration files and other assets used
# by the ETL software. This directory will contain the "sh", "conf"
# and "lib" subdirectories required to run the ETL software.
nba.etl.install.dir=~/nba-etl

# The base url of the nba. This setting is used in the dao module.
# (e.g. /getRestServices). When no value is set, the dao module
# try to establish the base url itself, but this may produce
# unexpected results in the context of a dockerized installation.
nba.baseurl=https://api.biodiversitydata.nl/


# ***************
# Shared settings
# ***************
elasticsearch.cluster.name=<es-cluster>
elasticsearch.transportaddress.host=127.0.0.1
elasticsearch.transportaddress.port=9200
elasticsearch.index.default.shards=4
elasticsearch.index.default.replicas=0
elasticsearch.index.0.name=specimen
elasticsearch.index.0.types=Specimen
elasticsearch.index.1.name=taxon
elasticsearch.index.1.types=Taxon
elasticsearch.index.2.name=multimedia
elasticsearch.index.2.types=MultiMediaObject
elasticsearch.index.3.name=geoareas
elasticsearch.index.3.types=GeoArea


# **************************
# REST service-only settings
# **************************

# Set this property to true, when one or more indexes are
# shared by an alias
elasticsearch.aliases=false

# The maximum number of groups that Elasticsearch can collect when 
# aggregating a set of documents
nl.naturalis.nba.aggregations.maxNumGroups=10000

# The maximum number of buckets (unique scientific names) that
# Elasticsearch must collect for the groupByScientificName service.
# Elasticsearch will stop aggregating over the result set the moment
# it has found this many buckets.
nl.naturalis.nba.specimen.groupByScientificName.maxNumBuckets=5000

# The size of the query cache, which maps queries to their results.
nl.naturalis.nba.specimen.groupByScientificName.queryCacheSize=1000

# The number of milliseconds a query must at least take to be cached.
nl.naturalis.nba.specimen.groupByScientificName.cacheTreshold=3000
nl.naturalis.nba.taxon.groupByScientificName.maxNumBuckets=5000
nl.naturalis.nba.taxon.groupByScientificName.queryCacheSize=1000
nl.naturalis.nba.taxon.groupByScientificName.cacheTreshold=3000

# For small dwca datasets the nba uses the Elasticsearch scroll API
# (through the AcidScroller) as this will honour the sortFields of the
# QuerySpec. For large datasets the "search_after" technique is
# used (through the DirtyScroller) while this excludes the possibility
# of timeouts. The default threshold for a "large" dataset is set at
# 10.000 documents:
nl.naturalis.nba.dwca.max.scroll.size=25000
nl.naturalis.nba.dwca.cache.path=~/nba/cache

# ColPlus API webservice for Name Resolution Requests
nl.naturalis.nba.dao.colplus.nameusage.service=https://api.catalogueoflife.org/nameusage/search


# **************************
# ETL module-only settings
# **************************

# The ETL can either push the documents it creates directly into 
# the document store (etl.output=es) or write them to the file 
# system (etl.output=file).
# Default output: es
nl.naturalis.nba.etl.output=file

# Set this property to true if you want specimen documents to be
# enriched with taxonomic information. (Default enrich: false)
nl.naturalis.nba.etl.enrich=false

# Optional system property that can be used to set the size of
# Elasticsearch bulk index requests (the number of documents to be indexed
# at once). Default: 1000
nl.naturalis.nba.etl.queueSize=1000

# Optional property to suppress errors from the log file (default = true).
#nl.naturalis.nba.etl.suppressErrors=false

# Set this property to true when you require the index(es) to be 
# recreated at the beginning of a new import
# Default: true
nl.naturalis.nba.etl.truncate=true

# CRS Harvest
crs.specimens.url.initial=http\://crs.naturalis.nl/atlantispubliek/oai.axd?verb\=ListRecords&metadataprefix\=oai_crs_object
crs.specimens.url.resume=http\://crs.naturalis.nl/atlantispubliek/oai.axd?verb\=ListRecords&resumptionToken\=%s
crs.multimedia.url.initial=http\://crs.naturalis.nl/atlantispubliek/oai.axd?verb\=ListRecords&metadataprefix\=oai_crs
crs.multimedia.url.resume=http\://crs.naturalis.nl/atlantispubliek/oai.axd?verb\=ListRecords&resumptionToken\=%s

# Whether we want to use the pre-harvested, locally
# stored XML files (true), instead of calling live calls to
# the CRS OAIPMH service (false)
crs.offline=true

# The maximum age in hours of the records to harvest. Zero (0) means
# no maximum (full harvest). Only applicable when using OAI service.
crs.harvest.max.age=0

# Path to source system data
brahms.data.dir=~/nba-etl/source-data/brahms
col.data.dir=~/nba-etl/source-data/col
crs.data.dir=~/nba-etl/source-data/crs
dcsr.data.dir=~/nba-etl/source-data/dcsr
geo.data.dir=~/nba-etl/source-data/geo
json.data.dir=~/nba-etl/source-data/json
medialib.data.dir=~/nba-etl/source-data/medialib
nsr.data.dir=~/nba-etl/source-data/nsr

# Needed to generate links to the CoL
col.year=2019

# PURL base url
purl.baseurl=https://data.biodiversitydata.nl
