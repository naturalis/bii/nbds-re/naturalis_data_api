#!/bin/bash

BRANCH="main"
if [ -n "$1" ]; then
  BRANCH=$1
fi
echo "Updating branch $BRANCH"

cd /etc/nba/dwca || exit 1

if ! git pull --ff-only; then
        echo "Could not fetch the latest commit(s). Please check the repository"
        exit 1
fi

if ! git fetch origin "${BRANCH}"; then
        echo "Unknown branch name: ${BRANCH}. Please use an existing branch name"
        echo "DwC-A config was not updated!"
        exit 1
fi

if ! git checkout "${BRANCH}" && git pull --ff-only; then
        echo "Checkout of branch \"${BRANCH}\" failed. Please retry"
        echo "DwC-A config was not updated!"
        exit 1
fi

echo "DwC-A config was updated with the latest settings from branch \"${BRANCH}\""
exit 0
