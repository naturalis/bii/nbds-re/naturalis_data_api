package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.api.model.License.CC0_10;
import static nl.naturalis.nba.utils.xml.DOMUtil.getDescendant;
import static nl.naturalis.nba.utils.xml.DOMUtil.getDescendants;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.GatheringSiteCoordinates;
import nl.naturalis.nba.api.model.MultiMediaContentIdentification;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.SpecimenTypeStatus;
import nl.naturalis.nba.api.model.VernacularName;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AbstractTransformer;
import nl.naturalis.nba.etl.AllTests;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.utils.CommonReflectionUtil;
import nl.naturalis.nba.utils.reflect.ReflectionUtil;
import nl.naturalis.nba.utils.xml.DOMUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

/**
 * Test class for CrsSpecimenTransformer.java
 */
@SuppressWarnings("unchecked")
public class CrsMultiMediaTransformerTest {

  URL multimediaUrl;
  File multimediaFile;

  /**
   * @throws Exception when resource file is missing
   */
  @Before
  public void setUp() throws Exception {

    System.setProperty(
        "nl.naturalis.nba.etl.testGenera",
        "malus,parus,larus,bombus,rhododendron,aedes,felix,tulipa,rosa,canis,passer,trientalis");
    multimediaUrl = AllTests.class.getResource("multimedia.00000000000000.013159.oai.xml");
    assert multimediaUrl != null;
    multimediaFile = new File(multimediaUrl.getFile());
  }

  /**
   * @throws Exception when test transformation fails
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * Test method for {@link CrsMultiMediaTransformer#doTransform()}.
   *
   * @throws Exception
   *
   *         Test to verify if the doTransform method returns the expected {@link MultiMediaObject} object
   */
  @Test
  @SuppressWarnings("ExtractMethodRecommender")
  public void testDoTransform() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    report.initialiseUrls();
    ETLStatistics etlStatistics = new ETLStatistics();
    List<MultiMediaObject> transformed;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    String[] testGenera =
        new String[] {
          "malus",
          "parus",
          "larus",
          "bombus",
          "rhododendron",
          "felix",
          "tulipa",
          "rosa",
          "canis",
          "passer",
          "trientalis"
        };

    Class<CrsMultiMediaTransformer> testedClass = CrsMultiMediaTransformer.class;
    Field privateField = testedClass.getDeclaredField("testGenera");
    privateField.setAccessible(true);
    privateField.set(crsMultiMediaTransformer, testGenera);

    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element record = extracted.getRecord();
      Element oaiDcElem = DOMUtil.getDescendant(record, "oai_dc:dc");
      assertNotNull(oaiDcElem);
      List<Element> frmDigitaleBestandenElems = DOMUtil.getDescendants(oaiDcElem, "frmDigitalebestanden");
      assertNotNull(frmDigitaleBestandenElems);
      List<Element> ncsrDeterminationElems = DOMUtil.getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer, "objectID", "RMNH.INS.867435");
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer, "input", extracted);
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer, "recordStr", ETLUtil.inputToString(record));
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer, "oaiDcElem", oaiDcElem);
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer, "frmDigitaleBestandenElems", frmDigitaleBestandenElems);
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer, "ncsrDeterminationElems", ncsrDeterminationElems);

      Object returned =
          CommonReflectionUtil.callMethod(null, null,
          crsMultiMediaTransformer, "doTransform");
      transformed = (List<MultiMediaObject>) returned;

      MultiMediaObject sp = transformed.get(0); // Just test the first one
      // String id = "RMNH.INS.867435_1";
      String id = "RMNH.INS.867435_0";
      // TODO: undo temporary adjustment of the id

      int expectedNumberOfMmObjects = 2;
      assertEquals("01", expectedNumberOfMmObjects, transformed.size());

      // Test the 1st Mm object
      MultiMediaObject expectedMmo = new MultiMediaObject();
      expectedMmo.setId(id.concat("@CRS"));
      expectedMmo.setSourceInstitutionID("Naturalis Biodiversity Center");
      expectedMmo.setSourceID("CRS");
      expectedMmo.setOwner("Naturalis Biodiversity Center");
      expectedMmo.setUnitID(id);
      expectedMmo.setLicense(CC0_10);
      expectedMmo.setCollectionType("Diptera");
      expectedMmo.setTitle(id);
      expectedMmo.setAssociatedSpecimenReference("RMNH.INS.867435@CRS");
      expectedMmo.setMultiMediaPublic(true);

      List<ServiceAccessPoint> serviceAccessPoints = new ArrayList<>();
      URI expectedUri =
          URI.create("https://medialib.naturalis.nl/file/id/RMNH.INS.867435_1/format/large");
      String expectedFormat = "image/jpeg";
      String expectedVariant = "ac:GoodQuality";
      serviceAccessPoints.add(new ServiceAccessPoint(expectedUri, expectedFormat, expectedVariant));
      expectedMmo.setServiceAccessPoints(serviceAccessPoints);

      assertNotNull("02", sp);
      assertEquals("03", expectedMmo.getId(), sp.getId());
      assertEquals("04", expectedMmo.getSourceInstitutionID(), sp.getSourceInstitutionID());
      assertEquals("05", expectedMmo.getSourceID(), sp.getSourceID());
      assertEquals("06", expectedMmo.getOwner(), sp.getOwner());
      assertEquals("07", expectedMmo.getUnitID(), sp.getUnitID());
      assertEquals("08", expectedMmo.getLicense(), sp.getLicense());
      assertEquals("09", expectedMmo.getCollectionType(), sp.getCollectionType());
      assertEquals("10", expectedMmo.getTitle(), sp.getTitle());
      assertEquals(
          "11", expectedMmo.getAssociatedSpecimenReference(), sp.getAssociatedSpecimenReference());
      assertEquals("12", expectedMmo.isMultiMediaPublic(), sp.isMultiMediaPublic());
      assertEquals(
          "13",
          expectedMmo.getServiceAccessPoints().get(0).getAccessUri(),
          sp.getServiceAccessPoints().get(0).getAccessUri());
      assertEquals(
          "14",
          expectedMmo.getServiceAccessPoints().get(0).getFormat(),
          sp.getServiceAccessPoints().get(0).getFormat());
      assertEquals(
          "15",
          expectedMmo.getServiceAccessPoints().get(0).getVariant(),
          sp.getServiceAccessPoints().get(0).getVariant());

      // Test the ServiceAccessPoint of the 2nd Mm object
      expectedMmo = new MultiMediaObject();
      serviceAccessPoints.clear();
      expectedUri =
          URI.create("https://medialib.naturalis.nl/file/id/RMNH.INS.867435_2/format/large");
      expectedFormat = "image/jpeg";
      expectedVariant = "ac:GoodQuality";
      serviceAccessPoints.add(new ServiceAccessPoint(expectedUri, expectedFormat, expectedVariant));
      expectedMmo.setServiceAccessPoints(serviceAccessPoints);

      sp = transformed.get(1);
      assertEquals(
          "16",
          expectedMmo.getServiceAccessPoints().get(0).getAccessUri(),
          sp.getServiceAccessPoints().get(0).getAccessUri());
      assertEquals(
          "17",
          expectedMmo.getServiceAccessPoints().get(0).getFormat(),
          sp.getServiceAccessPoints().get(0).getFormat());
      assertEquals(
          "18",
          expectedMmo.getServiceAccessPoints().get(0).getVariant(),
          sp.getServiceAccessPoints().get(0).getVariant());
    }
  }

  /**
   * Test method for {@link CrsMultiMediaTransformer#initialize}
   *
   * @throws Exception
   *
   *         Test to verify if the initialize method returns the expected {@link MultiMediaObject} object
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testDoTransfer() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    MultiMediaObject mmo = null;

    for (XMLRecordInfo extracted : extractor) {
      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID", "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input", extracted);

      String collectionType = DOMUtil.getDescendantValue(oaiDcElem, "abcd:CollectionType");
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer,
          "collectionType", collectionType);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      Object obj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "getIdentifications",
              new Class[] {List.class},
              ncsrDeterminationElems
      );
      ArrayList<MultiMediaContentIdentification> identifications = (ArrayList<MultiMediaContentIdentification>) obj;

      Object mmoObj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "initialize",
              new Class[] {Element.class, ArrayList.class},
              oaiDcElem, identifications
      );
      mmo = (MultiMediaObject) mmoObj;
    }

    MultiMediaObject expectedMmo = new MultiMediaObject();
    expectedMmo.setSourceInstitutionID("Naturalis Biodiversity Center");
    expectedMmo.setSourceID("CRS");
    expectedMmo.setOwner("Naturalis Biodiversity Center");
    expectedMmo.setLicense(CC0_10);
    expectedMmo.setCollectionType("Diptera");
    expectedMmo.setAssociatedSpecimenReference("RMNH.INS.867435@CRS");

    assertNotNull("01", mmo);
    assertEquals("02", expectedMmo.getSourceInstitutionID(), mmo.getSourceInstitutionID());
    assertEquals("03", expectedMmo.getSourceID(), mmo.getSourceID());
    assertEquals("04", expectedMmo.getOwner(), mmo.getOwner());
    assertEquals("05", expectedMmo.getLicense(), mmo.getLicense());
    assertEquals("06", expectedMmo.getCollectionType(), mmo.getCollectionType());
    assertEquals("07", expectedMmo.getAssociatedSpecimenReference(), mmo.getAssociatedSpecimenReference());
    // If not explicitly set, Boolean multiMediaPublic = null;
    assertEquals("08", expectedMmo.isMultiMediaPublic(), mmo.isMultiMediaPublic());
    // Test van setDefaultKingdom: kingdom wordt voorspeld op basis van het collectionType.
    assertEquals("09", "Animalia", mmo.getIdentifications().get(0).getDefaultClassification().getKingdom());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getIdentifications(List elements)}.
   * and verifies whether the initialize method returns the expected
   * list of {@link ArrayList<MultiMediaContentIdentification>}
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetIdentifications() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    ArrayList<MultiMediaContentIdentification> ids = null;
    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID", "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input", extracted);

      String collectionType = DOMUtil.getDescendantValue(oaiDcElem, "abcd:CollectionType");
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer,
          "collectionType", collectionType);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      Object obj =
          ReflectionUtil.call(
              crsMultiMediaTransformer,
              "getIdentifications",
              new Class[] {List.class},
              ncsrDeterminationElems);

      ids = (ArrayList<MultiMediaContentIdentification>) obj;
    }

    assertNotNull(ids);

    // Assert ScientificName Object values
    ids.stream().map(i -> i.getScientificName().getFullScientificName())
        .findFirst()
        .ifPresent(s -> assertEquals("Aedes kabaenensis", s));

    ids.stream().map(i -> i.getScientificName().getGenusOrMonomial())
        .findFirst()
        .ifPresent(s -> assertEquals("Aedes", s));

    ids.stream().map(i -> i.getScientificName().getSpecificEpithet())
        .findFirst()
        .ifPresent(s -> assertEquals("kabaenensis", s));

    // Assert DefaultClassification Object values
    ids.stream().map(i -> i.getDefaultClassification().getGenus())
        .findFirst()
        .ifPresent(s -> assertEquals("Aedes", s));

    ids.stream().map(i -> i.getDefaultClassification().getSpecificEpithet())
        .findFirst()
        .ifPresent(s -> assertEquals("kabaenensis", s));
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getGatheringEvent(Element oaiDcElem)}.
   *
   * @throws Exception
   *
   *         Test to verify if the getGatheringEvent method returns the expected {@link GatheringEvent} object
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetGatheringEvent() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    GatheringEvent ge = null;
    for (XMLRecordInfo extracted : extractor) {

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");

      Object obj = ReflectionUtil.call(
          crsMultiMediaTransformer,
          "getGatheringEvent",
          new Class[] {Element.class},
          oaiDcElem);

      ge = (GatheringEvent) obj;
    }

    GatheringEvent expectedGe = new GatheringEvent();
    expectedGe.setWorldRegion("Asia");
    expectedGe.setCountry("Indonesia");
    expectedGe.setProvinceState("Southeast Sulawesi");
    expectedGe.setSublocality("Kabaena, Pulau");
    Double longitudeDecimal = 14.3405556;
    Double latitudeDecimal = 35.9241667;
    Integer coordinateErrorDistanceInMeters = 2500;
    String altitude = "500";
    String altitudeUnit = "Meter";
    String depth = "0.5-1.0";
    String depthUnit = "m";

    assertNotNull("01", ge);
    assertEquals("02", expectedGe.getCountry(), ge.getCountry());
    assertEquals("03", expectedGe.getWorldRegion(), ge.getWorldRegion());
    assertEquals("04", expectedGe.getProvinceState(), ge.getProvinceState());
    assertEquals("05", expectedGe.getSublocality(), ge.getSublocality());
    assertNotNull("06", ge.getSiteCoordinates());
    GatheringSiteCoordinates siteCoordinates = ge.getSiteCoordinates().get(0);
    assertEquals("07", latitudeDecimal, siteCoordinates.getLatitudeDecimal());
    assertEquals("08", longitudeDecimal, siteCoordinates.getLongitudeDecimal());
    assertEquals("09", coordinateErrorDistanceInMeters, siteCoordinates.getCoordinateErrorDistanceInMeters());
    assertEquals("10", altitude, ge.getAltitude());
    assertEquals("11", altitudeUnit, ge.getAltitudeUnifOfMeasurement());
    assertEquals("12", depth, ge.getDepth());
    assertEquals("13", depthUnit, ge.getDepthUnitOfMeasurement());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getScientificName(Element oaiDcElem)}.
   * Verifies if the getScientificName method returns the expected {@link ScientificName} object
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetScientificName() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    ScientificName sn = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    outerloop:
    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID", "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input", extracted);

      String collectionType = DOMUtil.getDescendantValue(oaiDcElem, "abcd:CollectionType");
      CommonReflectionUtil.setField(
          CrsMultiMediaTransformer.class,
          crsMultiMediaTransformer,
          "collectionType", collectionType);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      for (Element element : ncsrDeterminationElems) {
        String fullScientificNameStr = DOMUtil.getDescendantValue(element, "dwc:scientificName");
        assertNotNull(fullScientificNameStr);
        Object obj = ReflectionUtil.call(
            crsMultiMediaTransformer,
            "getScientificName",
            new Class[] {Element.class}, element);
        sn = (ScientificName) obj;
        if (sn != null && fullScientificNameStr.equals("Aedes kabaenensis")) break outerloop;
      }
      ScientificName expectedSn = new ScientificName();
      expectedSn.setFullScientificName("Aedes kabaenensis");
      expectedSn.setGenusOrMonomial("Aedes");
      expectedSn.setScientificNameGroup("aedes kabaenensis");

      assertNotNull(sn);
      assertEquals("01", expectedSn.getFullScientificName(), sn.getFullScientificName());
      assertEquals("02", expectedSn.getGenusOrMonomial(), sn.getGenusOrMonomial());
      assertEquals("03", expectedSn.getScientificNameGroup(), sn.getScientificNameGroup());
    }
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getScientificName(Element element, String collectionType)},
   * but aimed at testing the construction of the full scientific name.
   *
   * @throws Exception
   *
   * Test method to verify the getIdentification method returns an expected ScientificName object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetScientificName_02() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    ScientificName scientificName;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element rootElement = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(rootElement, "ncrsDetermination");
      assertNotNull(elems);

      for (Element element : elems) {

        String fullScientificNameStr = DOMUtil.getDescendantValue(element, "dwc:scientificName");
        assertNotNull(fullScientificNameStr);

        String collectionType = DOMUtil.getDescendantValue(rootElement, "abcd:CollectionType");
        CommonReflectionUtil.setField(
            CrsMultiMediaTransformer.class,
            crsMultiMediaTransformer,
            "collectionType", collectionType);

        Object scientificNameObj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getScientificName",
                new Class[] {Element.class},
                element);
        scientificName = (ScientificName) scientificNameObj;

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest01")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("Aaaaa (bbbbb) ccccc ddddd Abc1234");
          assertEquals(
              "01", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest02")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("Aaaaa (bbbbb) ccccc eeeee Abc1234");
          assertEquals(
              "02", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest03")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("fullscientificnametest03");
          assertEquals(
              "03", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        String test = DOMUtil.getDescendantValue(element, "abcd:IdentificationQualifier3");
        assertNotNull(test);

        if (scientificName != null && test.equals("fullscientificnametest04")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("TAXONCOVERAGE");
          assertEquals(
              "04", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }
      }
    }
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getTitle(Element element, String unitId)}.
   * Verifies if the getTitle method returns the expected Title
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetTitle() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input", extracted);

      List<Element> frmDigitaleBestandenElems = getDescendants(oaiDcElem, "frmDigitalebestanden");
      assert frmDigitaleBestandenElems != null;

      Element frmDigitaleBestandenElem = frmDigitaleBestandenElems.get(0);
      Object obj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "getTitle",
              new Class[] {Element.class, String.class},
              frmDigitaleBestandenElem, "RMNH.INS.867435");
      String title = (String) obj;

      assertNotNull("01", obj);
      assertEquals("02", "RMNH.INS.867435", title);
    }
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getQualifiers(Element element)}.
   * Verifies if the getQualifiers method returns the expected {List<@Qualifiers>} object.

   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetQualifiers() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<String> qualifiers = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assert ncsrDeterminationElems != null;

      for (Element element : ncsrDeterminationElems) {
        Object obj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getQualifiers",
                new Class[] {Element.class},
                element);
        qualifiers = (List<String>) obj;
      }
    }
    String expectedQulifierName1 = "TestQualifier1";
    String expectedQualifierName2 = "TestQualifier2";

    assertNotNull("01", qualifiers);
    assertNotNull("02", qualifiers.get(0));
    assertEquals( "03", expectedQulifierName1, qualifiers.get(0));
    assertNotNull("04", qualifiers.get(1));
    assertEquals( "05", expectedQualifierName2, qualifiers.get(1));
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getVernacularNames(Element element)}.
   * Verifies if the getVernacularNames method returns the expected {List<@VernacularName>} object.
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetVernacularNames() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<VernacularName> vernacularNames = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      for (Element element : ncsrDeterminationElems) {
        Object obj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getVernacularNames",
                new Class[] {Element.class},
                element);
        vernacularNames = (List<VernacularName>) obj;
      }
    }
    String expectedVernecularName = "TestVernacularName";
    assertNotNull("01", vernacularNames);
    assertEquals( "02", 1, vernacularNames.size());
    assertEquals( "03", expectedVernecularName, vernacularNames.get(0).getName());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getPhaseOrStage(Element element)}.
   * Verifies if the getPhaseOrStage method returns the expected String (Phase or Stage) object.
   *
   *  @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetPhaseOrStage() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    String phaseOrStage = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncrsdatagroup = getDescendants(oaiDcElem, "ncrsdatagroup");
      assertNotNull(ncrsdatagroup);

      for (Element element : ncrsdatagroup) {
        Object obj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getPhaseOrStage",
                new Class[] {Element.class},
                element);
        phaseOrStage = (String) obj;
      }
    }
    String expectedPhase = "embryo";
    assertNotNull("01", phaseOrStage);
    assertEquals( "02", expectedPhase, phaseOrStage);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getTypeStatus(Element element)}.
   * Verifies if the getTypeStatus method returns the expected {@link SpecimenTypeStatus} object.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetTypeStatus() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    SpecimenTypeStatus specimenTypeStatus = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      for (Element element : ncsrDeterminationElems) {
        Object obj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getTypeStatus",
                new Class[] {Element.class},
                element);
        specimenTypeStatus = (SpecimenTypeStatus) obj;
      }
    }

    String expectedSpecimenTypeStatus = "ISOTYPE";
    assertNotNull("01", specimenTypeStatus);
    assertEquals( "02", expectedSpecimenTypeStatus, specimenTypeStatus.name());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#getSex(Element element)}.
   * Verifies if the getTypeStatus method returns the expected {@link SpecimenTypeStatus} object.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetSex() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    String sex = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncrsdatagroup = getDescendants(oaiDcElem, "ncrsdatagroup");
      assertNotNull(ncrsdatagroup);

      for (Element element : ncrsdatagroup) {
        Object obj =
            ReflectionUtil.call(
                crsMultiMediaTransformer,
                "getSex",
                new Class[] {Element.class},
                element);
        sex = (String) obj;
      }
    }

    String expectedSex = "male";
    assertNotNull("01", expectedSex);
    assertEquals( "02", expectedSex, sex);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#hasTestGenus(List elements)}
   * Verifies if the hasTestGenus method returns the expected boolean value.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testHasTestGenus_01() throws Exception {

    System.setProperty("nl.naturalis.nba.etl.testGenera", "aedes");
    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean testHasTestGenus = false;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      Object obj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "hasTestGenus",
              new Class[] {List.class},
              ncsrDeterminationElems
      );
      testHasTestGenus = (boolean) obj;
      if (testHasTestGenus) {
        break;
      }
    }
    assertTrue(testHasTestGenus);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#hasTestGenus(List elements)}
   * Verifies if the hasTestGenus method returns the expected boolean value.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testHasTestGenus_02() throws Exception {

    System.setProperty("nl.naturalis.nba.etl.testGenera", "test");
    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean testHasTestGenus = true;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncsrDeterminationElems = getDescendants(oaiDcElem, "ncrsDetermination");
      assertNotNull(ncsrDeterminationElems);

      Object obj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "hasTestGenus",
              new Class[] {List.class},
              ncsrDeterminationElems
      );
      testHasTestGenus = (boolean) obj;
      if (testHasTestGenus) {
        break;
      }
    }
    assertFalse(testHasTestGenus);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#val(Element element, String s)}.
   * Verifies if the val method returns the expected String value based on the value of a tag.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testVal() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    String val = null;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncrsdatagroup = getDescendants(oaiDcElem, "ncrsdatagroup");
      assertNotNull(ncrsdatagroup);

      Element frmDigitaleBestandenElem = ncrsdatagroup.get(0);
      Object obj =
          ReflectionUtil.call(
              crsMultiMediaTransformer,
              "val",
              new Class[] {Element.class, String.class},
              frmDigitaleBestandenElem, "dwc:sex");
      val = (String) obj;
    }
    assertNotNull("01", val);
    assertEquals( "02", "male", val);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#dval(Element element, String s)}.
   * Verifies if the dval method returns the expected Double value based on the value of a tag.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testDval() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    Double dval = 0.0;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> ncrsGatheringSites = getDescendants(oaiDcElem, "ncrsGatheringSites");
      assertNotNull(ncrsGatheringSites);

      Element element = ncrsGatheringSites.get(0);
      Object obj =
          ReflectionUtil.call(
              crsMultiMediaTransformer,
              "dval",
              new Class[] {Element.class, String.class},
              element, "dwc:decimalLongitude");
      dval = (Double) obj;
    }
    Double expectedDouble = 14.3405556;
    assertNotNull("01", dval);
    assertEquals( "02", expectedDouble, dval);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#bval(Element element, String s)}.
   * Verifies if the bval method returns the expected boolean value based on the value of a tag.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testBval() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean bval = false;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    for (XMLRecordInfo extracted : extractor) {

      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "objectID",
          "RMNH.INS.867435");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsMultiMediaTransformer,
          "input",
          extracted);

      List<Element> frmDigitaleBestandenElems = getDescendants(oaiDcElem, "frmDigitalebestanden");
      assertNotNull(frmDigitaleBestandenElems);

      Element frmDigitaleBestandenElem = frmDigitaleBestandenElems.get(0);
      Object obj = ReflectionUtil.call(
              crsMultiMediaTransformer,
              "bval",
              new Class[] {Element.class, String.class},
          frmDigitaleBestandenElem, "abcd:MultiMediaPublic"
      );
      bval = (boolean) obj;
    }
    assertTrue(bval);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsMultiMediaTransformer#skipRecord()}.
   * Verifies if the skipRecord method returns the expected boolean value.
   *
   * @throws Exception e
   */
  @Test
  public void testSkipRecord() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean bval = true;
    CrsMultiMediaTransformer crsMultiMediaTransformer = new CrsMultiMediaTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(multimediaFile, etlStatistics);
    for (XMLRecordInfo extracted : extractor) {

      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsMultiMediaTransformer, "objectID", "RMNH.INS.867435");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsMultiMediaTransformer, "input", extracted);

      Object obj =
          ReflectionUtil.call(
              crsMultiMediaTransformer, "skipRecord", new Class[] {}, new Object[] {});
      bval = (boolean) obj;
    }
    assertFalse(bval);
  }

  @Test
  public void testSetDefaultKingdom() {
    MultiMediaObject multiMediaObject = new MultiMediaObject();

    String collectionType = "Paleobotany";
    multiMediaObject.setCollectionType(collectionType);
    MultiMediaContentIdentification identification = new MultiMediaContentIdentification();
    DefaultClassification classification = new DefaultClassification();
    classification.setGenus("aaa");
    identification.setDefaultClassification(classification);
    List<MultiMediaContentIdentification> identifications = new ArrayList<>();
    identifications.add(identification);
    multiMediaObject.setIdentifications(identifications);
    CrsMultiMediaTransformer.setDefaultKingdom(multiMediaObject);
    assertEquals("01", "Plantae", multiMediaObject.getIdentifications().get(0).getDefaultClassification().getKingdom());

    collectionType = "Mineralogy";
    multiMediaObject.setCollectionType(collectionType);
    classification = new DefaultClassification();
    classification.setGenus("aaa");
    identification.setDefaultClassification(classification);
    identifications = new ArrayList<>();
    identifications.add(identification);
    multiMediaObject.setIdentifications(identifications);
    CrsMultiMediaTransformer.setDefaultKingdom(multiMediaObject);
    assertNull("02", multiMediaObject.getIdentifications().get(0).getDefaultClassification().getKingdom());

    collectionType = "Something different";
    multiMediaObject.setCollectionType(collectionType);
    classification = new DefaultClassification();
    classification.setGenus("aaa");
    identification.setDefaultClassification(classification);
    identifications = new ArrayList<>();
    identifications.add(identification);
    multiMediaObject.setIdentifications(identifications);
    CrsMultiMediaTransformer.setDefaultKingdom(multiMediaObject);
    assertEquals("03", "Animalia", multiMediaObject.getIdentifications().get(0).getDefaultClassification().getKingdom());
  }
}
