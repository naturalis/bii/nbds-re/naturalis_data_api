package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.utils.xml.DOMUtil.getDescendant;
import static nl.naturalis.nba.utils.xml.DOMUtil.getDescendants;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.Agent;
import nl.naturalis.nba.api.model.AreaClass;
import nl.naturalis.nba.api.model.AssociatedTaxon;
import nl.naturalis.nba.api.model.BioStratigraphy;
import nl.naturalis.nba.api.model.ChronoStratigraphy;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.GatheringSiteCoordinates;
import nl.naturalis.nba.api.model.LithoStratigraphy;
import nl.naturalis.nba.api.model.Monomial;
import nl.naturalis.nba.api.model.NamedArea;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.Sex;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.SpecimenIdentification;
import nl.naturalis.nba.api.model.SpecimenTypeStatus;
import nl.naturalis.nba.api.model.TaxonRelationType;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AbstractTransformer;
import nl.naturalis.nba.etl.AllTests;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.XMLRecordInfo;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.utils.CommonReflectionUtil;
import nl.naturalis.nba.utils.reflect.ReflectionUtil;
import nl.naturalis.nba.utils.xml.DOMUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

/**
 * Test class for CrsSpecimenTransformer.
 */
@SuppressWarnings({"unchecked"})
public class CrsSpecimenTransformerTest {

  URL specimenFileUrl;
  File specimenFile;

  @Before
  public void setUp() {

    System.setProperty(
        "nl.naturalis.nba.etl.testGenera",
        "malus,parus,larus,bombus,rhododendron,felix,tulipa,rosa,canis,passer,trientalis");
    specimenFileUrl = AllTests.class.getResource("specimens.20140701000000.000008.oai.xml");
    assert specimenFileUrl != null;
    specimenFile = new File(specimenFileUrl.getFile());
  }

  @After
  public void tearDown() {}

  /**
   * Test method to verify the doTransform method
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#doTransform()}
   * returns expected Specimen objected.
   *
   * @throws Exception by CommonReflectionUtil
   *
   *
   */
  @Test
  @SuppressWarnings("ExtractMethodRecommender")
  public void testDoTransform() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<Specimen> list;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.MAM.TT.5");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      Object returned = CommonReflectionUtil.callMethod(
              null,
              null,
              crsSpecimenTransformer,
              "doTransform");

      list = (List<Specimen>) returned;
      Specimen sp = list.get(0);

      List<String> previousSourceIds = new ArrayList<>();
      previousSourceIds.add("AAA");
      previousSourceIds.add("OOO");

      Specimen expectedSpecimen = new Specimen();
      expectedSpecimen.setSourceID("CRS");
      expectedSpecimen.setId("RMNH.MAM.TT.5@CRS");
      expectedSpecimen.setUnitGUID("https://data.biodiversitydata.nl/naturalis/specimen/RMNH.MAM.TT.5");
      expectedSpecimen.setOwner("Naturalis Biodiversity Center");
      expectedSpecimen.setTitle("RMNH.ART");
      expectedSpecimen.setKindOfUnit("WholeOrganism");
      expectedSpecimen.setCollectionType("PreservedSpecimen");
      expectedSpecimen.setNumberOfSpecimen(1);
      expectedSpecimen.setPreviousSourceID(previousSourceIds);
      expectedSpecimen.setMultiMediaPublic(true);

      assertNotNull("01", sp);
      assertEquals("02", expectedSpecimen.getSourceID(), sp.getSourceID());
      assertEquals("03", expectedSpecimen.getId(), sp.getId());
      assertEquals("04", expectedSpecimen.getUnitGUID(), sp.getUnitGUID());
      assertEquals("05", expectedSpecimen.getOwner(), sp.getOwner());
      assertEquals("06", expectedSpecimen.getTitle(), sp.getTitle());
      assertEquals("07", expectedSpecimen.getKindOfUnit(), sp.getKindOfUnit());
      assertEquals("08", expectedSpecimen.getCollectionType(), sp.getCollectionType());
      assertEquals("09", expectedSpecimen.getNumberOfSpecimen(), sp.getNumberOfSpecimen());
      assertEquals("10", expectedSpecimen.getPreviousSourceID(), sp.getPreviousSourceID());
      assertEquals("11", expectedSpecimen.isMultiMediaPublic(), sp.isMultiMediaPublic());
      assertTrue("12", sp.isFromCaptivity());
      assertEquals("13", "Animalia", sp.getIdentifications().get(0).getDefaultClassification().getKingdom());
    }
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getIdentification(Element element)}.
   *
   * @throws Exception
   *
   *         Test method to verify the getIdentification method returns an expected
   *         {@link SpecimenIdentification} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetIdentification() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    SpecimenIdentification identification = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    outerloop:
    for (XMLRecordInfo extracted : extractor) {
      Element rootElement = extracted.getRecord();
      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class,
          crsSpecimenTransformer,
          "collectionType",
          "Arts");

      List<Element> elems = DOMUtil.getDescendants(rootElement, "ncrsDetermination");
      assertNotNull(elems); // Make sure the test record contains at least one determination

      Object obj = CommonReflectionUtil.callMethod(
          elems,
          List.class,
          crsSpecimenTransformer,
          "hasTestGenus");

      boolean check = (boolean) obj;
      if (check) {
        for (Element element : elems) {
          String fullScientificNameStr = DOMUtil.getDescendantValue(element, "abcd:FullScientificNameString");
          Object identificationObj = ReflectionUtil.call(
                  crsSpecimenTransformer,
                  "getIdentification",
                  new Class[] {Element.class},
                  element);
          identification = (SpecimenIdentification) identificationObj;
          if (identification != null && fullScientificNameStr != null && fullScientificNameStr.equals("Cleyera japonica"))
            break outerloop;
        }
      }
    }

    SpecimenIdentification expectedSid = new SpecimenIdentification();
    expectedSid.setRockType("Test_Rock");
    expectedSid.setAssociatedFossilAssemblage("Test_Assemblage");
    expectedSid.setRockMineralUsage("Test_Minerals");
    expectedSid.addIdentifier(new Agent("Co de Boswachter"));

    assertNotNull("01", identification);
    assertTrue(   "02", identification.isPreferred());
    assertEquals( "03", expectedSid.getRockType(), identification.getRockType());
    assertEquals( "04", expectedSid.getAssociatedFossilAssemblage(),
        identification.getAssociatedFossilAssemblage());
    assertEquals( "05", expectedSid.getRockMineralUsage(), identification.getRockMineralUsage());
    assertEquals( "06", expectedSid.getIdentifiers().get(0).getAgentText(),
        identification.getIdentifiers().get(0).getAgentText());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getGatheringEvent()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getGatheringEvent method returns an expected
   *         {@link GatheringEvent} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetGatheringEvent() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    GatheringEvent event = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.MAM.TT.5");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class,
          crsSpecimenTransformer,
          "record",
          extracted.getRecord());

      Object returned =
          CommonReflectionUtil.callMethod(
              null,
              null,
              crsSpecimenTransformer,
              "getGatheringEvent");

      event = (GatheringEvent) returned;
      if (event != null) {
        break;
      }
    }

    GatheringEvent expectedGa = new GatheringEvent();
    expectedGa.setCountry("Netherlands");
    expectedGa.setProvinceState("Noord Holland");
    expectedGa.setLocality("Amsterdam");
    Double longitudeDecimal = 5.10234;
    Double latitudeDecimal = 52.45863;
    Integer coordinateErrorDistanceInMeters = 2500;
    String altitude = "0.55-0.75";
    String altitudeUnit = "m.-NAP";
    String depth = "0.25-0.5";
    String depthUnit = "m";
    OffsetDateTime collectingStartDate = OffsetDateTime.parse("1969-09-06T00:00Z");
    OffsetDateTime collectingEndDate = OffsetDateTime.parse("1969-11-14T00:00Z");
    String collectingDateString = "06 Sep 1969 t/m 14 Nov 1969";

    assertNotNull("01", event);
    assertEquals("02", expectedGa.getCountry(), event.getCountry());
    assertEquals("03", expectedGa.getProvinceState(), event.getProvinceState());
    assertEquals("04", expectedGa.getLocality(), event.getLocality());
    assertNotNull("05", event.getSiteCoordinates());
    GatheringSiteCoordinates siteCoordinates = event.getSiteCoordinates().get(0);
    assertEquals("06", latitudeDecimal, siteCoordinates.getLatitudeDecimal());
    assertEquals("07", longitudeDecimal, siteCoordinates.getLongitudeDecimal());
    assertEquals("08", coordinateErrorDistanceInMeters, siteCoordinates.getCoordinateErrorDistanceInMeters());
    assertEquals("09", altitude, event.getAltitude());
    assertEquals("10", altitudeUnit, event.getAltitudeUnifOfMeasurement());
    assertEquals("11", depth, event.getDepth());
    assertEquals("12", depthUnit, event.getDepthUnitOfMeasurement());
    assertEquals("13", collectingStartDate, event.getDateTimeBegin());
    assertEquals("14", collectingEndDate, event.getDateTimeEnd());
    assertEquals("15", collectingDateString, event.getDateText());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getScientificName(Element element, String collectionType)}.
   *
   * @throws Exception
   *
   * Test method to verify the getIdentification method returns an expected ScientificName object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetScientificName() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    ScientificName scientificName = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);
    outerloop:
    for (XMLRecordInfo extracted : extractor) {
      Element rootElement = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(rootElement, "ncrsDetermination");
      assertNotNull(elems);

      for (Element element : elems) {
        String fullScientificNameStr = DOMUtil.getDescendantValue(element, "abcd:FullScientificNameString");
        Object scientificNameObj =
            ReflectionUtil.call(
                crsSpecimenTransformer,
                "getScientificName",
                new Class[] {Element.class, String.class},
                element, "Arts");
        scientificName = (ScientificName) scientificNameObj;
        if (scientificName != null && fullScientificNameStr != null && fullScientificNameStr.equals("Cleyera japonica"))
          break outerloop;
      }
    }
    ScientificName expectedSn = new ScientificName();
    expectedSn.setFullScientificName("Cleyera japonica");
    expectedSn.setGenusOrMonomial("Cleyera");
    expectedSn.setSpecificEpithet("japonica");
    expectedSn.setScientificNameGroup("cleyera japonica");

    assertNotNull("01", scientificName);
    assertEquals( "02", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
    assertEquals( "03", expectedSn.getGenusOrMonomial(), scientificName.getGenusOrMonomial());
    assertEquals( "04", expectedSn.getSpecificEpithet(), scientificName.getSpecificEpithet());
    assertEquals( "05", expectedSn.getScientificNameGroup(), scientificName.getScientificNameGroup());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getScientificName(Element element, String collectionType)},
   * but aimed at testing the construction of the full scientific name.
   *
   * @throws Exception
   *
   * Test method to verify the getIdentification method returns an expected ScientificName object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetScientificName_02() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    ScientificName scientificName;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      Element rootElement = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(rootElement, "ncrsDetermination");
      assertNotNull(elems);

      for (Element element : elems) {
        String fullScientificNameStr = DOMUtil.getDescendantValue(element, "abcd:FullScientificNameString");
        Object scientificNameObj =
            ReflectionUtil.call(
                crsSpecimenTransformer,
                "getScientificName",
                new Class[] {Element.class, String.class},
                element, "Botany");
        scientificName = (ScientificName) scientificNameObj;

        assertNotNull("01", fullScientificNameStr);

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest01")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("Aaaaa (bbbbb) ccccc ddddd Zzz1234");
          assertEquals("02", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest02")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("Aaaaa (bbbbb) ccccc eeeee Zzz1234");
          assertEquals("03", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        if (scientificName != null && fullScientificNameStr.equals("fullscientificnametest03")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("fullscientificnametest03");
          assertEquals("04", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }

        String test = DOMUtil.getDescendantValue(element, "abcd:InformalNameString");
        if (scientificName != null && test != null && test.equals("fullscientificnametest04")) {
          ScientificName expectedSn = new ScientificName();
          expectedSn.setFullScientificName("Cypraeovulinae");
          assertEquals("04", expectedSn.getFullScientificName(), scientificName.getFullScientificName());
        }
      }
    }
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getSystemClassification(Element element, ScientificName scientificName)}.
   *
   * @throws Exception
   *
   *         Test method to verify the getSystemClassification method returns an expected
   *         List<{@link Monomial}> object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetSystemClassification() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    int listSize = 0;
    List<Monomial> classification = null;
    ScientificName scientificName = new ScientificName();
    scientificName.setFullScientificName("Cleyera japonica");
    scientificName.setGenusOrMonomial("Cleyera");
    scientificName.setSpecificEpithet("japonica");
    scientificName.setAuthor("Plabon");
    scientificName.setScientificNameGroup("cleyera japonica");
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);
    outerloop:
    for (XMLRecordInfo extracted : extractor) {
      Element elemet = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(elemet, "ncrsDetermination");
      assertNotNull(elems);

      for (Element element : elems) {
        Object scientificNameObj =
            ReflectionUtil.callStatic(
                crsSpecimenTransformer.getClass(),
                "getSystemClassification",
                new Class[] {Element.class, ScientificName.class},
                element, scientificName);
        classification = (List<Monomial>) scientificNameObj;
        listSize = classification.size();
        if (listSize > 0) break outerloop;
      }
    }

    Monomial expectedMono = new Monomial();
    expectedMono.setName("Cleyera");
    expectedMono.setRank("genus");

    assertEquals( "01", 2, listSize);
    assertNotNull("02", classification);
    assertEquals( "03", expectedMono.getName(), classification.get(0).getName());
    assertEquals( "04", expectedMono.getRank(), classification.get(0).getRank());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getChronoStratigraphyList()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getChronoStratigraphyList method returns an expected
   *         {@link ChronoStratigraphy} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetChronoStratigraphyList() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<ChronoStratigraphy> actualList = null;
    int listSize = 0;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class, crsSpecimenTransformer, "record", extracted.getRecord());

      Object returned =
          CommonReflectionUtil.callMethod(
              null, null, crsSpecimenTransformer, "getChronoStratigraphyList");
      actualList = (List<ChronoStratigraphy>) returned;
      listSize = actualList.size();
      if (listSize > 0) break;
    }

    ChronoStratigraphy expectedCsg = new ChronoStratigraphy();
    expectedCsg.setYoungRegionalSubstage("Test Regional Sub-Stage");
    expectedCsg.setYoungRegionalStage("Test Regional Stage");
    expectedCsg.setYoungChronoName("CRETACEOUS");

    assertNotNull("01", actualList);
    assertEquals( "02", 1, listSize);
    assertEquals( "03", expectedCsg.getYoungRegionalSubstage(), actualList.get(0).getYoungRegionalSubstage());
    assertEquals( "04", expectedCsg.getYoungRegionalStage(), actualList.get(0).getYoungRegionalStage());
    assertEquals( "05", expectedCsg.getYoungChronoName(), actualList.get(0).getYoungChronoName());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getBioStratigraphyList()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getBioStratigraphyList method returns an expected
   *         {@link BioStratigraphy} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetBioStratigraphyList() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<BioStratigraphy> actualList = null;
    int listSize = 0;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class, crsSpecimenTransformer, "record", extracted.getRecord());

      Object returned = CommonReflectionUtil.callMethod(
              null, null, crsSpecimenTransformer, "getBioStratigraphyList");
      actualList = (List<BioStratigraphy>) returned;
      listSize = actualList.size();
      if (listSize > 0) break;
    }
    BioStratigraphy expectedBs = new BioStratigraphy();
    expectedBs.setYoungBioName("Test Bio Name");
    expectedBs.setYoungFossilZone("Test Young Fossil Zone");
    expectedBs.setOldBioName("Hiatella arctica Acme Zone");

    assertNotNull("01", actualList);
    assertEquals( "02", 2, listSize);
    assertEquals( "03", expectedBs.getYoungBioName(), actualList.get(0).getYoungBioName());
    assertEquals( "04", expectedBs.getYoungFossilZone(), actualList.get(0).getYoungFossilZone());
    assertEquals( "05", expectedBs.getOldBioName(), actualList.get(0).getOldBioName());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getLithoStratigraphyList()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getLithoStratigraphyList method returns an expected
   *         {@link LithoStratigraphy} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetLithoStratigraphyList() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<LithoStratigraphy> actualList = null;
    int listSize = 0;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class, crsSpecimenTransformer, "record", extracted.getRecord());

      Object returned =
          CommonReflectionUtil.callMethod(
              null, null, crsSpecimenTransformer, "getLithoStratigraphyList");
      actualList = (List<LithoStratigraphy>) returned;
      listSize = actualList.size();
      if (listSize > 0) break;
    }

    LithoStratigraphy expectedLs = new LithoStratigraphy();
    expectedLs.setQualifier("Test Qualifier");
    expectedLs.setBed("Test Bed");
    expectedLs.setInformalName("Test InfoName");

    assertNotNull("01", actualList);
    assertEquals( "02", 2, listSize);
    assertEquals( "03", expectedLs.getQualifier(), actualList.get(1).getQualifier());
    assertEquals( "04", expectedLs.getBed(), actualList.get(1).getBed());
    assertEquals( "05", expectedLs.getInformalName(), actualList.get(1).getInformalName());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getPhaseOrStage()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getPhaseOrStage method returns an expected
   *         {@link PhaseOrStage} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetPhaseOrStage() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    String actualStage = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);

      Object returned = CommonReflectionUtil.callMethod(
              null,
              null,
              crsSpecimenTransformer,
              "getPhaseOrStage");
      actualStage = (String) returned;
      if (actualStage != null) break;
    }

    String expectedStage = "embryo";
    assertEquals(expectedStage, actualStage);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getTypeStatus(Element element)}.
   *
   * @throws Exception
   *
   *         Test method to verify the getTypeStatus method returns an expected
   *         {@link SpecimenTypeStatus} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetTypeStatus() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    String specimenTypeStatus = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);

      Element elemet = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(elemet, "ncrsDetermination");
      assertNotNull(elems);

      for (Element element : elems) {
        Object returned = CommonReflectionUtil.callMethod(
            element,
            Element.class,
            crsSpecimenTransformer,
            "getTypeStatus");
        if (returned != null) {
          specimenTypeStatus = returned.toString();
          break;
        }
      }
    }

    String expectedSpecimenTypeStatus = "paralectotype";
    assertNotNull("01", specimenTypeStatus);
    assertEquals( "02",expectedSpecimenTypeStatus, specimenTypeStatus);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getSex()}.
   *
   * @throws Exception
   *
   *         Test method to verify the getSex method returns an expected {@link Sex} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetSex() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    Sex sex = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);

      Object returned =
          CommonReflectionUtil.callMethod(null, null, crsSpecimenTransformer, "getSex");
      sex = (Sex) returned;
      if (sex != null) break;
    }

    String expectedStage = "male";
    assertNotNull("01", sex);
    assertEquals( "02",expectedStage, sex.toString());
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#date(Element element, String tag)}.
   *
   * @throws Exception
   *
   *         Test method to verify the date method returns an expected {@link OffsetDateTime} object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testDate() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    OffsetDateTime offsetDateTime = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    outerloop:
    for (XMLRecordInfo extracted : extractor) {
      Element element = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(element, "ncrsDetermination");
      assertNotNull(elems);

      for (Element elem : elems) {
        Object dateObj =
            ReflectionUtil.call(
                crsSpecimenTransformer,
                "date",
                new Class[] {Element.class, String.class},
                elem, "abcd:IdentificationDate");
        offsetDateTime = (OffsetDateTime) dateObj;
        if (offsetDateTime != null) {
          break outerloop;
        }
      }
    }

    String expectedOffsetTime = "1980-11-22T00:00Z";
    assertNotNull("01", offsetDateTime);
    assertEquals( "02",expectedOffsetTime, offsetDateTime.toString());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#hasStatusDeleted()}.
   *
   * @throws Exception
   *
   *         Test method to verify the hasStatusDeleted method returns an expected boolean object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testHasStatusDeleted() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean hasStatus = false;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);

      Object returned =
          CommonReflectionUtil.callMethod(null, null, crsSpecimenTransformer, "hasStatusDeleted");
      hasStatus = (boolean) returned;
    }
    assertFalse(hasStatus);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#hasTestGenus(List elements).
   *
   * @throws Exception
   *
   *         Test method to verify the hasTestGenus method returns an expected boolean object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testHasTestGenus_01() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean hasTestGenus = false;
    List<Element> elementList = new ArrayList<>();
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      Element elemet = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(elemet, "ncrsDetermination");
      assertNotNull(elems);
      elementList.addAll(elems);
      assertNotNull(elementList);

      Object returned = CommonReflectionUtil.callMethod(
          elementList,
          List.class,
          crsSpecimenTransformer,
          "hasTestGenus");
      hasTestGenus = (boolean) returned;
    }

    assertTrue("01", hasTestGenus);
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#hasTestGenus(List elements).
   *
   * @throws Exception
   *
   *         Test method to verify the hasTestGenus method returns an expected boolean object
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testHasTestGenus_02() throws Exception {

    System.setProperty("nl.naturalis.nba.etl.testGenera", "test");
    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    boolean hasTestGenus = true;
    List<Element> elementList = new ArrayList<>();
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      Element elemet = extracted.getRecord();
      List<Element> elems = DOMUtil.getDescendants(elemet, "ncrsDetermination");
      assertNotNull(elems);
      elementList.addAll(elems);
      assertNotNull(elementList);

      Object returned = CommonReflectionUtil.callMethod(
          elementList,
          List.class,
          crsSpecimenTransformer,
          "hasTestGenus");
      hasTestGenus = (boolean) returned;
    }

    assertFalse("01", hasTestGenus);
  }

  /**
   * Test method to verify if the val method returns the expected
   * String value based on the value of a tag
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#val(Element element, String s)}.
   *
   * @throws Exception e
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testVal() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    String val = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);
    for (XMLRecordInfo extracted : extractor) {

      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);
      Element oaiDcElem = getDescendant(extracted.getRecord(), "oai_dc:dc");
      assertNotNull(oaiDcElem);

      List<Element> ncrsdatagroup = getDescendants(oaiDcElem, "ncrsdatagroup");
      assertNotNull(ncrsdatagroup);

      Element sexElement = ncrsdatagroup.get(0);

      Object obj = ReflectionUtil.call(
              crsSpecimenTransformer,
              "val",
              new Class[] {Element.class, String.class},
              sexElement, "abcd:Sex");
      val = (String) obj;
    }

    assertNotNull("01", val);
    assertEquals( "02", "Male", val);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getAreaClass(Element element)}.
   * to verify whether it returns the expected {@link AreaClass} object
   *
   * @throws Exception  may be thrown by CommonReflectionUtil
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetAreaClass() throws Exception{

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    List<String> areaClasses = new ArrayList<>();
    int failedAreas = 0;
    for (XMLRecordInfo extracted : extractor) {

      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "objectID", "RMNH.MAM.TT.5");
      CommonReflectionUtil.setField(
          AbstractTransformer.class, crsSpecimenTransformer, "input", extracted);

      Element element = extracted.getRecord();
      List<Element> elements = DOMUtil.getDescendants(element, "ncrsNamedAreas");
      assertNotNull(elements);

      for (Element elem : elements) {
          Object returned = CommonReflectionUtil.callMethod(
              elem,
              Element.class,
              crsSpecimenTransformer,
              "getAreaClass");
          if (returned == null) {
            failedAreas++;
          } else{
            areaClasses.add(returned.toString());
          }
      }
    }


    int expectedFailedAreas = 1;
    String[] expectedAreaClasses = {"county", "continent"};
    int n = 0;
    assertEquals(  "0" + n++, 2, areaClasses.size());
    assertEquals(  "0" + n++, expectedFailedAreas, failedAreas);
    for (String areaClass : areaClasses) {
      assertEquals("0" + n++, areaClass, AreaClass.parse(areaClass).toString());
      assertTrue(  "0" + n++, Arrays.asList(expectedAreaClasses).contains(areaClass));
    }
  }

  /**
   * Test method for getNamedAreas(@link Element gatheringSite)
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getNamedAreas(Element gatheringSite)}.
   *
   * @throws Exception  may be thrown by CommonReflectionUtil
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetNamedAreas() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    List<NamedArea> namedAreas = new ArrayList<>();

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.MAM.TT.5");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class,
          crsSpecimenTransformer,
          "record",
          extracted.getRecord());

      List<Element> elems = DOMUtil.getDescendants(extracted.getRecord(), "ncrsGatheringSites");
      assertNotNull(elems); // Make sure that the test record contains at least one gatheringSite!
      Element gatheringSite = elems.get(0);

      Object returned = CommonReflectionUtil.callMethod(
              gatheringSite, Element.class,
              crsSpecimenTransformer,
              "getNamedAreas"
              );

      namedAreas = (List<NamedArea>) returned;
    }

    NamedArea expectedNamedArea_01 = new NamedArea(AreaClass.parse("CONTINENT"), "Africa");
    NamedArea expectedNamedArea_02 = new NamedArea(AreaClass.parse("county"), "Atewa Range FR");

    assertEquals("01", 2, namedAreas.size());
    assertTrue(  "02", namedAreas.contains(expectedNamedArea_01));
    assertTrue(  "03", namedAreas.contains(expectedNamedArea_02));
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getTaxonRelationType(Element element)}.
   * to verify whether it returns the expected {@link TaxonRelationType} object
   *
   * @throws Exception  may be thrown by CommonReflectionUtil
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetTaxonRelationType() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);
    List<String> taxonRelationTypes = new ArrayList<>();
    int failedTaxonRelationTypes = 0;

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.MAM.TT.5");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      Element element = extracted.getRecord();
      List<Element> elements = DOMUtil.getDescendants(element, "ncrsSynecology");
      assertNotNull("01", elements);

      for (Element elem : elements) {
        Object returned = CommonReflectionUtil.callMethod(
            elem,
            Element.class,
            crsSpecimenTransformer,
            "getTaxonRelationType");
        if (returned == null) {
          failedTaxonRelationTypes++;
        } else {
          taxonRelationTypes.add(returned.toString());
        }
      }
    }

    assertEquals("02", 3, taxonRelationTypes.size());

    String[] expectedRelationTypes = {"in relation with", "has host", "has parasite"};
    int expectedFailedRelationTypes = 1;
    int n = 1;
    assertEquals("0" + n++, expectedFailedRelationTypes, failedTaxonRelationTypes);
    assertEquals("0" + n++, expectedRelationTypes.length, taxonRelationTypes.size());
    for (String relationType : taxonRelationTypes) {
      assertTrue("0" + n++, Arrays.asList(expectedRelationTypes).contains(relationType));
    }
  }

  /**
   * Test method for
   * {@link nl.naturalis.nba.etl.crs.CrsSpecimenTransformer#getAssociatedTaxa()}.
   * to verify whether it returns the expected result
   *
   * @throws Exception  may be thrown by CommonReflectionUtil
   *
   */
  @SuppressWarnings("JavadocReference")
  @Test
  public void testGetAssociatedTaxa() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<AssociatedTaxon> associatedTaxa = null;
    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.MAM.TT.5");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class,
          crsSpecimenTransformer,
          "record",
          extracted.getRecord());

      Object returned = CommonReflectionUtil.callMethod(
          null,
          null,
          crsSpecimenTransformer,
          "getAssociatedTaxa");

      associatedTaxa = (List<AssociatedTaxon>) returned;
    }

    AssociatedTaxon expectedAssociatedTaxon =
        new AssociatedTaxon("Achillea millefolium", TaxonRelationType.parse("in relation with"));
    assertNotNull("01", associatedTaxa);
    assertEquals( "02", 2, associatedTaxa.size());
    assertTrue(   "03", associatedTaxa.contains(expectedAssociatedTaxon));
  }

  @Test
  public void testGetAssociatedMultiMediaUris() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.CRS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();
    List<ServiceAccessPoint> serviceAccessPoints = null;

    CrsSpecimenTransformer crsSpecimenTransformer = new CrsSpecimenTransformer(report, etlStatistics);
    CrsExtractor extractor = new CrsExtractor(specimenFile, etlStatistics);

    for (XMLRecordInfo extracted : extractor) {
      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "objectID",
          "RMNH.ART.354");

      CommonReflectionUtil.setField(
          AbstractTransformer.class,
          crsSpecimenTransformer,
          "input",
          extracted);

      CommonReflectionUtil.setField(
          CrsSpecimenTransformer.class,
          crsSpecimenTransformer,
          "record",
          extracted.getRecord());

      Object returned = CommonReflectionUtil.callMethod(
          null,
          null,
          crsSpecimenTransformer,
          "getAssociatedMultiMediaUris");

      serviceAccessPoints = (List<ServiceAccessPoint>) returned;
    }

    URI expectedUri_01 = URI.create("https://medialib.naturalis.nl/file/id/RMNH.ART.354_1/format/large");
    URI expectedUri_02 = URI.create("https://medialib.naturalis.nl/file/id/RMNH.ART.354%20_1/format/large");
    assertNotNull("01", serviceAccessPoints);
    assertEquals( "02", 2, serviceAccessPoints.size());
    assertEquals( "03", serviceAccessPoints.get(0).getAccessUri(), expectedUri_01);
    assertEquals( "04", serviceAccessPoints.get(1).getAccessUri(), expectedUri_02);
  }

  @Test
  public void testSetDefaultKingdom() {
    SpecimenIdentification si = new SpecimenIdentification();

    String collectionType = "Paleobotany";
    DefaultClassification classification = new DefaultClassification();
    classification.setGenus("aaa");
    si.setDefaultClassification(classification);
    CrsSpecimenTransformer.setDefaultKingdom(si, collectionType);
    assertEquals("01", "Plantae", si.getDefaultClassification().getKingdom());

    collectionType = "Mineralogy";
    classification.setKingdom(null);
    si.setDefaultClassification(classification);
    CrsSpecimenTransformer.setDefaultKingdom(si, collectionType);
    assertNull("02", si.getDefaultClassification().getKingdom());

    collectionType = "Something different";
    classification.setKingdom(null);
    si.setDefaultClassification(classification);
    CrsSpecimenTransformer.setDefaultKingdom(si, collectionType);
    assertEquals("03", "Animalia", si.getDefaultClassification().getKingdom());


  }
}
