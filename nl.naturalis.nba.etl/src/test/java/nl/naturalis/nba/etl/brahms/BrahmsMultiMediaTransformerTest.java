package nl.naturalis.nba.etl.brahms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaContentIdentification;
import nl.naturalis.nba.api.model.MultiMediaGatheringEvent;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.Person;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AbstractTransformer;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.utils.CommonReflectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for BrahmsMultiMediaTransformer.java
 */
@SuppressWarnings({"unchecked"})
public class BrahmsMultiMediaTransformerTest {

  private static final String jobId = UUID.randomUUID().toString();

  @Before
  public void setUp() {}

  @After
  public void tearDown() {}

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsMultiMediaTransformer#doTransform()}.
   *
   * @throws Exception Test to verify the do Transform object returns the correct
   *     List<MultiMediaObject> object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testDoTransform() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    report.initialiseUrls();
    ETLStatistics etlStatistics = new ETLStatistics();

    // Set all csv fields:
    when(record.get(BrahmsCsvField.imagelist)).thenReturn("https://medialib.naturalis.nl/file/id/L.3355550/format/large");
    when(record.get(BrahmsCsvField.specimenbarcode)).thenReturn("L.3355550");
    when(record.get(BrahmsCsvField.specimencategory)).thenReturn("Herbarium sheet");
    when(record.get(BrahmsCsvField.typecategory)).thenReturn("holotype");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("020491");
    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("AMD0088222");
    when(record.get(BrahmsCsvField.collectioneventid)).thenReturn("4326b2ce-9b99-4691-bccc-cda09841877d");
    when(record.get(BrahmsCsvField.addedby)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.addedon)).thenReturn("2015-09-04 00:00:00");
    when(record.get(BrahmsCsvField.clusterid)).thenReturn("5078424");
    when(record.get(BrahmsCsvField.determinedby)).thenReturn("Huis, E. van");
    when(record.get(BrahmsCsvField.determinationday)).thenReturn("11");
    when(record.get(BrahmsCsvField.determinationmonth)).thenReturn("3");
    when(record.get(BrahmsCsvField.determinationyear)).thenReturn("2015");
    when(record.get(BrahmsCsvField.determinationfamilyname)).thenReturn("Rhabdonemataceae");
    when(record.get(BrahmsCsvField.determinationcalcfullname)).thenReturn("Acmena acuminatissima (Blume) Merr. & L.M.Perry");
    when(record.get(BrahmsCsvField.prefix)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.fieldnumber)).thenReturn("12918");
    when(record.get(BrahmsCsvField.suffix)).thenReturn("bis A!");
    when(record.get(BrahmsCsvField.collectionyear)).thenReturn("2014");
    when(record.get(BrahmsCsvField.collectionmonth)).thenReturn("4");
    when(record.get(BrahmsCsvField.collectionday)).thenReturn("15");
    when(record.get(BrahmsCsvField.collectors)).thenReturn("Nordenskjöld, O.");
    when(record.get(BrahmsCsvField.additionalcollectors)).thenReturn("Popescu, G.; Cirtu, D.; Zaharia, I.");
    when(record.get(BrahmsCsvField.lldatum)).thenReturn("WGS 84");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("52.165538");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("4.474924");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("12");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.localitynotes)).thenReturn("Tuin Naturalis");
    when(record.get(BrahmsCsvField.habitattext)).thenReturn("Aangelegde tuin");
    when(record.get(BrahmsCsvField.descriptiontext)).thenReturn("Vak van 1 x 1 meter.");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("Zuid-Holland");
    when(record.get(BrahmsCsvField.minoradminname)).thenReturn("Gemeente Leiden");
    when(record.get(BrahmsCsvField.localityname)).thenReturn("Leiden");
    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("The Netherlands");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("Scrophularia");
    when(record.get(BrahmsCsvField.calcfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.calcacceptedname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.synofname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("heterophylla");
    when(record.get(BrahmsCsvField.subspecies)).thenReturn("laciniata");
    when(record.get(BrahmsCsvField.variety)).thenReturn("");
    when(record.get(BrahmsCsvField.forma)).thenReturn("");
    when(record.get(BrahmsCsvField.speciesauthorname)).thenReturn("L.");
    when(record.get(BrahmsCsvField.subspeciesauthorname)).thenReturn("(Koch) P.Fourn.");
    when(record.get(BrahmsCsvField.varietyauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.formaauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.taxstatus)).thenReturn("acc");
    when(record.get(BrahmsCsvField.genusid)).thenReturn("1db69ef8-25f2-4985-9f46-b26ead192ca4");
    when(record.get(BrahmsCsvField.kingdom)).thenReturn("Plantae");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.classname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("Lamiales");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("Scrophulariaceae");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.curationspeciesfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("AMD");

    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer =
            new BrahmsMultiMediaTransformer(report, etlStatistics);

    CommonReflectionUtil.setField(
            AbstractTransformer.class,
            brahmsMultiMediaTransformer,
            "objectID", "L.3355550");

    CommonReflectionUtil.setField(
            AbstractTransformer.class,
            brahmsMultiMediaTransformer,
            "input", record);

    Object returned = CommonReflectionUtil.callMethod(
                    null,
            CSVRecordInfo.class,
            brahmsMultiMediaTransformer,
            "doTransform");

    List<MultiMediaObject> list = (List<MultiMediaObject>) returned;

    String expectedAssociatedSpecimenRef = "L.3355550@BRAHMS";
    String expectedSourceID = "Brahms";

    assertNotNull("01", list);
    assertEquals("02", 1, list.size());
    assertEquals("03",
            expectedAssociatedSpecimenRef,
            list.stream().map(MultiMediaObject::getAssociatedSpecimenReference).findFirst().get());
    assertEquals("04",
            expectedSourceID,
            list.stream().map(MultiMediaObject::getSourceID).findFirst().get());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsMultiMediaTransformer
   * #transformOne()}.
   *
   * @throws Exception Test to verify the transform One object returns the correct MultiMediaObject
   *                   object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testTransformOne() throws Exception {

    ETLStatistics etlStatistics = new ETLStatistics();
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    // Set all csv fields:
    when(record.get(BrahmsCsvField.imagelist)).thenReturn("https://medialib.naturalis.nl/file/id/L.3355550/format/large");
    when(record.get(BrahmsCsvField.specimenbarcode)).thenReturn("L.3355550");
    when(record.get(BrahmsCsvField.specimencategory)).thenReturn("Herbarium sheet");
    when(record.get(BrahmsCsvField.typecategory)).thenReturn("holotype");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("020491");
    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("AMD0088222");
    when(record.get(BrahmsCsvField.collectioneventid)).thenReturn("4326b2ce-9b99-4691-bccc-cda09841877d");
    when(record.get(BrahmsCsvField.addedby)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.addedon)).thenReturn("2015-09-04 00:00:00");
    when(record.get(BrahmsCsvField.clusterid)).thenReturn("5078424");
    when(record.get(BrahmsCsvField.determinedby)).thenReturn("Huis, E. van");
    when(record.get(BrahmsCsvField.determinationday)).thenReturn("11");
    when(record.get(BrahmsCsvField.determinationmonth)).thenReturn("3");
    when(record.get(BrahmsCsvField.determinationyear)).thenReturn("2015");
    when(record.get(BrahmsCsvField.determinationfamilyname)).thenReturn("Rhabdonemataceae");
    when(record.get(BrahmsCsvField.determinationcalcfullname)).thenReturn("Acmena acuminatissima (Blume) Merr. & L.M.Perry");
    when(record.get(BrahmsCsvField.prefix)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.fieldnumber)).thenReturn("12918");
    when(record.get(BrahmsCsvField.suffix)).thenReturn("bis A!");
    when(record.get(BrahmsCsvField.collectionyear)).thenReturn("2014");
    when(record.get(BrahmsCsvField.collectionmonth)).thenReturn("4");
    when(record.get(BrahmsCsvField.collectionday)).thenReturn("15");
    when(record.get(BrahmsCsvField.collectors)).thenReturn("Nordenskjöld, O.");
    when(record.get(BrahmsCsvField.additionalcollectors)).thenReturn("Popescu, G.; Cirtu, D.; Zaharia, I.");
    when(record.get(BrahmsCsvField.lldatum)).thenReturn("WGS 84");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("52.165538");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("4.474924");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("12");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.localitynotes)).thenReturn("Tuin Naturalis");
    when(record.get(BrahmsCsvField.habitattext)).thenReturn("Aangelegde tuin");
    when(record.get(BrahmsCsvField.descriptiontext)).thenReturn("Vak van 1 x 1 meter.");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("Zuid-Holland");
    when(record.get(BrahmsCsvField.minoradminname)).thenReturn("Gemeente Leiden");
    when(record.get(BrahmsCsvField.localityname)).thenReturn("Leiden");
    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("The Netherlands");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("Scrophularia");
    when(record.get(BrahmsCsvField.calcfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.calcacceptedname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.synofname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("heterophylla");
    when(record.get(BrahmsCsvField.subspecies)).thenReturn("laciniata");
    when(record.get(BrahmsCsvField.variety)).thenReturn("");
    when(record.get(BrahmsCsvField.forma)).thenReturn("");
    when(record.get(BrahmsCsvField.speciesauthorname)).thenReturn("L.");
    when(record.get(BrahmsCsvField.subspeciesauthorname)).thenReturn("(Koch) P.Fourn.");
    when(record.get(BrahmsCsvField.varietyauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.formaauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.taxstatus)).thenReturn("acc");
    when(record.get(BrahmsCsvField.genusid)).thenReturn("1db69ef8-25f2-4985-9f46-b26ead192ca4");
    when(record.get(BrahmsCsvField.kingdom)).thenReturn("Plantae");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.classname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("Lamiales");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("Scrophulariaceae");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.curationspeciesfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("AMD");

    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer =
        new BrahmsMultiMediaTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "input", record);

    String url = "https://medialib.naturalis.nl/file/id/L.3355550/format/large";

    String expectedAssociatedSpecimenRef = "L.3355550@BRAHMS";

    Object returned =
        CommonReflectionUtil.callMethod(
            url, String.class, brahmsMultiMediaTransformer, "transformOne");

    MultiMediaObject mediaObject = (MultiMediaObject) returned;
    assertNotNull("01", mediaObject);
    assertEquals("02", expectedAssociatedSpecimenRef, mediaObject.getAssociatedSpecimenReference());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsMultiMediaTransformer
   * #getIdentification()}.
   *
   * @throws Exception Test to verify the getIdentification() method returns the correct
   *                   {@link MultiMediaContentIdentification} object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGetIdentification() throws Exception {

    ETLStatistics etlStatistics = new ETLStatistics();
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    // Set all csv fields:
    when(record.get(BrahmsCsvField.imagelist)).thenReturn("https://medialib.naturalis.nl/file/id/L.3355550/format/large");
    when(record.get(BrahmsCsvField.specimenbarcode)).thenReturn("L.3355550");
    when(record.get(BrahmsCsvField.specimencategory)).thenReturn("Herbarium sheet");
    when(record.get(BrahmsCsvField.typecategory)).thenReturn("holotype");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("020491");
    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("AMD0088222");
    when(record.get(BrahmsCsvField.collectioneventid)).thenReturn("4326b2ce-9b99-4691-bccc-cda09841877d");
    when(record.get(BrahmsCsvField.addedby)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.addedon)).thenReturn("2015-09-04 00:00:00");
    when(record.get(BrahmsCsvField.clusterid)).thenReturn("5078424");
    when(record.get(BrahmsCsvField.determinedby)).thenReturn("Huis, E. van");
    when(record.get(BrahmsCsvField.determinationday)).thenReturn("11");
    when(record.get(BrahmsCsvField.determinationmonth)).thenReturn("3");
    when(record.get(BrahmsCsvField.determinationyear)).thenReturn("2015");
    when(record.get(BrahmsCsvField.determinationfamilyname)).thenReturn("Rhabdonemataceae");
    when(record.get(BrahmsCsvField.determinationcalcfullname)).thenReturn("Acmena acuminatissima (Blume) Merr. & L.M.Perry");
    when(record.get(BrahmsCsvField.prefix)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.fieldnumber)).thenReturn("12918");
    when(record.get(BrahmsCsvField.suffix)).thenReturn("bis A!");
    when(record.get(BrahmsCsvField.collectionyear)).thenReturn("2014");
    when(record.get(BrahmsCsvField.collectionmonth)).thenReturn("4");
    when(record.get(BrahmsCsvField.collectionday)).thenReturn("15");
    when(record.get(BrahmsCsvField.collectors)).thenReturn("Nordenskjöld, O.");
    when(record.get(BrahmsCsvField.additionalcollectors)).thenReturn("Popescu, G.; Cirtu, D.; Zaharia, I.");
    when(record.get(BrahmsCsvField.lldatum)).thenReturn("WGS 84");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("52.165538");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("4.474924");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("12");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.localitynotes)).thenReturn("Tuin Naturalis");
    when(record.get(BrahmsCsvField.habitattext)).thenReturn("Aangelegde tuin");
    when(record.get(BrahmsCsvField.descriptiontext)).thenReturn("Vak van 1 x 1 meter.");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("Zuid-Holland");
    when(record.get(BrahmsCsvField.minoradminname)).thenReturn("Gemeente Leiden");
    when(record.get(BrahmsCsvField.localityname)).thenReturn("Leiden");
    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("The Netherlands");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("Scrophularia");
    when(record.get(BrahmsCsvField.calcfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.calcacceptedname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.synofname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("heterophylla");
    when(record.get(BrahmsCsvField.subspecies)).thenReturn("laciniata");
    when(record.get(BrahmsCsvField.variety)).thenReturn("");
    when(record.get(BrahmsCsvField.forma)).thenReturn("");
    when(record.get(BrahmsCsvField.speciesauthorname)).thenReturn("L.");
    when(record.get(BrahmsCsvField.subspeciesauthorname)).thenReturn("(Koch) P.Fourn.");
    when(record.get(BrahmsCsvField.varietyauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.formaauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.taxstatus)).thenReturn("acc");
    when(record.get(BrahmsCsvField.genusid)).thenReturn("1db69ef8-25f2-4985-9f46-b26ead192ca4");
    when(record.get(BrahmsCsvField.kingdom)).thenReturn("Plantae");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.classname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("Lamiales");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("Scrophulariaceae");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.curationspeciesfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("AMD");


    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer =
        new BrahmsMultiMediaTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "input", record);

    String expectedScientificNameGroup = "scrophularia heterophylla laciniata";
    String expectedTaxonGroup = "(Koch) P.Fourn.";

    Object returned = CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsMultiMediaTransformer, "getIdentification");

    MultiMediaContentIdentification contentIdentification = (MultiMediaContentIdentification) returned;

    assertNotNull("01", contentIdentification);
    assertEquals("02",
        expectedScientificNameGroup,
        contentIdentification.getScientificName().getScientificNameGroup());
    assertEquals("03",
        expectedTaxonGroup,
        contentIdentification.getScientificName().getAuthorshipVerbatim());
  }

  /**
   * Test method for {@link BrahmsMultiMediaTransformer
   * #getMultiMediaGatheringEvent()}.
   *
   * @throws Exception Test to verify the getMultiMediaGatheringEvent One object returns the correct
   *                   {@link MultiMediaGatheringEvent} object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGetMultiMediaGatheringEvent() throws Exception {

    ETLStatistics etlStatistics = new ETLStatistics();
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    // Set relevant csv fields:
    when(record.get(BrahmsCsvField.collectors, false)).thenReturn("Plabon Kakoti");
    when(record.get(BrahmsCsvField.additionalcollectors, false)).thenReturn("Ayco Holleman");
    when(record.get(BrahmsCsvField.lldatum)).thenReturn("WGS 84");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("52.165538");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("4.474924");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("1");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("2");
    when(record.get(BrahmsCsvField.localitynotes)).thenReturn("Tuin Naturalis");
    when(record.get(BrahmsCsvField.habitattext)).thenReturn("Aangelegde tuin");
    when(record.get(BrahmsCsvField.descriptiontext)).thenReturn("Vak van 1 x 1 meter.");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("Zuid-Holland");
    when(record.get(BrahmsCsvField.minoradminname)).thenReturn("Gemeente Leiden");
    when(record.get(BrahmsCsvField.localityname)).thenReturn("Leiden");
    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("The Netherlands");

    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer = new BrahmsMultiMediaTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(AbstractTransformer.class, brahmsMultiMediaTransformer,
            "objectID",
            "L.3355550");
    CommonReflectionUtil.setField(AbstractTransformer.class, brahmsMultiMediaTransformer,
            "input",
            record);

    Object returned = CommonReflectionUtil.callMethod(
            record,
            CSVRecordInfo.class,
            brahmsMultiMediaTransformer,
            "getMultiMediaGatheringEvent");

    MultiMediaGatheringEvent multiMediaGatheringEvent = (MultiMediaGatheringEvent) returned;

    assertNotNull("01", multiMediaGatheringEvent);
    assertEquals("02", "Europe", multiMediaGatheringEvent.getContinent());
    assertEquals("03", "The Netherlands", multiMediaGatheringEvent.getCountry());
    assertEquals("04", "1 - 2", multiMediaGatheringEvent.getAltitude());
    assertEquals("05", "m", multiMediaGatheringEvent.getAltitudeUnifOfMeasurement());
    assertEquals("06", new Person("Plabon Kakoti; Ayco Holleman"), multiMediaGatheringEvent.getGatheringPersons().get(0));
  }


  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsMultiMediaTransformer #getUri()}.
   *
   * @throws Exception Test to verify the getUri method returns the correct {@link URI} object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test(expected = URISyntaxException.class)
  public void testGetUri() throws Exception {

    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer =
        new BrahmsMultiMediaTransformer(report, etlStatistics);

    String urlStr = "https://medialib.naturalis.nl/file/id/Test URL/L. 3355 550/format/large";
    String urlStrEncoded =
        "https://medialib.naturalis.nl/file/id/Test%20URL/L.%203355%20550/format/large";
    URI uriExpected = new URI(urlStrEncoded);
    Object obj =
        CommonReflectionUtil.callMethod(
            urlStr, String.class, brahmsMultiMediaTransformer, "getUri");
    URI uriActual = (URI) obj;
    assertEquals("01", uriActual, uriExpected);

    urlStr = "https://medialib.naturalis.nl/file/id/Test_URL/L.3355550/format/large";
    uriExpected = new URI(urlStr);
    obj =
        CommonReflectionUtil.callMethod(
            urlStr, String.class, brahmsMultiMediaTransformer, "getUri");
    uriActual = (URI) obj;
    assertEquals("02", uriActual, uriExpected);

    urlStr = "https://medialib.naturalis.nl/file/id/L.3355550/format/small";
    uriExpected = new URI(urlStr);
    obj =
        CommonReflectionUtil.callMethod(
            urlStr, String.class, brahmsMultiMediaTransformer, "getUri");
    uriActual = (URI) obj;
    assertEquals("03", uriActual, uriExpected);

    String illegalUriStr = "&#12288;quatsch&#12288;";
    try {
      obj =
          CommonReflectionUtil.callMethod(
              illegalUriStr, String.class, brahmsMultiMediaTransformer, "getUri");
      uriActual = (URI) obj;
      System.out.println(uriActual.toString());
    } catch (InvocationTargetException e) {
      if (e.getCause() instanceof URISyntaxException) {
        throw new URISyntaxException(illegalUriStr, e.getCause().getMessage());
      }
    }
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsMultiMediaTransformer
   * #newServiceAccessPoint()}.
   *
   * @throws Exception Test to verify the newServiceAccessPoint One object returns the correct
   *                   {@link ServiceAccessPoint} object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testNewServiceAccessPoint() throws Exception {

    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.MULTI_MEDIA_OBJECT);
    ETLStatistics etlStatistics = new ETLStatistics();
    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    when(record.get(BrahmsCsvField.imagelist))
        .thenReturn("https://medialib.naturalis.nl/file/id/L.3355550/format/large/");

    BrahmsMultiMediaTransformer brahmsMultiMediaTransformer =
        new BrahmsMultiMediaTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsMultiMediaTransformer, "input", record);

    URI uri = new URI("https://medialib.naturalis.nl/file/id/L.3355550/format/large");
    Object obj =
        CommonReflectionUtil.callMethod(
            uri, URI.class, brahmsMultiMediaTransformer, "newServiceAccessPoint");
    ServiceAccessPoint serviceAccessPoint = (ServiceAccessPoint) obj;

    assertNotNull("01", obj);
    assertEquals("02", ServiceAccessPoint.class, serviceAccessPoint.getClass());
    assertEquals("03", uri, serviceAccessPoint.getAccessUri());
    assertEquals("04", "image/jpeg", serviceAccessPoint.getFormat());
    assertEquals("05", "ac:GoodQuality", serviceAccessPoint.getVariant());

    uri = new URI("https://medialib.naturalis.nl/file/id/L.3355550/format/small");
    obj =
        CommonReflectionUtil.callMethod(
            uri, URI.class, brahmsMultiMediaTransformer, "newServiceAccessPoint");
    serviceAccessPoint = (ServiceAccessPoint) obj;
    assertEquals("06", uri, serviceAccessPoint.getAccessUri());

    uri = new URI("https://medialib.naturalis.nl/file/id/L.3355550/fromat/large");
    obj =
        CommonReflectionUtil.callMethod(
            uri, URI.class, brahmsMultiMediaTransformer, "newServiceAccessPoint");
    assertNull("07", obj);
  }
}
