package nl.naturalis.nba.etl.dcsr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.api.model.TaxonomicStatus;
import nl.naturalis.nba.api.model.VernacularName;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AllTests;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.dcsr.model.DcsrTaxon;
import nl.naturalis.nba.etl.dcsr.model.Name;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.utils.reflect.ReflectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Test Class for {@link DcsrTaxonTransformer}
 */
public class DcsrTaxonTransformerTest {

  URL dcsrFileURL;
  File dcsrFile;
  ArrayList<DcsrTaxon> dcsrTaxa;

  /**
   * @throws Exception exception
   */
  @Before
  public void setUp() throws Exception {
    System.setProperty(
        "nl.naturalis.nba.etl.testGenera",
        "malus,parus,larus,manduca,bombus,rhododendron,felix,tulipa,rosa,canis,passer,trientalis");
    dcsrFileURL = AllTests.class.getResource("dcsr-export--2021-02-05.jsonl");
    dcsrFile = new File(dcsrFileURL.getFile());
    dcsrTaxa = getDcsrTaxa(dcsrFile);
  }

  // TODO: create test file for this test!!!


  @After
  public void tearDown() {}

  /**
   * Test method for {@link DcsrTaxonTransformer#doTransform()}.
   * <p>
   * Test to verify if the doTransform method returns an expected {List<@Taxon>}
   *
   * @throws Exception exception
   */
  @Test
  public void testDoTransform() throws Exception {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer taxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);
    List<Taxon> transformed = null;

    LineNumberReader lnr;
    FileReader fr = new FileReader(dcsrFile);
    lnr = new LineNumberReader(fr, 4096);
    String json;
    while ((json = lnr.readLine()) != null) {
      transformed = taxonTransformer.transform(json);
    }

    assertNotNull("01", transformed);
    Taxon actual = transformed.get(0);

    String expectedId = "43J2SPLJRPL@DCSR";
    String expectedSourceSystemId = "43J2SPLJRPL";
    String expectedAuthorName = "Rotschild";
    String expectedFullScientificName = "Manduca rustica harteri Rotschild, 1894";
    String expectedGenusOrMonomial = "Manduca";
    String expectedScientificNameGroup = "manduca rustica harteri";
    String recordUri = "https://www.dutchcaribbeanspecies.org/nsr/concept/043J2SPLJRPL";
    String sourceSystemName = SourceSystem.DCSR.getName();

    assertEquals("02", expectedId, actual.getId());
    assertEquals("03", expectedSourceSystemId, actual.getSourceSystemId());
    assertEquals("04", expectedAuthorName, actual.getAcceptedName().getAuthor());
    assertEquals("05", recordUri, actual.getRecordURI().toString());
    assertEquals("06", expectedFullScientificName, actual.getAcceptedName().getFullScientificName());
    assertEquals("07", expectedGenusOrMonomial, actual.getAcceptedName().getGenusOrMonomial());
    assertEquals("08", expectedScientificNameGroup, actual.getAcceptedName().getScientificNameGroup());
    assertEquals("09", sourceSystemName, actual.getSourceSystem().getName());
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#invalidRank(String rank)
   *
   * Test to verify if the invalidRank method returns an expected boolean value
   */
  @Test
  public void testInvalidRank() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    Object obj = ReflectionUtil.call(
            dcsrTaxonTransformer,"invalidRank", new Class[] {String.class}, "superfamilia");
    boolean rank = (boolean) obj;
    assertTrue("01", rank);

    obj = ReflectionUtil.call(
            dcsrTaxonTransformer, "invalidRank", new Class[] {String.class}, new Object[] {null});
    rank = (boolean) obj;
    assertTrue("02", rank);

    obj = ReflectionUtil.call(
        dcsrTaxonTransformer, "invalidRank", new Class[] {String.class}, "species");
    rank = (boolean) obj;
    assertFalse("03", rank);
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#isVernacularName(String nameType)
   *
   * Test to verify if the isVernacularName method returns an expected boolean value
   */
  @Test
  public void testIsVernacularName() {

    boolean isVernacularName;
    Object obj = ReflectionUtil.callStatic(
        DcsrTaxonTransformer.class, "isVernacularName", new Class[] {String.class}, "isPreferredNameOf");
    isVernacularName = (boolean) obj;
    assertTrue("01", isVernacularName);

    obj = ReflectionUtil.callStatic(
        DcsrTaxonTransformer.class, "isVernacularName", new Class[] {String.class}, "isPreferredNameOf");
    isVernacularName = (boolean) obj;
    assertTrue("02", isVernacularName);

    obj = ReflectionUtil.callStatic(
        DcsrTaxonTransformer.class, "isVernacularName", new Class[] {String.class}, "slide");
    isVernacularName = (boolean) obj;
    assertFalse("03", isVernacularName);

    obj = ReflectionUtil.callStatic(
        DcsrTaxonTransformer.class, "isVernacularName", new Class[] {String.class}, new Object[] {null});
    isVernacularName = (boolean) obj;
    assertFalse("04", isVernacularName);
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#addScientificNames(Taxon taxon).
   * <p>
   * Test to verify if the addScientificNames method returns an expected boolean value
   */
  @Ignore
  @Test
  public void testAddScientificNames() {

    // Todo: add test method

    // String jobId = UUID.randomUUID().toString();
    // Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    // ETLStatistics etlStatistics = new ETLStatistics();
    // Object returned = null;

    // Taxon taxon = new Taxon();
    // DcsrTaxon dcsrTaxon = dcsrTaxa.get(0);

    // DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(etlStatistics);
    // DcsrTaxonTransformer dcsrTaxonTransformer = mock(DcsrTaxonTransformer.class);
    // returned = ReflectionUtil.call(
    //           dcsrTaxonTransformer,
    //           "addScientificNames",
    //           new Class[] {Taxon.class},
    //           new Object[] {taxon});
  }


  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#add(Taxon taxon, ScientificName scientificName)
   *
   * Test to verify if the add method returns an expected boolean value
   */
  @Test
  public void testAdd_01() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);
    ScientificName name = new ScientificName();
    name.setFullScientificName("Manduca rustica harteri Rotschild, 1894");
    name.setScientificNameGroup("manduca rustica harteri");
    name.setGenusOrMonomial("Manduca");
    name.setAuthor("Rotschild");
    name.setSpecificEpithet("rustica");
    name.setInfraspecificEpithet("harteri");
    name.setTaxonomicStatus(TaxonomicStatus.ACCEPTED_NAME);

    Taxon taxon = new Taxon();
    taxon.setAcceptedName(name);
    taxon.setSourceSystemId("43J2SPLJRPL");
    taxon.setId("43J2SPLJRPL@DCSR");
    taxon.setValidName(name);

    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "add", new Class[] {Taxon.class, ScientificName.class}, taxon, name);

    boolean addScientificName = (boolean) returned;
    assertFalse(addScientificName);

    // TODO: revisit this test and/or the method it tests. Not sure if it is fully functional
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#add(Taxon taxon, ScientificName scientificName)
   *
   * Test to verify if the add method returns an expected boolean value
   */
  @Test
  public void testAdd_02() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    ScientificName name = new ScientificName();
    name.setFullScientificName("Manduca rustica harteri Rotschild, 1894");
    name.setScientificNameGroup("manduca rustica harteri");
    name.setGenusOrMonomial("Manduca");
    name.setAuthor("Rotschild");
    name.setSpecificEpithet("rustica");
    name.setInfraspecificEpithet("harteri");
    name.setTaxonomicStatus(TaxonomicStatus.ACCEPTED_NAME);

    Taxon taxon = new Taxon();
    taxon.setAcceptedName(null);
    taxon.setSourceSystemId("43J2SPLJRPL");
    taxon.setId("43J2SPLJRPL@DCSR");

    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "add", new Class[] {Taxon.class, ScientificName.class}, taxon, name);

    boolean addScientificName = (boolean) returned;
    assertTrue(addScientificName);
  }

  /**
   * Test method for <a href="nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#hasTestGenus(Taxon taxon)">
   *   nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#hasTestGenus(Taxon taxon)</a>.
   *
   * Test to verify if the hasTestGenus method returns an expected boolean value
   *
   */
  @Test
  public void testHasTestGenus_01() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    ScientificName name = new ScientificName();
    name.setFullScientificName("Manduca rustica harteri Rotschild, 1894");
    name.setScientificNameGroup("manduca rustica harteri");
    name.setGenusOrMonomial("Manduca");
    name.setAuthor("Rotschild");
    name.setSpecificEpithet("rustica");
    name.setInfraspecificEpithet("harteri");

    Taxon taxon = new Taxon();
    taxon.setAcceptedName(name);
    taxon.setSourceSystemId("43J2SPLJRPL");
    taxon.setId("43J2SPLJRPL@DCSR");
    taxon.setValidName(name);

    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "hasTestGenus", new Class[] {Taxon.class}, taxon);

    boolean hastTestGenus = (boolean) returned;
    assertTrue(hastTestGenus);
  }

  /**
   * Test method for <a href="nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#hasTestGenus(Taxon taxon)">
   *   nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#hasTestGenus(Taxon taxon)</a>.
   *
   * Test to verify if the hasTestGenus method returns an expected boolean value
   *
   */
  @Test
  public void testHasTestGenus_02() {

    System.setProperty("nl.naturalis.nba.etl.testGenera", "quatsch");
    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    ScientificName name = new ScientificName();
    name.setFullScientificName("Manduca rustica harteri Rotschild, 1894");
    name.setScientificNameGroup("manduca rustica harteri");
    name.setGenusOrMonomial("Manduca");
    name.setAuthor("Rotschild");
    name.setSpecificEpithet("rustica");
    name.setInfraspecificEpithet("harteri");

    Taxon taxon = new Taxon();
    taxon.setAcceptedName(name);
    taxon.setSourceSystemId("43J2SPLJRPL");
    taxon.setId("43J2SPLJRPL@DCSR");
    taxon.setValidName(name);

    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "hasTestGenus", new Class[] {Taxon.class}, taxon);

    boolean hastTestGenus = (boolean) returned;
    assertFalse(hastTestGenus);
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#getScientificName(Name name).
   *
   * Test to verify if the getScientificName method returns an expected {@link ScientificName} object
   */
  @Test
  public void testGetScientificName() {

    ScientificName actual;
    Object returned;

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    DcsrTaxon dcsrTaxon = dcsrTaxa.get(0);
    Name name = dcsrTaxon.getNames()[0];
    returned = ReflectionUtil.call(dcsrTaxonTransformer, "getScientificName", new Class[] {Name.class}, name);
    actual = (ScientificName) returned;

    String expectedAuthorName = "Rotschild";
    String expectedFullScintificName = "Manduca rustica harteri Rotschild, 1894";
    String expectedGenusOrMonomial = "Manduca";
    String expectedScientificNameGroup = "manduca rustica harteri";
    String expectedSpecificEpithet = "rustica";
    String expectedInfraspecificEpithet = "harteri";
    String expectedAuthorshipVerbatim = "Rotschild, 1894";
    String expectedYear = "1894";

    assertNotNull("01", actual);
    assertEquals("02", expectedAuthorName, actual.getAuthor());
    assertEquals("03", expectedFullScintificName, actual.getFullScientificName());
    assertEquals("04", expectedGenusOrMonomial, actual.getGenusOrMonomial());
    assertEquals("05", expectedScientificNameGroup, actual.getScientificNameGroup());
    assertEquals("06", expectedSpecificEpithet, actual.getSpecificEpithet());
    assertEquals("07", expectedInfraspecificEpithet, actual.getInfraspecificEpithet());
    assertEquals("08", expectedAuthorshipVerbatim, actual.getAuthorshipVerbatim());
    assertEquals("09", expectedYear, actual.getYear());
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#getVernacularName(Element element)
   *
   * Test to verify if the getVernacularName method returns an expected {@link VernacularName} object
   */
  @Test
  public void testGetVernacularName() {

    ETLStatistics etlStatistics = new ETLStatistics();
    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);
    DcsrTaxon dcsrTaxon = dcsrTaxa.get(0);

    Name vernacularName = null;
    for (Name name : dcsrTaxon.getNames()) {
      if (name.getNametype().equals("isPreferredNameOf")) vernacularName = name;
    }

    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "getVernacularName", new Class[] {Name.class}, vernacularName);
    VernacularName actual = (VernacularName) returned;

    String expectedName = "Barbulètè Gabilan Mancha Hel";
    boolean isPreffered = true;
    String expectedLanguage = "Papiamento";

    assertNotNull("01", actual);
    assertEquals("02", expectedName, actual.getName());
    assertEquals("03", isPreffered, actual.getPreferred());
    assertEquals("04", expectedLanguage, actual.getLanguage());
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#getReferenceDate(Name name)
   *
   * Test to verify if the getReferenceDate method returns an expected {@link OffsetDateTime} object
   */
  @Test
  public void testGetReferenceDate() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    DcsrTaxon dcsrTaxon = dcsrTaxa.get(0);
    Name name = dcsrTaxon.getNames()[1];
    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "getReferenceDate", new Class[] {Name.class}, name);
    OffsetDateTime actual = (OffsetDateTime) returned;

    String expectedDateString = "2004-01-01T00:00Z";
    assertNotNull("01", actual);
    assertEquals("02", expectedDateString, actual.toString());
  }

  /**
   * Test method for nl.naturalis.nba.etl.dcsr.DcsrTaxonTransformer#getTaxonomicStatus(Name name).
   * <p>
   * Test to verify if the getTaxonomicStatus method returns an expected {@link TaxonomicStatus} object
   */
  @Test
  public void testGetTaxonomicStatus() {

    String jobId = UUID.randomUUID().toString();
    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
    ETLStatistics etlStatistics = new ETLStatistics();
    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    Name name = dcsrTaxa.get(0).getNames()[0];
    Object returned = ReflectionUtil.call(
        dcsrTaxonTransformer, "getTaxonomicStatus", new Class[] {Name.class}, name);
    TaxonomicStatus actual = (TaxonomicStatus) returned;

    String expectedDateString = "accepted name";
    assertNotNull("01", actual);
    assertEquals("02", expectedDateString, actual.toString());
  }

  private static ArrayList<DcsrTaxon> getDcsrTaxa(File dcsrFile) throws IOException {

    ArrayList<DcsrTaxon> dcsrTaxa = new ArrayList<>();

//    String jobId = UUID.randomUUID().toString();
//    Report report = new Report(jobId, SourceSystem.DCSR, DocumentType.TAXON);
//    ETLStatistics etlStatistics = new ETLStatistics();
//    DcsrTaxonTransformer dcsrTaxonTransformer = new DcsrTaxonTransformer(report, etlStatistics);

    LineNumberReader lnr;
    FileReader fr = new FileReader(dcsrFile);
    lnr = new LineNumberReader(fr, 4096);
    String jsonLine;
    ObjectMapper objectMapper = new ObjectMapper();
    while ((jsonLine = lnr.readLine()) != null) {
      dcsrTaxa.add(objectMapper.readValue(jsonLine, DcsrTaxon.class));
    }
    return dcsrTaxa;
  }
}
