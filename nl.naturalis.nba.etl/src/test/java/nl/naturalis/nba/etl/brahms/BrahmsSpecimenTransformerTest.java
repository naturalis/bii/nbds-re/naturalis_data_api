package nl.naturalis.nba.etl.brahms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import nl.naturalis.nba.api.model.Agent;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.ServiceAccessPoint;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.api.model.SpecimenIdentification;
import nl.naturalis.nba.api.model.SpecimenTypeStatus;
import nl.naturalis.nba.dao.DocumentType;
import nl.naturalis.nba.etl.AbstractTransformer;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.utils.CommonReflectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for BrahmsSpecimenTransformer.java
 */
@SuppressWarnings({"unchecked"})
public class BrahmsSpecimenTransformerTest {

  private static final String jobId = UUID.randomUUID().toString();

  @Before
  public void setUp() {}

  @After
  public void tearDown() {}


  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer#doTransform()}.
   *
   * @throws Exception Test to verify the do Transform object returns the correct
   *                   {@link List<Specimen>} object
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testDoTransform() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    // Set all csv fields:
    when(record.get(BrahmsCsvField.specimenbarcode)).thenReturn("AMD.88222");
    when(record.get(BrahmsCsvField.specimencategory)).thenReturn("Herbarium sheet");
    when(record.get(BrahmsCsvField.typecategory)).thenReturn("holotype");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("020491");
    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("AMD0088222");
    when(record.get(BrahmsCsvField.collectioneventid)).thenReturn("4326b2ce-9b99-4691-bccc-cda09841877d");
    when(record.get(BrahmsCsvField.addedby)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.addedon)).thenReturn("2015-09-04 00:00:00");
    when(record.get(BrahmsCsvField.clusterid)).thenReturn("5078424");
    when(record.get(BrahmsCsvField.determinedby)).thenReturn("Huis, E. van");
    when(record.get(BrahmsCsvField.determinationday)).thenReturn("11");
    when(record.get(BrahmsCsvField.determinationmonth)).thenReturn("3");
    when(record.get(BrahmsCsvField.determinationyear)).thenReturn("2015");
    when(record.get(BrahmsCsvField.imagelist)).thenReturn("https://medialib.naturalis.nl/file/id/AMD.88222/format/large");
    when(record.get(BrahmsCsvField.determinationfamilyname)).thenReturn("Rhabdonemataceae");
    when(record.get(BrahmsCsvField.determinationcalcfullname)).thenReturn("Acmena acuminatissima (Blume) Merr. & L.M.Perry");
    when(record.get(BrahmsCsvField.prefix)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.fieldnumber)).thenReturn("12918");
    when(record.get(BrahmsCsvField.suffix)).thenReturn("bis A!");
    when(record.get(BrahmsCsvField.collectionyear)).thenReturn("2014");
    when(record.get(BrahmsCsvField.collectionmonth)).thenReturn("4");
    when(record.get(BrahmsCsvField.collectionday)).thenReturn("15");
    when(record.get(BrahmsCsvField.collectors)).thenReturn("Nordenskjöld, O.");
    when(record.get(BrahmsCsvField.additionalcollectors)).thenReturn("Popescu, G.; Cirtu, D.; Zaharia, I.");
    when(record.get(BrahmsCsvField.lldatum)).thenReturn("WGS 84");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("52.165538");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("4.474924");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("12");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.localitynotes)).thenReturn("Tuin Naturalis");
    when(record.get(BrahmsCsvField.habitattext)).thenReturn("Aangelegde tuin");
    when(record.get(BrahmsCsvField.descriptiontext)).thenReturn("Vak van 1 x 1 meter.");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("Zuid-Holland");
    when(record.get(BrahmsCsvField.minoradminname)).thenReturn("Gemeente Leiden");
    when(record.get(BrahmsCsvField.localityname)).thenReturn("Leiden");
    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("The Netherlands");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("Scrophularia");
    when(record.get(BrahmsCsvField.calcfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.calcacceptedname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.synofname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("heterophylla");
    when(record.get(BrahmsCsvField.subspecies)).thenReturn("laciniata");
    when(record.get(BrahmsCsvField.variety)).thenReturn("");
    when(record.get(BrahmsCsvField.forma)).thenReturn("");
    when(record.get(BrahmsCsvField.speciesauthorname)).thenReturn("L.");
    when(record.get(BrahmsCsvField.subspeciesauthorname)).thenReturn("(Koch) P.Fourn.");
    when(record.get(BrahmsCsvField.varietyauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.formaauthorname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.taxstatus)).thenReturn("acc");
    when(record.get(BrahmsCsvField.genusid)).thenReturn("1db69ef8-25f2-4985-9f46-b26ead192ca4");
    when(record.get(BrahmsCsvField.kingdom)).thenReturn("Plantae");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.classname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("Lamiales");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("Scrophulariaceae");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.curationspeciesfullname)).thenReturn("Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.");
    when(record.get(BrahmsCsvField.genushybrid)).thenReturn("AMD");

    BrahmsSpecimenTransformer brahmsSpecimenTransformer = new BrahmsSpecimenTransformer(report, etlStatistics);

    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "AMD.88222");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    Object returned =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "doTransform");

    List<Specimen> list = (List<Specimen>) returned;

    String expectedId = "AMD.88222@BRAHMS";
    String expectedGUID = "https://data.biodiversitydata.nl/naturalis/specimen/AMD.88222";
    String expectedFullScientificName = "Scrophularia heterophylla subsp. laciniata (Waldst. & Kit.) Maire & Petitm.";
    String expectedScientificNameGroup = "scrophularia heterophylla laciniata";
    String expectedGenusMonomial = "Scrophularia";
    String expectedPreviousUnitsTexts = "AMD0088222 | 020491";
    String expectedAssemblageId = "4326b2ce-9b99-4691-bccc-cda09841877d";

    assertNotNull("01", list);
    assertEquals("02", 1, list.size());
    assertEquals("03", expectedId, list.stream().map(Specimen::getId).findFirst().get());
    assertEquals("04", expectedGUID, list.stream().map(Specimen::getUnitGUID).findFirst().get());
    assertEquals(
        "05",
        expectedFullScientificName,
        list.stream()
            .map(i -> i.getIdentifications().get(0).getScientificName().getFullScientificName())
            .findFirst()
            .get());
    assertEquals(
        "06",
        expectedScientificNameGroup,
        list.stream()
            .map(i -> i.getIdentifications().get(0).getScientificName().getScientificNameGroup())
            .findFirst()
            .get());
    assertEquals(
        "07",
        expectedGenusMonomial,
        list.stream()
            .map(i -> i.getIdentifications().get(0).getScientificName().getGenusOrMonomial())
            .findFirst()
            .get());
    assertEquals(
        "08",
        expectedPreviousUnitsTexts,
        list.stream().map(Specimen::getPreviousUnitsText).findFirst().get());
    assertEquals(
            "09",
            expectedAssemblageId,
            list.stream().map(Specimen::getAssemblageID).findFirst().get());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer
   * #gatheringEvent())}.
   *
   * @throws Exception Test to verify gathering event object returned.
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGatheringEvent() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    when(record.get(BrahmsCsvField.continent)).thenReturn("Europe");
    when(record.get(BrahmsCsvField.countryname)).thenReturn("Netherlands");
    when(record.get(BrahmsCsvField.majoradminname)).thenReturn("");
    when(record.get(BrahmsCsvField.localitynotes))
        .thenReturn("Io een rotsachtige rij vochtige berghelling bij het Kolmhans.");
    when(record.get(BrahmsCsvField.collectionyear)).thenReturn("      0.000000");
    when(record.get(BrahmsCsvField.collectionmonth)).thenReturn("      8.000000");
    when(record.get(BrahmsCsvField.collectionday)).thenReturn("      3.000000");
    when(record.get(BrahmsCsvField.latitude)).thenReturn("-1.58");
    when(record.get(BrahmsCsvField.longitude)).thenReturn("114.53333933");
    when(record.get(BrahmsCsvField.elevation)).thenReturn("1");
    when(record.get(BrahmsCsvField.elevationmax)).thenReturn("2");
    when(record.get(BrahmsCsvField.collectors, false)).thenReturn("Mrs A");
    when(record.get(BrahmsCsvField.additionalcollectors, false)).thenReturn("Mrs B");

    BrahmsSpecimenTransformer brahmsSpecimenTransformer =
        new BrahmsSpecimenTransformer(report, etlStatistics);

    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    Object returned = CommonReflectionUtil.callMethod(
            record,
            CSVRecordInfo.class,
            brahmsSpecimenTransformer,
            "getGatheringEvent");
    GatheringEvent ge = (GatheringEvent) returned;

    String expectedContinent = "Europe";
    String expectedCountry = "Netherlands";
    Double expectedLatitude = -1.58d;
    Double expectedLongitude = 114.53334d;
    String expectedAltitude = "1 - 2";
    String expectedAltitudeUnitOfMeasurement = "m";
    String expectedCollectors = "Mrs A; Mrs B";
    String expectedLocalityText = "Europe; Netherlands; Io een rotsachtige rij vochtige berghelling bij het Kolmhans.";

    assertNotNull("01", returned);
    assertEquals("02", expectedContinent, ge.getContinent());

    assertEquals("03", expectedCountry, ge.getCountry());
    assertEquals("04", expectedLatitude, ge.getSiteCoordinates().get(0).getLatitudeDecimal());
    assertEquals("05", expectedLongitude, ge.getSiteCoordinates().get(0).getLongitudeDecimal());

    assertEquals("06", expectedAltitude, ge.getAltitude());
    assertEquals("07", expectedAltitudeUnitOfMeasurement, ge.getAltitudeUnifOfMeasurement());
    assertEquals("08", expectedCollectors, ge.getGatheringPersons().get(0).getFullName());
    assertEquals("09", expectedLocalityText, ge.getLocalityText());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer
   * #getSpecimenIdentification())}.
   *
   * @throws Exception Test to verify the specimen identification object returned.
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGetSpecimenIdentification() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    when(record.get(BrahmsCsvField.typecategory)).thenReturn("holotype, illustrated");
    when(record.get(BrahmsCsvField.determinedby)).thenReturn("Vanthournout, S");
    when(record.get(BrahmsCsvField.determinationyear)).thenReturn("2010");
    when(record.get(BrahmsCsvField.determinationmonth)).thenReturn("11");
    when(record.get(BrahmsCsvField.determinationday)).thenReturn("");

    when(record.get(BrahmsCsvField.kingdom)).thenReturn("Plantae");
    when(record.get(BrahmsCsvField.subkingdom)).thenReturn("");
    when(record.get(BrahmsCsvField.phylum)).thenReturn("");
    when(record.get(BrahmsCsvField.subphylum)).thenReturn("Magnoliopsidae");
    when(record.get(BrahmsCsvField.classname)).thenReturn("");
    when(record.get(BrahmsCsvField.subclassname)).thenReturn("");
    when(record.get(BrahmsCsvField.ordername)).thenReturn("Malpighiales");
    when(record.get(BrahmsCsvField.familyname)).thenReturn("Flacourtiaceae");
    when(record.get(BrahmsCsvField.subfamily)).thenReturn("");
    when(record.get(BrahmsCsvField.tribe)).thenReturn("");
    when(record.get(BrahmsCsvField.subtribe)).thenReturn("");
    when(record.get(BrahmsCsvField.genusname)).thenReturn("Ryania");
    when(record.get(BrahmsCsvField.speciesname)).thenReturn("pyrifera");
    when(record.get(BrahmsCsvField.calcfullname)).thenReturn("Ryania pyrifera (Rich.) Sleumer & Uittien");

    // AuthorShipVerbatim, InfraspecificMarker, infraspecificEpithet
    when(record.get(BrahmsCsvField.forma)).thenReturn("NULL");
    when(record.get(BrahmsCsvField.formaauthorname)).thenReturn("Not me!!!");
    when(record.get(BrahmsCsvField.variety)).thenReturn("");
    when(record.get(BrahmsCsvField.varietyauthorname)).thenReturn("Me neither!!!");
    when(record.get(BrahmsCsvField.subspecies)).thenReturn("null");
    when(record.get(BrahmsCsvField.subspeciesauthorname)).thenReturn("Nope!!!");
    when(record.get(BrahmsCsvField.speciesauthorname)).thenReturn("(Rich.) Sleumer & Uittien");

    // Default classification
    BrahmsSpecimenTransformer brahmsSpecimenTransformer = new BrahmsSpecimenTransformer(report, etlStatistics);

    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);
    Object obj =
        CommonReflectionUtil.callMethod(
            record, CSVRecordInfo.class, brahmsSpecimenTransformer, "getSpecimenIdentification");
    SpecimenIdentification identification = (SpecimenIdentification) obj;

    SpecimenTypeStatus expectedTypeStatus = SpecimenTypeStatus.HOLOTYPE;
    Agent expectedAgent = new Agent("Vanthournout, S");
    OffsetDateTime expectedDateIdentified = OffsetDateTime.of(2010, 11, 1, 0, 0, 0, 0, ZoneOffset.ofHours(0));
    String expectedGenus = "Ryania";
    String expectedKingdom = "Plantae";
    String expectedSpecificEpithet = "pyrifera";
    String expectedAuthorShipVerbatim = "(Rich.) Sleumer & Uittien";

    assertNotNull("01", identification);
    assertEquals("02", expectedTypeStatus, identification.getTypeStatus());
    assertEquals("03", expectedAgent, identification.getIdentifiers().get(0));
    assertEquals("04", expectedDateIdentified, identification.getDateIdentified());
    assertEquals("05", expectedAuthorShipVerbatim, identification.getScientificName().getAuthorshipVerbatim());
    assertNull("06", identification.getScientificName().getInfraspecificMarker());
    assertNull("07", identification.getScientificName().getInfraspecificEpithet());

    assertEquals("08", expectedGenus, identification.getDefaultClassification().getGenus());
    assertEquals("09", expectedSpecificEpithet, identification
            .getDefaultClassification().getSpecificEpithet());
    assertEquals("10", expectedKingdom, identification.getDefaultClassification().getKingdom());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer
   * #getCollectorsFieldNumber())}.
   *
   * @throws Exception Test to verify the collection field number returned.
   */
  @SuppressWarnings("ConstantConditions")
  @Test
  public void testGetCollectorsFieldNumber() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    // TODO: review is this test is complete enough after B8 migration
    when(record.get(BrahmsCsvField.collectors, true)).thenReturn("Unknown ");
    when(record.get(BrahmsCsvField.prefix, true)).thenReturn(" ");
    when(record.get(BrahmsCsvField.fieldnumber, true)).thenReturn("s.n. ");
    when(record.get(BrahmsCsvField.suffix, true)).thenReturn(" ");

    BrahmsSpecimenTransformer brahmsSpecimenTransformer =
        new BrahmsSpecimenTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "AMD.88222");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    Object obj = CommonReflectionUtil.callMethod(
            null,
            CSVRecordInfo.class,
            brahmsSpecimenTransformer,
            "getCollectorsFieldNumber");
    String expectedCollectionNumber = "Unknown s.n.";

    assertNotNull("01", obj);
    assertEquals("02", expectedCollectionNumber, obj.toString());

    when(record.get(BrahmsCsvField.collectors, true)).thenReturn("Hooker, JD");
    when(record.get(BrahmsCsvField.additionalcollectors, true)).thenReturn(" Thomson, T");
    when(record.get(BrahmsCsvField.prefix, true)).thenReturn("");
    when(record.get(BrahmsCsvField.fieldnumber, true)).thenReturn("");
    when(record.get(BrahmsCsvField.suffix, true)).thenReturn(" s.n.");

    brahmsSpecimenTransformer =
            new BrahmsSpecimenTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
            AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "AMD.88222");
    CommonReflectionUtil.setField(
            AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    obj = CommonReflectionUtil.callMethod(
            null,
            CSVRecordInfo.class,
            brahmsSpecimenTransformer,
            "getCollectorsFieldNumber");
    expectedCollectionNumber = "Hooker, JD; Thomson, T s.n.";
    assertEquals("03", expectedCollectionNumber, obj.toString());
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer
   * #getServiceAccessPoints())}.
   *
   * @throws Exception Test to verify the returned service access points
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGetServiceAccessPoints() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    when(record.get(BrahmsCsvField.imagelist))
        .thenReturn("https://medialib.naturalis.nl/file/id/L.3355550/format/large");

    BrahmsSpecimenTransformer brahmsSpecimenTransformer =
        new BrahmsSpecimenTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    Object obj =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "getServiceAccessPoints");

    List<ServiceAccessPoint> actualList = (List<ServiceAccessPoint>) obj;

    String expectedAccessPoint = "https://medialib.naturalis.nl/file/id/L.3355550/format/large";

    String actualAccessPoint =
        actualList.stream().map(i -> i.getAccessUri().toString()).findFirst().get();

    assertNotNull("01", actualList);
    assertEquals("02", expectedAccessPoint, actualAccessPoint);
  }

  /**
   * Test method for {@link nl.naturalis.nba.etl.brahms.BrahmsSpecimenTransformer
   * #getPreviousUnitsText())}.
   *
   * @throws Exception Test to verify values returned by getPreviousUnitsText()
   */
  @SuppressWarnings({"ConstantConditions", "UnreachableCode"})
  @Test
  public void testGetPreviousUnitsText() throws Exception {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);
    Report report = new Report(jobId, SourceSystem.BRAHMS, DocumentType.SPECIMEN);
    ETLStatistics etlStatistics = new ETLStatistics();

    BrahmsSpecimenTransformer brahmsSpecimenTransformer =
        new BrahmsSpecimenTransformer(report, etlStatistics);
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "objectID", "L.3355550");
    CommonReflectionUtil.setField(
        AbstractTransformer.class, brahmsSpecimenTransformer, "input", record);

    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("L  0020601");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("125784");
    Object obj =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "getPreviousUnitsText");
    String previousUnitsText = (String) obj;
    String expectedPreviousUnitsText = "L  0020601 | 125784";
    assertNotNull("01", previousUnitsText);
    assertEquals("02", expectedPreviousUnitsText, previousUnitsText);

    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn("L  0020601");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn(null);
    obj =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "getPreviousUnitsText");
    previousUnitsText = (String) obj;
    expectedPreviousUnitsText = "L  0020601";
    assertNotNull("03", previousUnitsText);
    assertEquals("04", expectedPreviousUnitsText, previousUnitsText);

    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn(null);
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn("125784");
    obj =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "getPreviousUnitsText");
    previousUnitsText = (String) obj;
    expectedPreviousUnitsText = "125784";
    assertNotNull("05", previousUnitsText);
    assertEquals("06", expectedPreviousUnitsText, previousUnitsText);

    when(record.get(BrahmsCsvField.oldbarcode, false)).thenReturn(" L  0020601 ");
    when(record.get(BrahmsCsvField.specimenaccession, false)).thenReturn(" ");
    obj =
        CommonReflectionUtil.callMethod(
            null, CSVRecordInfo.class, brahmsSpecimenTransformer, "getPreviousUnitsText");
    previousUnitsText = (String) obj;
    expectedPreviousUnitsText = "L  0020601";
    assertNotNull("07", previousUnitsText);
    assertEquals("08", expectedPreviousUnitsText, previousUnitsText);
  }
}
