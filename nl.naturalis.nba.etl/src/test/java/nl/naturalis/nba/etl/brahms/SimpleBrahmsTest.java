package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.calcfullname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.forma;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.formaauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.genusname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.speciesname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subspecies;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.subspeciesauthorname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.variety;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.varietyauthorname;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.etl.CSVRecordInfo;
import org.junit.Test;

public class SimpleBrahmsTest {

  @SuppressWarnings("unchecked")
  @Test
  public void testGetAuthorShipVerbatim() {
    CSVRecordInfo<BrahmsCsvField> csvRecord = mock(CSVRecordInfo.class);
    when(csvRecord.get(forma)).thenReturn("forma");
    when(csvRecord.get(formaauthorname)).thenReturn("For A & B");
    when(csvRecord.get(variety)).thenReturn("variety");
    when(csvRecord.get(varietyauthorname)).thenReturn("Var A. & B.");
    when(csvRecord.get(subspecies)).thenReturn("subspecies");
    when(csvRecord.get(subspeciesauthorname)).thenReturn("Sub A. & B.");
    when(csvRecord.get(speciesauthorname)).thenReturn("Spe A. & B.");

    String expected = "For A & B";
    String actual = BrahmsImportUtil.getAuthorShipVerbatim(csvRecord);
    assertEquals("01", expected, actual);

    when(csvRecord.get(forma)).thenReturn("NULL");
    expected = "Var A. & B.";
    actual = BrahmsImportUtil.getAuthorShipVerbatim(csvRecord);
    assertEquals("02", expected, actual);

    when(csvRecord.get(variety)).thenReturn("NULL");
    expected = "Sub A. & B.";
    actual = BrahmsImportUtil.getAuthorShipVerbatim(csvRecord);
    assertEquals("03", expected, actual);

    when(csvRecord.get(subspecies)).thenReturn("NULL");
    expected = "Spe A. & B.";
    actual = BrahmsImportUtil.getAuthorShipVerbatim(csvRecord);
    assertEquals("04", expected, actual);
  }

  /**
   * Test method for {@link
   * nl.naturalis.nba.etl.brahms.BrahmsImportUtil#getScientificName(nl.naturalis.nba.etl.CSVRecordInfo)}.
   *
   * <p>Test to verify getScientificName method returns the expected {@link ScientificName} object
   */
  @SuppressWarnings("unchecked")
  @Test
  public void testGetScientificName_01() {

    CSVRecordInfo<BrahmsCsvField> record = mock(CSVRecordInfo.class);

    when(record.get(calcfullname)).thenReturn("Rhododendron ferrugineum L.");
    when(record.get(varietyauthorname)).thenReturn("L.");
    when(record.get(genusname)).thenReturn("Rhododendron");
    when(record.get(speciesname)).thenReturn("ferrugineum");

    ScientificName expected = new ScientificName();
    expected.setFullScientificName("Rhododendron ferrugineum L.");
    expected.setAuthorshipVerbatim("L.");
    expected.setGenusOrMonomial("Rhododendron");
    expected.setSpecificEpithet("ferrugineum");
    expected.setInfraspecificMarker("");
    expected.setInfraspecificEpithet("");

    ScientificName actual = BrahmsImportUtil.getScientificName(record);
    assertNotNull("01", actual);
    assertEquals("02", expected.getFullScientificName(), actual.getFullScientificName());
  }

}
