package nl.naturalis.nba.etl.nsr;

import static nl.naturalis.nba.dao.DocumentType.TAXON;

import java.io.IOException;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for NSR taxon documents.
 */
public class NsrTaxonJsonNDWriter extends JsonNDWriter<Taxon> {

  public NsrTaxonJsonNDWriter(String sourceFile, ETLStatistics stats) throws IOException {
    super(TAXON, "Nsr", sourceFile, stats);
  }

  public NsrTaxonJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(TAXON, "Nsr", sourceFile, report, stats);
  }
}
