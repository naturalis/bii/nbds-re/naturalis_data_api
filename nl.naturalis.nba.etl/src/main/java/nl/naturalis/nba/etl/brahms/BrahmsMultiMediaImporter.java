package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.api.model.SourceSystem.BRAHMS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_OUTPUT;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_TRUNCATE;
import static nl.naturalis.nba.etl.ETLUtil.getLogger;
import static nl.naturalis.nba.etl.ETLUtil.logDuration;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getCsvFiles;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getDataDir;

import com.univocity.parsers.common.TextParsingException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.exception.DaoException;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.CSVExtractor;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLRuntimeException;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.report.ProcessingError;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Export;
import nl.naturalis.nba.etl.report.Report.Status;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.FileUtil;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Driver class for the import of Brahms multimedia.
 *
 * @author Ayco Holleman
 */
public class BrahmsMultiMediaImporter {

  private static final Logger logger = getLogger(BrahmsMultiMediaImporter.class);
  private final Report report;
  private final int loaderQueueSize;
  private final boolean suppressErrors;
  private final boolean shouldUpdateEs;

  public BrahmsMultiMediaImporter() {
    this(UUID.randomUUID().toString());
  }

  /**
   * Initialise the BrahmsMultiMediaImporter with a jobId.
   *
   * @param jobId of the ETL job
   */
  public BrahmsMultiMediaImporter(String jobId) {
    report = new Report(jobId, SourceSystem.BRAHMS, MULTI_MEDIA_OBJECT);
    report.initialiseUrls();
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    String val = System.getProperty(SYSPROP_LOADER_QUEUE_SIZE, "1000");
    loaderQueueSize = Integer.parseInt(val);
    shouldUpdateEs = !DaoRegistry
        .getInstance()
        .getConfiguration()
        .get(SYSPROP_ETL_OUTPUT, "es")
        .equals("file");
  }

  /**
   * Main.
   *
   * @param args path of the CSV file(s) to be imported
   */
  public static void main(String[] args) {
    try {
      BrahmsMultiMediaImporter importer = new BrahmsMultiMediaImporter();
      if (args.length == 0 || args[0].trim().length() == 0) {
        importer.importCsvFiles();
      } else {
        importer.importCsvFile(args[0]);
      }
    } catch (Throwable t) {
      logger.error("BrahmsMultiMediaImporter terminated unexpectedly!", t);
      System.exit(1);
    } finally {
      ESUtil.refreshIndex(MULTI_MEDIA_OBJECT);
      ESClientManager.getInstance().closeClient();
    }
  }

  /**
   * Import the CSV file or files on path given.
   *
   * @param path path of the csv file(s) to be imported
   */
  public void importCsvFile(String path) {
    File file;
    if (path.startsWith("/")) {
      file = new File(path);
    } else {
      file = FileUtil.newFile(getDataDir(), path);
    }
    if (!file.isFile()) {
      throw new ETLRuntimeException("No such file: " + file.getAbsolutePath());
    }
    importCsvFiles(new File[]{file});
  }

  /**
   * Iterates over the CSV files in the brahms data directory and imports them.
   */
  public void importCsvFiles() {
    importCsvFiles(getCsvFiles());
  }

  /**
   * Imports the Source files in the Brahms data directory.
   */
  public void importCsvFiles(File[] csvFiles) {

    long start = System.currentTimeMillis();
    if (csvFiles.length == 0) {
      logger.info("No CSV files to process");
      report.setFinished();
      return;
    }

    report.setStatus(Status.IN_PROGRESS);
    SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
    ThemeCache.getInstance().resetMatchCounters();
    report.setThemesVersion(ThemeCache.getInstance().getThemesVersion());
    ETLStatistics stats = new ETLStatistics();
    stats.setOneToMany(true);
    if (ConfigObject.isEnabled(SYSPROP_TRUNCATE, true) && !shouldUpdateEs) {
      try {
        ETLUtil.truncate(MULTI_MEDIA_OBJECT, BRAHMS);
      } catch (DaoException e) {
        logger.error("Failed to truncate the Brahms multimedia index: {}", e.getMessage());
        System.exit(1);
      }
    }
    for (File f : csvFiles) {
      processFile(f, stats);
    }
    // Report is ready now
    report.addThemesSummary(ThemeCache.getInstance().getThemesSummary());
    report.setFinished();
    logger.info(report.save());

    SpecimenTypeStatusNormalizer.getInstance().logStatistics();
    ThemeCache.getInstance().logMatchInfo();
    stats.logStatistics(logger, "Multimedia");
    logDuration(logger, getClass(), start);
  }

  private void processFile(File f, ETLStatistics globalStats) {

    long start = System.currentTimeMillis();
    report.addSourceFile(f);
    logger.debug("Processing file {}", f.getAbsolutePath());
    ETLStatistics myStats = new ETLStatistics();
    myStats.setOneToMany(true);

    CSVExtractor<BrahmsCsvField> extractor;
    BrahmsMultiMediaTransformer transformer;
    DocumentObjectWriter<MultiMediaObject> loader = null;
    try {
      extractor = createExtractor(f, myStats);
      transformer = new BrahmsMultiMediaTransformer(report, myStats);
      // Temporary (?) modification to allow for enrichment during the specimen import
      if (DaoRegistry.getInstance()
          .getConfiguration().get(SYSPROP_ETL_ENRICH, "false").equals("true")) {
        transformer.setEnrich(true);
        logger.info("Taxonomic enrichment of Specimen documents: true");
      }
      if (DaoRegistry.getInstance()
          .getConfiguration().get(SYSPROP_ETL_OUTPUT, "file").equals("file")) {
        report.setExport(Export.FILE);
        logger.info("ETL Output: Writing the multimedia documents to the file system");
        try {
          loader = new BrahmsMultiMediaJsonNDWriter(f.getName(), report, myStats);
        } catch (IOException e) {
          logger.error(
              "Failed to create export file for source file {}: {}", f.getName(), e.getMessage());
        }
      } else {
        logger.info("ETL Output: loading the multimedia documents into the document store");
        loader = new BrahmsMultiMediaLoader(loaderQueueSize, myStats);
      }
      for (CSVRecordInfo<BrahmsCsvField> rec : extractor) {
        if (rec == null) {
          continue;
        }
        loader.write(transformer.transform(rec));
        report.save();
        if (myStats.recordsProcessed != 0 && myStats.recordsProcessed % 50000 == 0) {
          logger.info("Records processed: {}", myStats.recordsProcessed);
          logger.info("Documents indexed: {}", myStats.documentsIndexed);
        }
      }
    } catch (TextParsingException e) {
      report.addProcessingError(ProcessingError.PARSING_ERROR, String.format("Parsing of file %s ended at line:%s", f.getName(), e.getLineIndex()));
      logger.error("Parsing of csv file: {} failed!", f.getAbsolutePath());
      logger.error("Processing ended at line: {}", e.getLineIndex());
    } catch (OutOfMemoryError e) {
      String msg = String.format("ETL process ran out of memory while processing file: %s "
          + "Error message: %s", f.getAbsolutePath(), e.getMessage());
      report.addProcessingError(ProcessingError.SOFTWARE_ERROR, String.format("Processing of file %s failed: %s", f.getName(), e.getMessage()));
      logger.error("Parsing of file: {} failed!", f.getAbsolutePath());
      logger.error("Cause: {}", e.getMessage());
    } finally {
      loader.flush();
      IOUtil.close(loader);
      report.save();
    }
    globalStats.add(myStats);
    myStats.logStatistics(logger, "Multimedia");
    logDuration(logger, getClass(), start);
    logger.info(" ");
    logger.info(" ");
  }

  private CSVExtractor<BrahmsCsvField> createExtractor(File f, ETLStatistics stats) {
    CSVExtractor<BrahmsCsvField> extractor = new CSVExtractor<>(f, BrahmsCsvField.class, stats);
    extractor.setSkipHeader(true);
    extractor.setDelimiter(',');
    extractor.setCharset(StandardCharsets.UTF_8);
    extractor.setSuppressErrors(suppressErrors);
    return extractor;
  }
}
