package nl.naturalis.nba.etl;

import org.elasticsearch.action.bulk.BulkItemResponse;

/**
 * Provides information about an index request failure.
 *
 * @author Ayco Holleman
 *
 */
class BulkIndexFailure {

  private final String index;
  private final String type;
  private final Object object;
  private final String message;

  BulkIndexFailure(BulkItemResponse response, Object obj) {
    this.index = response.getIndex();
    this.type = response.getType();
    this.object = obj;
    this.message = response.getFailureMessage();
  }

  /**
   * The ElasticSearch index that the object belongs to.
   *
   * @return the ElasticSearch index that the object belongs to
   */
  @SuppressWarnings("unused")
  String getIndex() {
    return index;
  }

  /**
   * The ElasticSearch type of the object.
   *
   * @return the ElasticSearch type of the object
   */
  @SuppressWarnings("unused")
  String getType() {
    return type;
  }

  /**
   * The object that failed to be indexed.
   *
   * @return the object that failed to be indexed
   */
  @SuppressWarnings("unused")
  Object getObject() {
    return object;
  }

  /**
   * The failure message from ElasticSearch.
   *
   * @return the failure message from ElasticSearch
   */
  @SuppressWarnings("unused")
  String getMessage() {
    return message;
  }

}
