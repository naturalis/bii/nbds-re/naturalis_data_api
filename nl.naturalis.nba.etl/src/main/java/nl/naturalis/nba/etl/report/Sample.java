package nl.naturalis.nba.etl.report;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.File;
import java.util.Objects;
import nl.naturalis.nba.common.json.JsonUtil;

/**
 * A Sample represents a sample of an issue occurring during an ETL import.
 *
 * @author Tom Gilissen
 */
@JsonPropertyOrder({"source", "detail", "fileName", "lineNumber"})
public class Sample {

  @JsonInclude(Include.NON_NULL)
  private String source;
  @JsonInclude(Include.NON_NULL)
  private String detail;
  @JsonInclude(Include.NON_NULL)
  private String fileName;
  @JsonInclude(Include.NON_NULL)
  private Long lineNumber;

  /**
   * Initialise a sample with just the source of the record.
   *
   * @param source  the record source that needs to be sampled
   */
  public Sample(String source) {
    this.source = source;
  }

  /**
   * Initialise a sample with the source of the record and
   * a description of the problem.
   *
   * @param source  the record source that caused the problem
   * @param detail  description of the problem
   */
  public Sample(String source, String detail) {
    this.source = source;
    this.detail = detail;
  }

  /**
   * Initialise a sample with the source of the record,
   * a description of the problem, and the file the record
   * comes from.
   *
   * @param source  String representation of the source record
   * @param file  the file the sample originates from
   */
  public Sample(String source, File file) {
    Objects.requireNonNull(file);
    this.source = source;
    this.fileName = file.getName();
  }

  /**
   * Initialise a sample with the source of the record,
   * a description of the problem, and the file the record
   * comes from.
   *
   * @param source  String representation of the source record
   * @param detail  String describing the problem with this source
   * @param file  the file the sample originates from
   */
  public Sample(String source, String detail, File file) {
    this(source, file);
    this.detail = detail;
  }

  /**
   * Initialise a sample with the source of the record,
   * a description of the problem, and the file the record
   * comes from.
   *
   * @param source  String representation of the source record
   * @param file  the file the sample originates from
   * @param lineNumber  the line number of the record
   */
  public Sample(String source, File file, Long lineNumber) {
    this(source, file);
    this.lineNumber = lineNumber;
  }

  /**
   * Initialise a sample with the source of the record,
   * a description of the problem, the file and line number the
   * record comes from.
   *
   * @param source  String representation of the source record
   * @param detail  String describing the problem with this source
   * @param file  the file the sample originates from
   * @param lineNumber  line number or index number of the record in the source file
   */
  public Sample(String source, String detail, File file, Long lineNumber) {
    this(source, detail, file);
    this.lineNumber = lineNumber;
  }

  /**
   * Initialise a sample with the file and the line number of the line that
   * caused the issue.
   *
   * @param file  the file
   * @param lineNumber  the line number
  */
  public Sample(File file, Long lineNumber) {
    Objects.requireNonNull(file);
    this.fileName = file.getName();
    this.lineNumber = lineNumber;
  }

  @JsonGetter("source")
  public String getSource() {
    return source;
  }

  @JsonGetter("detail")
  public String getDetail() {
    return detail;
  }

  @JsonGetter("fileName")
  public String getFileName() {
    return fileName;
  }

  @JsonGetter("lineNumber")
  public Long getLineNumber() {
    return lineNumber;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Sample)) {
      return false;
    }
    Sample otherSample = (Sample) obj;
    String strOtherSample = JsonUtil.toJson(otherSample);
    String strThisSample = JsonUtil.toJson(this);
    return strThisSample.equals(strOtherSample);
  }

  @Override
  public int hashCode() {
    int result = 17;
    if (source != null) {
      result = 31 * result + source.hashCode();
    }
    if (detail != null) {
      result = 31 * result + detail.hashCode();
    }
    if (fileName != null) {
      result = 31 * result + fileName.hashCode();
    }
    if (lineNumber != null && lineNumber > 0) {
      int n = lineNumber.intValue();
      result = 31 * n;
    }
    return result;
  }

  @Override
  public String toString() {
    return JsonUtil.toJson(this);
  }

}
