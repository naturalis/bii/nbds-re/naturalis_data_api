package nl.naturalis.nba.etl.report;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Objects;
import java.util.HashSet;

/**
 * Represents a log that records samples of an occuring issue during the ETL process.
 * A (predefined) maximum of samples (default = 10) will be recorded in the report
 * by adding an issue with a sample to an IssueLog.
 *
 * @author Tom Gilissen
 */
@JsonPropertyOrder({"level", "description", "count", "samples"})
class IssueLog {

  private final Issue issue;
  private int count = 0;
  @JsonIgnore
  private final int sampleSize;
  private final HashSet<Sample> samples;


  public IssueLog(Issue issue, Sample sample) {
    this.sampleSize = Integer.parseInt(
        System.getProperty("nl.naturalis.nba.etl.report.sampleSize", "10"));
    this.samples = new HashSet<>(sampleSize);
    this.issue = issue;
    this.samples.add(sample);
    count++;
  }

  @JsonIgnore
  public Issue getIssue() {
    return issue;
  }

  @JsonGetter("level")
  public String getLevel() {
    return issue.level.name();
  }

  @JsonGetter("description")
  public String getIssueDescription() {
    return issue.description;
  }

  @JsonGetter("count")
  public int getCount() {
    return count;
  }

  @JsonGetter("samples")
  public HashSet<Sample> getSamples() {
    return samples;
  }

  public void addSample(Sample sample) {
    count++;
    if (samples.size() < sampleSize) {
      samples.add(sample);
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof IssueLog)) {
      return false;
    }
    IssueLog issueLog = (IssueLog) obj;
    return Objects.equal(this, issueLog);
  }
}
