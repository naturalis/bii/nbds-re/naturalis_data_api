package nl.naturalis.nba.etl.nsr;

import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;

import java.io.IOException;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for DCSR multimedia documents.
 */
public class NsrMultiMediaJsonNDWriter extends JsonNDWriter<MultiMediaObject> {

  public NsrMultiMediaJsonNDWriter(String sourceFile, ETLStatistics stats) throws IOException {
    super(MULTI_MEDIA_OBJECT, "Dcsr", sourceFile, stats);
  }

  public NsrMultiMediaJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(MULTI_MEDIA_OBJECT, "Dcsr", sourceFile, report, stats);
  }
}
