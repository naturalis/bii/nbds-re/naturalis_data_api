package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;

import java.io.IOException;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for Brahms multimedia objects.
 */
public class BrahmsMultiMediaJsonNDWriter extends JsonNDWriter<MultiMediaObject> {

  public BrahmsMultiMediaJsonNDWriter(String sourceFile, Report report, ETLStatistics stats)
      throws IOException {
    super(MULTI_MEDIA_OBJECT, "Brahms", sourceFile, report, stats);
  }
}
