package nl.naturalis.nba.etl;

import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.etl.report.Report;

/**
 * Base class for transformers that take JSON documents as their input. Used for NSR.
 *
 * @param <OUTPUT> The type of object that is output from the transformer
 */
@SuppressWarnings({"checkstyle:AbbreviationAsWordInName", "CheckStyle"})
public abstract class AbstractJSONTransformer<OUTPUT extends IDocumentObject>
    extends AbstractTransformer<String, OUTPUT> {

  public AbstractJSONTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
  }
}
