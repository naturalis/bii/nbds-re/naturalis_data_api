package nl.naturalis.nba.etl.crs;

import java.util.UUID;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.ETLRegistry;
import org.apache.logging.log4j.Logger;

/**
 * Class that manages the extraction and transformation of CRS specimens
 * and multimedia from a job file
 *
 * @author Tom Gilissen
 *
 */
public class CrsImportJob {

  private static final Logger logger = ETLRegistry.getInstance().getLogger(CrsImportJob.class);

  /**
   * CrsExtractTransform expects just two arguments: --jobId [String jobId]
   *
   * @param args  --jobId [String jobId]
   */
  public static void main(String[] args) {

    String jobId;
    if (args.length == 2 && args[0].equals("--jobId")) {
      jobId = args[1];
    } else {
      jobId = UUID.randomUUID().toString();
    }

    // Elasticsearch needs to be up and running for taxonomic enrichment
    try {
      ESUtil.esClient();
    } catch (Throwable t) {
      logger.error("Elasticsearch seems to be down. CRS Import is cancelled!");
      ESClientManager.getInstance().closeClient();
      System.exit(1);
    }

    try {
      CrsImportJobSpecimen importJobSpecimen = new CrsImportJobSpecimen(jobId);
      importJobSpecimen.importSpecimens();

      CrsImportJobMultimedia importJobMultimedia = new CrsImportJobMultimedia(jobId);
      importJobMultimedia.importMultimedia();
    }
    catch (Throwable t) {
      logger.error("CrsImportJob terminated unexpectedly!", t);
      System.exit(1);
    }
    finally{
      ESClientManager.getInstance().closeClient();
    }
  }

}
