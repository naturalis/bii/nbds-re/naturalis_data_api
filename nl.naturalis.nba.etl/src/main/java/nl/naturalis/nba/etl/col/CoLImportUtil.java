package nl.naturalis.nba.etl.col;

import java.io.File;
import nl.naturalis.nba.dao.DaoRegistry;

/**
 * Provides common functionality related to the CoL ETL cycle.
 *
 * @author Ayco Holleman
 *
 */
class CoLImportUtil {

  private CoLImportUtil() {}

  public static File getDataDir() {
    return DaoRegistry.getInstance().getConfiguration().getDirectory("col.data.dir");
  }

}
