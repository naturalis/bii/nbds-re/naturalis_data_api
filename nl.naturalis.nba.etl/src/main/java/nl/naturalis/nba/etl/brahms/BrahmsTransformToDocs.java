package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.api.model.SourceSystem.BRAHMS;
import static nl.naturalis.nba.dao.DocumentType.MULTI_MEDIA_OBJECT;
import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_ETL_ENRICH;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_LOADER_QUEUE_SIZE;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_TRUNCATE;
import static nl.naturalis.nba.etl.ETLUtil.logDuration;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.backup;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.getCsvFiles;
import static nl.naturalis.nba.etl.brahms.BrahmsImportUtil.removeBackupExtension;
import static nl.naturalis.nba.utils.TimeUtil.getDuration;

import com.univocity.parsers.common.TextParsingException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.UUID;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.dao.DaoRegistry;
import nl.naturalis.nba.dao.ESClientManager;
import nl.naturalis.nba.dao.util.es.ESUtil;
import nl.naturalis.nba.etl.CSVExtractor;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.DocumentObjectWriter;
import nl.naturalis.nba.etl.ETLRegistry;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ETLUtil;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Report.Export;
import nl.naturalis.nba.utils.ConfigObject;
import nl.naturalis.nba.utils.IOUtil;
import org.apache.logging.log4j.Logger;

/**
 * Manages the transformation of Brahms specimens and multimedia into NBA documents.
 *
 * @author Tom Gilissen
 */
public class BrahmsTransformToDocs {

  protected static final HashSet<String> specimenObjectIdsCache = new HashSet<>();
  private static final Logger logger =
      ETLRegistry.getInstance().getLogger(BrahmsTransformToDocs.class);
  private static final boolean toFile = true;
  private final boolean backup;
  private final boolean parallel;
  private final int loaderQueueSize;
  private final boolean suppressErrors;

  private String jobId;
  private Report specimenReport;
  private Report multimediaReport;


  /**
   * Coordinating class for running the ETL with Brahms source files.
   *
   */
  public BrahmsTransformToDocs() {
    backup = ConfigObject.isEnabled("brahms.backup", true);
    parallel = ConfigObject.isEnabled("brahms.parallel", false);
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    String val = System.getProperty(SYSPROP_LOADER_QUEUE_SIZE, "1000");
    loaderQueueSize = Integer.parseInt(val);
  }

  /**
   * Main method.
   *
   */
  public static void main(String[] args) {

    // TODO: wrap all in a try catch block and secure closing the ES client in finally

    String batchJobId = null;
    if (args.length > 1 && args[0].equalsIgnoreCase("--jobid")) {
      batchJobId = args[1];
    }
    if (batchJobId == null || batchJobId.equals("")) {
      logger.warn("Missing job id. Please provide a job id when using parameter --jobid");
      batchJobId = UUID.randomUUID().toString();
    }
    logger.info("Starting batch job with id: {}", batchJobId);
    try {
      new BrahmsTransformToDocs().importPerFile(batchJobId);
    } catch (Throwable t) {
      logger.error("BrahmsImporter in batch mode terminated unexpectedly!", t);
    } finally {
      ESClientManager.getInstance().closeClient();
    }
  }

  /**
   * This method first imports all specimens, then all multimedia. Note: each CSV file is thus read
   * twice!
   */
  public void batchImport(String batchJobId) {

    logger.info("Started ETL batch transform");

    // Import specimens
    BrahmsSpecimenImporter specimenImporter = new BrahmsSpecimenImporter(batchJobId);
    specimenImporter.importCsvFiles();

    // Import multimedia
    BrahmsMultiMediaImporter multiMediaImporter = new BrahmsMultiMediaImporter(batchJobId);
    multiMediaImporter.importCsvFiles();

    // Remove source files
    for (File f : getCsvFiles()) {
      logger.debug("Deleting source file {}", f.getName());
      if (!f.delete()) {
        logger.error("Failed to remove source file: {}", f.getAbsoluteFile());
      }
    }
    logger.debug("Brahms batch import has finished");
  }

  /**
   * This method processes each CSV files only once, extracting and loading both specimens and
   * multimedia at the same time.
   */
  public void importPerFile(String batchJobId) {

    long start = System.currentTimeMillis();

    File[] csvFiles = getCsvFiles();
    if (csvFiles.length == 0) {
      logger.info("No CSV files to process");
      return;
    }

    SpecimenTypeStatusNormalizer.getInstance().resetStatistics();
    ThemeCache.getInstance().resetMatchCounters();
    /* Global statistics for specimen import (across all files) */
    ETLStatistics sStats = new ETLStatistics();
    /* Global statistics for multimedia import (across all files) */
    ETLStatistics mStats = new ETLStatistics();
    mStats.setOneToMany(true);

    try {
      for (File f : csvFiles) {
        processFile(f, sStats, mStats);
      }
      if (backup) {
        backup();
      }
    } catch (Throwable t) {
      logger.error(getClass().getSimpleName() + " terminated unexpectedly!", t);
      specimenReport.setFailed();
      multimediaReport.setFailed();
    } finally {
      specimenReport.save();
      multimediaReport.save();
    }
    specimenReport.setFinished();
    multimediaReport.setFinished();
    specimenReport.save();
    multimediaReport.save();
    SpecimenTypeStatusNormalizer.getInstance().logStatistics();
    ThemeCache.getInstance().logMatchInfo();
    sStats.logStatistics(logger, "Specimens");
    mStats.logStatistics(logger, "Multimedia");
    logDuration(logger, getClass(), start);
  }

  /**
   * Backs up the CSV files in the Brahms data directory by appending a "&#46;imported" extension to
   * the file name.
   */
  public void backupSourceFiles() {
    backup();
  }

  /**
   * Removes the "&#46;imported" file name extension from the files in the Brahms data directory.
   * Nice for repitive testing. Not meant for production purposes.
   */
  public void reset() {
    removeBackupExtension();
  }

  private void processFile(File f, ETLStatistics sStats, ETLStatistics mStats) {
    long start = System.currentTimeMillis();
    logger.info("Processing file {}", f.getAbsolutePath());
    /* Statistics for specimen import (current file) */
    ETLStatistics specimenStats = new ETLStatistics();
    /* Statistics for multimedia import (current file) */
    ETLStatistics multimediaStats = new ETLStatistics();
    multimediaStats.setOneToMany(true);
    ETLStatistics extractionStats = new ETLStatistics();
    CSVExtractor<BrahmsCsvField> extractor;
    BrahmsSpecimenTransformer specimenTransformer;
    BrahmsMultiMediaTransformer multimediaTransformer;
    DocumentObjectWriter<Specimen> specimenLoader = null;
    DocumentObjectWriter<MultiMediaObject> multimediaLoader = null;
    try {
      extractor = createExtractor(f, extractionStats);
      specimenTransformer = new BrahmsSpecimenTransformer(specimenReport, specimenStats);
      multimediaTransformer = new BrahmsMultiMediaTransformer(multimediaReport, multimediaStats);

      // Temporary (?) modification to allow for enrichment during the specimen import
      if (DaoRegistry.getInstance()
          .getConfiguration()
          .get(SYSPROP_ETL_ENRICH, "false")
          .equals("true")) {
        specimenTransformer.setEnrich();
        specimenReport.doEnrich();
        logger.info("Taxonomic enrichment of Specimen documents: true");
        multimediaTransformer.setEnrich(true);
        multimediaReport.doEnrich();
        logger.info("Taxonomic enrichment of Multimedia documents: true");
      }

      try {

        logger.info("ETL Output: Writing the documents to the file system");

        specimenLoader = new BrahmsSpecimenJsonNDWriter(f.getName(), specimenReport, specimenStats);
        specimenReport.setExport(Export.FILE);

        multimediaLoader = new BrahmsMultiMediaJsonNDWriter(f.getName(), multimediaReport, multimediaStats);
        multimediaReport.setExport(Export.FILE);

      } catch (IOException e) {
        logger.error("Failed to create export file for source file {}: {}", f.getName(), e.getMessage());
      }
      for (CSVRecordInfo<BrahmsCsvField> rec : extractor) {
        if (rec == null) {
          continue;
        }
        assert specimenLoader != null;
        specimenLoader.write(specimenTransformer.transform(rec));

        assert multimediaLoader != null;
        multimediaLoader.write(multimediaTransformer.transform(rec));

        if (specimenStats.recordsProcessed != 0 && specimenStats.recordsProcessed % 50000 == 0) {
          logger.info("Records processed: {}", specimenStats.recordsProcessed);
          logger.info("Specimen documents transformed: {}", specimenStats.documentsIndexed);
          logger.info("Multimedia documents transformed: {}", multimediaStats.documentsIndexed);
        }
      }
    } catch (TextParsingException e) {
      logger.error("Parsing of csv file: {} failed!", f.getAbsolutePath());
      logger.error("Processing ended at line: {}", e.getLineIndex());
    } catch (OutOfMemoryError e) {
      logger.error("Parsing of file: {} failed!", f.getAbsolutePath());
      logger.error("Cause: {}", e.getMessage());
    } finally {
      specimenReport.save();
      multimediaReport.save();
      IOUtil.close(specimenLoader, multimediaLoader);
    }
    specimenReport.save();
    multimediaReport.save();
    specimenStats.add(extractionStats);
    multimediaStats.add(extractionStats);
    specimenStats.logStatistics(logger, "Specimens");
    multimediaStats.logStatistics(logger, "Multimedia");
    sStats.add(specimenStats);
    mStats.add(multimediaStats);
    logger.info("Importing {} took {}", f.getName(), getDuration(start));
    logger.info(" ");
    logger.info(" ");
  }

  @SuppressWarnings("DuplicatedCode")
  private CSVExtractor<BrahmsCsvField> createExtractor(File f, ETLStatistics extractionStats) {
    CSVExtractor<BrahmsCsvField> extractor =
        new CSVExtractor<>(f, BrahmsCsvField.class, extractionStats);
    extractor.setSkipHeader(true);
    extractor.setDelimiter(',');
    extractor.setCharset(Charset.forName("Windows-1252"));
    extractor.setSuppressErrors(suppressErrors);
    return extractor;
  }
}
