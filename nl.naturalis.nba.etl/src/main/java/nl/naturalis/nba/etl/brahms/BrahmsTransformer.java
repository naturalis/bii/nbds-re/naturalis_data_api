package nl.naturalis.nba.etl.brahms;

import static nl.naturalis.nba.etl.ETLConstants.SYSPROP_SUPPRESS_ERRORS;
import static nl.naturalis.nba.etl.ETLUtil.getTestGenera;
import static nl.naturalis.nba.etl.ETLUtil.hasValue;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.additionalcollectors;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectionday;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectionmonth;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectionyear;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.collectors;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.continent;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.countryname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.elevation;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.elevationmax;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.genusname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.habitattext;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.latitude;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.localitynotes;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.longitude;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.majoradminname;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.specimenbarcode;
import static nl.naturalis.nba.etl.brahms.BrahmsCsvField.typecategory;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.time.ZoneOffset;
import java.util.Collections;
import nl.naturalis.nba.api.model.GatheringEvent;
import nl.naturalis.nba.api.model.GatheringSiteCoordinates;
import nl.naturalis.nba.api.model.IDocumentObject;
import nl.naturalis.nba.api.model.Person;
import nl.naturalis.nba.api.model.SpecimenTypeStatus;
import nl.naturalis.nba.etl.AbstractCSVTransformer;
import nl.naturalis.nba.etl.CSVRecordInfo;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.ThemeCache;
import nl.naturalis.nba.etl.normalize.SpecimenTypeStatusNormalizer;
import nl.naturalis.nba.etl.normalize.UnmappedValueException;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.utils.ConfigObject;

@SuppressWarnings("DuplicatedCode")
abstract class BrahmsTransformer<T extends IDocumentObject>
    extends AbstractCSVTransformer<BrahmsCsvField, T> {

  static final SpecimenTypeStatusNormalizer typeStatusNormalizer;
  static final ThemeCache themeCache;

  private static final String MSG_INVALID_NUMBER =
      "Invalid number in field %s: \"%s\" (value set to 0)";
  private static final String MSG_INVALID_DATE =
      "Unable to construct date for year=\"%s\";month=\"%s\";day=\"%s\": %s";

  static {
    typeStatusNormalizer = SpecimenTypeStatusNormalizer.getInstance();
    themeCache = ThemeCache.getInstance();
  }

  private final String[] testGenera;

  BrahmsTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
    suppressErrors = ConfigObject.isEnabled(SYSPROP_SUPPRESS_ERRORS);
    testGenera = getTestGenera();
  }

  /*
   * This method exists for testing purposes. It allows you to process
   * only records with a specified value in the column GENUS
   * You can specify the value with: -Dnl.naturalis.nba.etl.testGenera=
   * If no value is specified, all records will be processed.
   *
   * @return    true for test records; false for other records
   */
  @Override
  protected boolean skipRecord() {
    return testGenera != null && !isTestSetGenus();
  }

  @Override
  public String getObjectID() {
    return input.get(specimenbarcode);
  }

  SpecimenTypeStatus getTypeStatus() {
    try {
      return typeStatusNormalizer.map(input.get(typecategory));
    } catch (UnmappedValueException e) {
      if (logger.isDebugEnabled()) {
        debug(e.getMessage());
      }
      return null;
    }
  }

  void populateGatheringEvent(GatheringEvent ge, CSVRecordInfo<BrahmsCsvField> csvRecord) {
    ge.setWorldRegion(csvRecord.get(continent));
    ge.setContinent(csvRecord.get(continent));
    ge.setCountry(csvRecord.get(countryname));
    ge.setProvinceState(csvRecord.get(majoradminname));
    setLocalityInfo(ge, csvRecord);
    ge.setBiotopeText(csvRecord.get(habitattext));
    String y = csvRecord.get(collectionyear);
    String m = csvRecord.get(collectionmonth);
    String d = csvRecord.get(collectionday);
    ge.setDateTimeBegin(getDate(y, m, d, false));
    ge.setDateTimeEnd(getDate(y, m, d, true));
    setGatheringSiteCoordinates(ge, csvRecord);
    setAltitude(ge, csvRecord);
    setCollector(ge, csvRecord);
  }

  /*
   * The string value "-9999" is invalid and considered a NULL value.
   */
  private String setNullIfInvalid(String s) {
    if (s == null || s.trim().equals("-9999") || s.equalsIgnoreCase("NULL")) {
      return null;
    }
    return s.trim();
  }

  Double getDouble(CSVRecordInfo<BrahmsCsvField> csvRecord, BrahmsCsvField field) {
    String s = csvRecord.get(field);
    if (s == null || s.equalsIgnoreCase("NULL")) {
      return null;
    }
    try {
      return Double.valueOf(s);
    } catch (NumberFormatException e) {
      if (!suppressErrors) {
        warn(String.format(MSG_INVALID_NUMBER, field.ordinal(), s));
      }
      return null;
    }
  }
  
  /*
   * Constructs a Date object from the date fields in a Brahms export file.
   * This method can be used to construct a start or end date from
   * problematic gathering event dates in the source data. If year is empty
   * or zero, null is returned. If month is empty or zero, the month is set
   * to january or december, depending on the value of lastDayOfMonth. If
   * day is empty or zero, the day is set to the first day or the last day
   * of the month depending on the value of the lastDayOfMonth argument. If
   * year, month or day are not numeric, null is returned and a warning
   * given. If month or day are out-of-range (e.g. 13 for month), the
   * result is undefined.
   */
  OffsetDateTime getDate(String year, String month, String day, boolean lastDayOfMonth) {
    try {

      int yearInt = parseYear(year);
      int monthInt = parseMonth(month, lastDayOfMonth);
      int dayInt = parseDay(day);

      LocalDate date;
      if (dayInt == -1) {
        if (!lastDayOfMonth) {
          date = LocalDate.of(yearInt, monthInt, 1);
        } else {
          date = YearMonth.of(yearInt, monthInt).atEndOfMonth();
        }
      } else {
        date = LocalDate.of(yearInt, monthInt, dayInt);
      }
      return date.atStartOfDay().atOffset(ZoneOffset.UTC);
    } catch (Exception e) {
      if (!suppressErrors) {
        warn(String.format(MSG_INVALID_DATE, year, month, day, e.getMessage()));
      }
      return null;
    }
  }

  OffsetDateTime getDate(String year, String month, String day) {
    try {
      if (year == null || year.isEmpty() || year.equalsIgnoreCase("NULL")) {
        return null;
      }
      int yearInt = (int) Float.parseFloat(year.trim());
      if (yearInt == 0) {
        return null;
      }
      if (month == null || month.isEmpty() || month.equalsIgnoreCase("NULL")) {
        return null;
      }
      int monthInt = (int) Float.parseFloat(month.trim());
      if (monthInt == 0) {
        return null;
      }
      int dayInt;
      if (day == null || day.isEmpty() || day.equalsIgnoreCase("NULL")) {
        dayInt = 1;
      } else {
        dayInt = (int) Float.parseFloat(day.trim());
        if (dayInt == 0) {
          dayInt = 1;
        }
      }
      LocalDate date = LocalDate.of(yearInt, monthInt, dayInt);
      return date.atStartOfDay().atOffset(ZoneOffset.UTC);
    } catch (Exception e) {
      if (!suppressErrors) {
        warn(String.format(MSG_INVALID_DATE, year, month, day, e.getMessage()));
      }
      return null;
    }
  }

  private boolean isTestSetGenus() {
    String genus = input.get(genusname);
    if (genus == null) {
      return false;
    }
    genus = genus.toLowerCase();
    for (String s : testGenera) {
      if (s.equals(genus)) {
        return true;
      }
    }
    return false;
  }

  private int parseYear(String str) {
    if (str == null || str.isEmpty() || str.equalsIgnoreCase("NULL")) {
      throw new IllegalArgumentException("Invalid year: " + str);
    }
    try {
        return (int) Float.parseFloat(str.trim());
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Invalid year: " + str);
    }
  }

  private int parseMonth(String str, boolean lastDayOfMonth) {
    int month;
    if (str == null || str.isEmpty() || str.equalsIgnoreCase("NULL")) {
      month = -1;
    } else {
      try {
        month = (int) Float.parseFloat(str.trim());
      } catch (NumberFormatException e) {
        throw new IllegalArgumentException("Invalid month: " + str);
      }
      if (month == 0) {
        month = -1;
      }
    }
    if (month == -1) {
      if (lastDayOfMonth) {
        month = 12;
      } else {
        month = 1;
      }
    }
    return month;
  }

  private int parseDay(String str) {
    int dayInt;
    if (str == null || str.isEmpty() || str.equalsIgnoreCase("NULL")) {
      dayInt = -1;
    } else {
      try {
      dayInt = (int) Float.parseFloat(str.trim());
      } catch (NumberFormatException e) {
        throw new IllegalArgumentException("Invalid day: " + str);
      }
      if (dayInt == 0) {
        dayInt = -1;
      }
    }
    return dayInt;
  }

  private void setLocalityInfo(GatheringEvent ge, CSVRecordInfo<BrahmsCsvField> csvRecord) {
    StringBuilder sb = new StringBuilder(50);
    if (ge.getWorldRegion() != null && !ge.getWorldRegion().isEmpty()) {
      sb.append(ge.getWorldRegion());
    }
    if (ge.getCountry() != null && !ge.getCountry().isEmpty()) {
      if (!sb.isEmpty()) {
        sb.append("; ");
      }
      sb.append(ge.getCountry());
    }
    if (ge.getProvinceState() != null && !ge.getProvinceState().isEmpty()) {
      if (!sb.isEmpty()) {
        sb.append("; ");
      }
      sb.append(ge.getProvinceState());
    }
    String locNotes = csvRecord.get(localitynotes);
    if (locNotes != null && !locNotes.isEmpty()) {
      ge.setLocality(locNotes);
      if (!sb.isEmpty()) {
        sb.append("; ");
      }
      sb.append(locNotes);
    }
    ge.setLocalityText(sb.toString());
  }

  private void setGatheringSiteCoordinates(GatheringEvent ge, CSVRecordInfo<BrahmsCsvField> csvRecord) {
    Double lat = getDouble(csvRecord, latitude);
    Double lon = getDouble(csvRecord, longitude);
    if (lat != null && lon != null && lat == 0D && lon == 0D) {
      lat = null;
      lon = null;
    }
    if (lon != null && (lon < -180D || lon > 180D)) {
      error("Invalid longitude: " + lon);
      lon = null;
    }
    if (lat != null && (lat < -90D || lat > 90D)) {
      error("Invalid latitude: " + lat);
      lat = null;
    }
    if (lat != null || lon != null) {
      ge.setSiteCoordinates(Collections.singletonList(new GatheringSiteCoordinates(round(lat), round(lon))));
    }
  }

  // Utility method for limiting the number of decimals to max 5 (NBAX-787)
  private Double round(Double value) {
    if (value == null) {
      return null;
    }
    DecimalFormat df = new DecimalFormat("#.#####");
    df.setRoundingMode(RoundingMode.HALF_DOWN);
    String str = df.format(value);
    return Double.valueOf(str);
  }

  private void setAltitude(GatheringEvent ge, CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String minelev = setNullIfInvalid(csvRecord.get(elevation));
    String maxelev = setNullIfInvalid(csvRecord.get(elevationmax));
    if (minelev != null) {
      if (maxelev == null || maxelev.equals(minelev) || maxelev.equals("0")) {
        ge.setAltitude(minelev);
      } else {
        ge.setAltitude(minelev.concat(" - ").concat(maxelev));
      }
    } else if (maxelev != null && !maxelev.equals("0")) {
      ge.setAltitude(maxelev);
    }
    if (ge.getAltitude() != null && !ge.getAltitude().isEmpty()) {
      ge.setAltitudeUnifOfMeasurement("m");
    }
  }

  private void setCollector(GatheringEvent ge, CSVRecordInfo<BrahmsCsvField> csvRecord) {
    String collector = csvRecord.get(collectors, false);
    String additionalCollectors = csvRecord.get(additionalcollectors, false);
    String collectorStr = "";
    if (hasValue(collector)) {
      collectorStr = collector.trim();
    }
    if (hasValue(additionalCollectors)) {
      collectorStr = collectorStr.concat("; ").concat(additionalCollectors.trim());
    }
    if (!collectorStr.isEmpty()) {
      ge.setGatheringPersons(Collections.singletonList(new Person(collectorStr)));
    }
  }

}
