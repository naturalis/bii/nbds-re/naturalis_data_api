package nl.naturalis.nba.etl.report;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;
import nl.naturalis.common.StringMethods;
import nl.naturalis.nba.api.model.SourceSystem;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.DocumentType;

/**
 * A JavaBean representing data about the entire ETL process.
 * It includes, among others, the following details:
 *
 * <ul>
 *   <li>unique id of the ETL process
 *   <li>start and end time
 *   <li>the sourcesystem concerned
 *   <li>the sourcefiles and their order of processing
 *   <li>the total number of records processed
 *   <li>the total number of documents created
 *   <li>the total number of records rejected
 *   <li>a list of reasons for rejection (issues), including samples*
 *   <li>a list of processing errors
 * </ul>
 *
 * <p>*Note: the default sample size is 10 records. This can be
 * changed by setting the system property:</p>
 * <p>
 * -Dnl.naturalis.nba.etl.report.sampleSize=25
 * </p>
 *
 * @author Tom Gilissen
 *
 */
@JsonPropertyOrder({
    "jobId",
    "reportName",
    "timeStarted",
    "timeFinished",
    "exportFile",
    "status",
    "sourceSystem",
    "documentType",
    "enrich",
    "export",
    "sourceFiles",
    "recordsIngested",
    "recordsSkipped",
    "recordsProcessed",
    "recordsRejected",
    "urlsIngested",
    "urlsProcessed",
    "urlsRejected",
    "documentsCreated",
    "themesVersion",
    "themes",
    "issues",
    "processingErrors"
})
public class Report {

  private final String reportName;
  private final String jobId;
  private Status status;
  private final String timeStarted;
  private String timeFinished;
  private final ArrayList<String> sourceFiles = new ArrayList<>();
  private final IssueLogger issues;
  private final String sourceSystem;
  private final String documentType;
  private String exportFile;
  private boolean enrich = false;
  private Export export;
  public int recordsIngested = 0;
  public int recordsSkipped = 0;
  public int recordsRejected = 0;
  public int recordsProcessed = 0;
  @JsonInclude(Include.NON_NULL)
  public Integer urlsIngested;
  @JsonInclude(Include.NON_NULL)
  public Integer urlsProcessed;
  @JsonInclude(Include.NON_NULL)
  public Integer urlsRejected;
  public int documentsCreated = 0;
  private String themesVersion;
  private TreeMap<String, Integer> themes;
  @JsonInclude(Include.NON_EMPTY)
  private final HashMap<ProcessingError, List<String>> processingErrors = new HashMap<>();

  /**
   * Report containing data about the ETL process.
   *
   * @param jobId : jobId of the import
   * @param sourceSystem : SourceSystem of the import
   * @param documentType : either specimen, multimediaobject or taxon
   */
  public Report(String jobId, SourceSystem sourceSystem, DocumentType<?> documentType) {
    this.jobId = jobId;
    this.timeStarted = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    this.sourceSystem = sourceSystem.getCode();
    this.documentType = documentType.getName();
    this.reportName =
        "etl-report-"
            + this.sourceSystem.toLowerCase()
            + "-"
            + this.documentType.toLowerCase()
            + "-"
            + this.jobId
            + ".json";
    this.status = Status.INITIALISED;
    issues = new IssueLogger();
  }

  /**
   * Counting urls is only useful for Brahms multimedia imports.
   * For all other sources urls counts can stay null.
   */
  public void initialiseUrls() {
    this.urlsIngested = 0;
    this.urlsProcessed = 0;
    this.urlsRejected = 0;
  }

  public String getJobId() {
    return jobId;
  }

  public String getTimeStarted() {
    return timeStarted;
  }

  @SuppressWarnings({"unused", "RedundantSuppression"})
  public String getTimeFinished() {
    return timeFinished;
  }

  public String getSourceSystem() {
    return sourceSystem;
  }

  public String getDocumentType() {
    return documentType;
  }

  public String getReportName() {
    return reportName;
  }

  public ArrayList<String> getSourceFiles() {
    return sourceFiles;
  }

  public String getStatus() {
    return status.name();
  }

  /**
   * Set the status of the report.
   *
   * @param status : the status of the ETL Process
   */
  public void setStatus(Status status) {
    if (!failed()) {
      this.status = status;
    }
    save();
  }

  public void setThemesVersion(String version) {
    this.themesVersion = version;
  }

  public String getThemesVersion() {
    return themesVersion;
  }

  public void addSourceFile(File f) {
    sourceFiles.add(f.getName());
  }

  /**
   * Add an issue to the report.
   *
   * @param issue  the issue appearing in the ETL proces
   * @param sample  the current sample showing where this issue applies
   */
  public void addIssue(Issue issue, Sample sample) {
    issues.addIssue(issue, sample);
  }

  @JsonGetter("issues")
  public HashMap<Issue, IssueLog> getIssues() {
    return issues.getIssues();
  }

  /**
   * Add a ProcessingError to the report.
   *
   * @param error  a FileProcessingError
   */
  public void addProcessingError(ProcessingError error, String description) {
    Objects.requireNonNull(error);
    if (!processingErrors.containsKey(error)) {
      List<String> descriptions = new ArrayList<>();
      descriptions.add(description);
      processingErrors.put(error, descriptions);
    }
    status = Status.FAILED;
    save();
  }

  /**
   * Add a file and description as a FileProcessingError to the report.
   *
   * @param error  the type of processing error
   * @param file  the file the error relates to
   */
  public void addProcessingError(ProcessingError error, File file) {
    Objects.requireNonNull(error);
    Objects.requireNonNull(file);
    String description = String.format("Processing of file %s failed", file.getName());
    addProcessingError(error, description);
  }

  public HashMap<ProcessingError, List<String>> getProcessingErrors() {
    return processingErrors;
  }


  /**
   * Set the status of the ETL process as FINISHED.
   */
  public void setFinished() {
    if (!failed()) {
      this.status = Status.FINISHED;
    }
    timeFinished = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    save();
  }

  public boolean failed() {
    return (status.equals(Status.FAILED));
  }

  public void setFailed() {
    this.status = Status.FAILED;
    setFinished();
  }

  public boolean isReady() {
    return (timeFinished != null);
  }

  /**
   * Save a copy of the report to the file system.
   *
   * @return a string displaying information about saving the file
   */
  public String save() {
    String logDir = StringMethods.rtrim(
        System.getProperty("nl.naturalis.nba.etl.logDir", "/var/log/etl"), '/');
    String reportFile = logDir.concat("/").concat(reportName);
    try {
      PrintWriter printWriter = new PrintWriter(reportFile);
      printWriter.write(JsonUtil.toPrettyJson(this));
      printWriter.close();
      return "Wrote ETL report to: " + reportFile;
    } catch (FileNotFoundException e) {
      return ("Failed to write ETL Report: " + reportFile);
    }
  }

  public boolean isEnrich() {
    return enrich;
  }

  public void doEnrich() {
    this.enrich = true;
  }

  public String getExport() {
    return export.destination;
  }

  public void setExport(Export export) {
    this.export = export;
  }

  /**
   * Add the themes summary to the report.
   *
   * @param summary from the @ThemeCache
   */
  public void addThemesSummary(TreeMap<String, Integer> summary) {
    if (summary != null && summary.size() > 0) {
      this.themes = summary;
    }
  }

  public void setExportFile(String fileName) {
    this.exportFile = fileName;
  }

  public String getExportFile() {
    return exportFile;
  }

  public TreeMap<String, Integer> getThemes() {
    return themes;
  }

  /**
   * Status of the report.
   */
  public enum Status {
    INITIALISED,
    IN_PROGRESS,
    FINISHED,
    FAILED
  }

  /**
   * ETL destination.
   */
  public enum Export {

    FILE("file"),
    ES("Document Store");

    String destination;

    Export(String destination) {
      this.destination = destination;
    }
  }
}
