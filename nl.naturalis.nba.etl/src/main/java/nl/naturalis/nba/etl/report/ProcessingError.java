package nl.naturalis.nba.etl.report;

/**
 * ProcessingError represents errors that can occur during the transformation process.
 *
 * @author Tom Gilissen
 */
public enum ProcessingError {

  IO_ERROR(
      "An error occurred while trying to create a file or directory."),
  OUT_OF_MEMORY_ERROR(
      "ETL process ran out of memory."),
  PARSING_ERROR(
      "Parsing of a source file failed."),
  SOFTWARE_ERROR(
      "A software error occurred while processing a source file. The ETL Process was disrupted.");

  public final String description;

  ProcessingError(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }


}
