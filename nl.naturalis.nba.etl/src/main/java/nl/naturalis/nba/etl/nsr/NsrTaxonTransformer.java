package nl.naturalis.nba.etl.nsr;

import static nl.naturalis.nba.api.model.SourceSystem.NSR;
import static nl.naturalis.nba.api.model.TaxonomicStatus.ACCEPTED_NAME;
import static nl.naturalis.nba.api.model.TaxonomicStatus.ALTERNATIVE_NAME;
import static nl.naturalis.nba.api.model.TaxonomicStatus.BASIONYM;
import static nl.naturalis.nba.api.model.TaxonomicStatus.HOMONYM;
import static nl.naturalis.nba.api.model.TaxonomicStatus.INVALID_NAME;
import static nl.naturalis.nba.api.model.TaxonomicStatus.MISIDENTIFICATION;
import static nl.naturalis.nba.api.model.TaxonomicStatus.MISSPELLED_NAME;
import static nl.naturalis.nba.api.model.TaxonomicStatus.NOMEN_NUDUM;
import static nl.naturalis.nba.api.model.TaxonomicStatus.PREFERRED_NAME;
import static nl.naturalis.nba.api.model.TaxonomicStatus.SYNONYM;
import static nl.naturalis.nba.dao.util.es.ESUtil.getElasticsearchId;
import static nl.naturalis.nba.etl.ETLUtil.getTestGenera;
import static nl.naturalis.nba.etl.TransformUtil.setScientificNameGroup;
import static nl.naturalis.nba.etl.nsr.NsrImportUtil.val;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import nl.naturalis.nba.api.model.DefaultClassification;
import nl.naturalis.nba.api.model.License;
import nl.naturalis.nba.api.model.Monomial;
import nl.naturalis.nba.api.model.Person;
import nl.naturalis.nba.api.model.Reference;
import nl.naturalis.nba.api.model.ScientificName;
import nl.naturalis.nba.api.model.Taxon;
import nl.naturalis.nba.api.model.TaxonDescription;
import nl.naturalis.nba.api.model.TaxonomicStatus;
import nl.naturalis.nba.api.model.VernacularName;
import nl.naturalis.nba.common.es.ESDateInput;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.etl.AbstractJSONTransformer;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.nsr.model.Author;
import nl.naturalis.nba.etl.nsr.model.Classification;
import nl.naturalis.nba.etl.nsr.model.Description;
import nl.naturalis.nba.etl.nsr.model.Name;
import nl.naturalis.nba.etl.nsr.model.NsrTaxon;
import nl.naturalis.nba.etl.nsr.model.Status;
import nl.naturalis.nba.etl.report.Issue;
import nl.naturalis.nba.etl.report.Report;
import nl.naturalis.nba.etl.report.Sample;

/**
 * The transformer component in the NSR ETL cycle for taxa.
 *
 * @author Ayco Holleman
 * @author Tom Gilissen
 */
class NsrTaxonTransformer extends AbstractJSONTransformer<Taxon> {

  private static final HashMap<String, TaxonomicStatus> translations = new HashMap<>();

  static {
    translations.put("isBasionymOf", BASIONYM);
    translations.put("isHomonymOf", HOMONYM);
    translations.put("isMisidentificationOf", MISIDENTIFICATION);
    translations.put("isMisspelledNameOf", MISSPELLED_NAME);
    translations.put("isNomenNudumOf", NOMEN_NUDUM);
    // TODO: this should be deleted after the NSR source has been cleaned of this misspelling
    translations.put("isNomenNudemOf", NOMEN_NUDUM);
    translations.put("isPreferredNameOf", PREFERRED_NAME);
    translations.put("isSynonymOf", SYNONYM);
    translations.put("isSynonymSLOf", SYNONYM);
    translations.put("isInvalidNameOf", INVALID_NAME);
    translations.put("isValidNameOf", ACCEPTED_NAME);
    translations.put("isAlternativeNameOf", ALTERNATIVE_NAME);
  }

  private static final List<String> allowedTaxonRanks =
      Arrays.asList(
          "species",
          "subspecies",
          "varietas",
          "cultivar",
          "forma_specialis",
          "forma",
          "nothospecies",
          "nothosubspecies",
          "nothovarietas",
          "subforma");

  private static final ObjectMapper objectMapper = new ObjectMapper();

  private final String[] testGenera;
  private NsrTaxon nsrTaxon;

  NsrTaxonTransformer(Report report, ETLStatistics stats) {
    super(report, stats);
    testGenera = getTestGenera();
  }

  @Override
  protected String getObjectID() {
    return nsrTaxon.getNsr_id();
  }

  /**
   * Checks if the String provided is valid JSON and if it can be converted
   * to a valid Taxon Document. 3 elements are checked for null values (nsr_id,
   * nsr_parent_id, and url) so we can report if any one of them has no value.
   *
   * @param jsonStr  the json string from the source file
   * @return true when the string provided is a valid json taxon document; false otherwise
   */
  protected boolean isValidJson(String jsonStr) {
    try {
      JsonNode jsonNode = objectMapper.readTree(jsonStr);
      if (jsonNode.get("nsr_id").textValue() == null) {
        report.recordsRejected++;
        report.addIssue(Issue.MISSING_ID, new Sample(input));
        stats.recordsRejected++;
        if (!suppressErrors) {
          error("Missing ID. Record rejected.");
        }
        return false;
      }
      if (jsonNode.get("nsr_id_parent").textValue() == null) {
        report.recordsRejected++;
        report.addIssue(Issue.MISSING_PARENT_ID, new Sample(input));
        stats.recordsRejected++;
        if (!suppressErrors) {
          error("Missing Parent ID. Record rejected.");
        }
        return false;
      }
      if (jsonNode.get("url").textValue() == null) {
        report.recordsRejected++;
        report.addIssue(Issue.MISSING_RECORD_URI, new Sample(input));
        stats.recordsRejected++;
        if (!suppressErrors) {
          error("Missing record URI. Record rejected.");
        }
        return false;
      }
      nsrTaxon = objectMapper.readValue(input, NsrTaxon.class);
    } catch (JsonProcessingException e) {
      report.recordsRejected++;
      report.addIssue(Issue.JSON_ERROR, new Sample(input));
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("JSON parsing error. Record rejected.");
      }
      return false;
    }
    return true;
  }

  @Override
  public List<Taxon> transform(String input) {
    this.input = input;
    report.recordsIngested++;
    if (!isValidJson(input)) {
      return null;
    }
    stats.recordsProcessed++;
    objectID = getObjectID();
    if (objectID == null) {
      report.addIssue(Issue.MISSING_ID, new Sample(input));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Missing object ID. Record rejected.");
      }
      return null;
    }
    if (skipRecord()) {
      report.addIssue(Issue.SKIPPED, new Sample(input));
      report.recordsSkipped++;
      stats.recordsSkipped++;
      return null;
    }
    return doTransform();
  }


  @Override
  protected List<Taxon> doTransform() {
    try {
      String rank = val(nsrTaxon.getRank());
      if (invalidRank(rank)) {
        return null;
      }
      Taxon taxon = new Taxon();
      taxon.setId(getElasticsearchId(NSR, objectID));

      if (!addScientificNames(taxon)) {
        // Without scientific name, we have no taxon document
        return null;
      }

      // For testing purposes only
      if (testGenera != null && !hasTestGenus(taxon)) {
        report.addIssue(Issue.SKIPPED, new Sample(input));
        report.recordsSkipped++;
        stats.recordsSkipped++;
        return null;
      }

      // From here onward, we have a valid taxon document, to which
      // we can now add more detailed information
      addSystemClassification(taxon);
      addDefaultClassification(taxon);
      taxon.setSourceSystem(NSR);
      taxon.setSourceSystemId(objectID);
      taxon.setTaxonRank(rank);
      String s = getOccurrenceStatusVerbatim(nsrTaxon.getStatus());
      taxon.setOccurrenceStatusVerbatim(s);
      taxon.setSourceSystemParentId(val(nsrTaxon.getNsr_id_parent()));
      setRecordURI(taxon);
      addVernacularNames(taxon);
      addDescriptions(taxon);
      stats.recordsAccepted++;
      stats.objectsProcessed++;
      stats.objectsAccepted++;
      report.recordsProcessed++;
      return Collections.singletonList(taxon);
    } catch (Throwable t) {
      stats.recordsRejected++;
      report.addIssue(Issue.SOFTWARE_ERROR, new Sample(input, t.getMessage()));
      if (!suppressErrors) {
        error("Record rejected! {}", t.getMessage());
      }
      return null;
    }
  }

  private boolean invalidRank(String rank) {
    if (rank == null) {
      report.addIssue(Issue.MISSING_RANK, new Sample(input));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected! Missing taxonomic rank");
      }
      return true;
    }
    if (!allowedTaxonRanks.contains(rank)) {
      report.addIssue(Issue.INVALID_RANK, new Sample(input));
      report.recordsRejected++;
      stats.recordsSkipped++;
      error("Record skipped. Ignoring higher taxon: \"%s\"", rank);
      return true;
    }
    return false;
  }

  /*
   * Add at least one full full scientific name to taxon document to be created.
   * When this fails, the creation of the taxon document fails.
   */
  private boolean addScientificNames(Taxon taxon) {

    // Collect name data from the source record
    Name[] names = nsrTaxon.getNames();
    if (names == null || names.length == 0) {
      stats.recordsRejected++;
      report.addIssue(Issue.MISSING_NAMES_ELEMENT, new Sample(input));
      if (!suppressErrors) {
        error("Record rejected! Missing <names> element under <taxon> element");
      }
      return false;
    }

    // Process each name element and ...
    for (Name name : names) {
      String nametype = val(name.getNametype());

      // 1. check if each <names> element contains a <nametype>, and
      if (nametype == null) {
        report.addIssue(Issue.MISSING_NAME_TYPE, new Sample(input, JsonUtil.toJson(name)));
        report.recordsRejected++;
        stats.recordsRejected++;
        if (!suppressErrors) {
          error("Record rejected! Missing <nametype> element under <name> element");
        }
        return false;
      }

      // 2. add the scientific name, when <nametype> is not vernacularName, but
      //    reject the record if that fails!
      if (!isVernacularName(nametype)) {
        if (!add(taxon, getScientificName(name))) {
          return false;
        }
      }
    }

    // Finally, check if the newly created taxon now has an Accepted Name
    // If not, the scientific name of the taxon is incomplete and
    // the record should fail
    if (taxon.getAcceptedName() == null) {
      report.recordsRejected++;
      report.addIssue(Issue.MISSING_ACCEPTED_NAME, new Sample(input));
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected! Missing accepted name for taxon");
      }
      return false;
    }
    return true;
  }

  private boolean add(Taxon taxon, ScientificName sn) {
    if (sn.getTaxonomicStatus() == ACCEPTED_NAME) {
      if (taxon.getAcceptedName() != null) {
        report.addIssue(Issue.MULTIPLE_ACCEPTED_NAMES, new Sample(input));
        report.recordsRejected++;
        stats.recordsRejected++;
        if (!suppressErrors) {
          error("Record rejected! Only one accepted name per taxon allowed");
        }
        return false;
      }
      taxon.setAcceptedName(sn);
    } else {
      taxon.addSynonym(sn);
    }
    return true;
  }

  /*
   * This method MUST be called after addScientificNames(), because it relies
   * on checks that are done in that method.
   */
  private void addVernacularNames(Taxon taxon) {
    Name[] names = nsrTaxon.getNames();
    if (names == null) {
      return;
    }
    for (Name name : names) {
      String nameType = name.getNametype();
      if (isVernacularName(nameType)) {
        taxon.addVernacularName(getVernacularName(name));
      }
    }
  }

  /*
   * Returns true when the nameType given belongs to a vernacular name;
   * false otherwise.
   */
  private static boolean isVernacularName(String nameType) {
    if (nameType == null) {
      return false;
    }
    return (nameType.equals("isPreferredNameOf") || nameType.equals("isAlternativeNameOf"));
  }

  private void setRecordURI(Taxon taxon) {
    String uri = val(nsrTaxon.getUrl());
    if (uri == null) {
      if (!suppressErrors) {
        warn("Missing URL for taxon with id \"%s\"", taxon.getSourceSystemId());
      }
    } else {
      try {
        taxon.setRecordURI(new URI(uri));
      } catch (URISyntaxException e) {
        if (!suppressErrors) {
          warn("Invalid URL: \"%s\"", uri);
        }
      }
    }
  }

  private void addDescriptions(Taxon taxon) {
    Description[] descriptions = nsrTaxon.getDescription();
    if (descriptions == null || descriptions.length == 0) {
      return;
    }
    for (Description description : descriptions) {
      TaxonDescription taxonDescription = new TaxonDescription();
      Author[] authors = description.getAuthors();
      if (authors != null && authors.length > 0) {
        List<String> authorStr = new ArrayList<>(authors.length);
        for (Author author : authors) {
          authorStr.add(author.getName());
        }
        taxonDescription.setAuthor(authorStr);
      }
      taxonDescription.setPublicationDate(description.getLast_change());
      taxonDescription.setCategory(val(description.getTitle()));
      taxonDescription.setDescription(val(description.getText()));
      taxonDescription.setLanguage(val(description.getLanguage()));
      taxonDescription.setLicense(License.CCBYSA);
      taxon.addDescription(taxonDescription);
    }
  }

  @SuppressWarnings("ConstantConditions")
  private void addSystemClassification(Taxon taxon) {

    Classification[] classifications = nsrTaxon.getClassification();
    if (classifications == null) {
      report.addIssue(Issue.MISSING_CLASSIFICATION, new Sample(input));
      if (!suppressErrors) {
        warn("No classification for taxon \"%s\"", nsrTaxon.getName());
      }
      return;
    }
    List<Monomial> monomials = new ArrayList<>(classifications.length);
    for (Classification classification : classifications) {
      String rank = val(classification.getRank());
      if (rank == null) {
        report.addIssue(Issue.EMPTY_RANK, new Sample(input));
        if (!suppressErrors) {
          warn("Empty <rank> element for \"%s\" (monomial discarded)", nsrTaxon.getName());
        }
        continue;
      }
      String epithet = val(classification.getName());
      if (epithet == null) {
        report.addIssue(Issue.EMPTY_NAME, new Sample(input));
        if (!suppressErrors) {
          warn("Empty <name> element for \"%s\" (monomial discarded)", nsrTaxon.getName());
        }
        continue;
      }
      monomials.add(new Monomial(rank, epithet));
      if (monomials.size() > 0) {
        taxon.setSystemClassification(monomials);
      }
    }
  }

  private boolean hasTestGenus(Taxon taxon) {
    String genus = taxon.getAcceptedName().getGenusOrMonomial();
    if (genus == null) {
      return false;
    }
    genus = genus.toLowerCase();
    for (String testGenus : testGenera) {
      if (genus.equals(testGenus)) {
        return true;
      }
    }
    return false;
  }

  private static String getOccurrenceStatusVerbatim(Status status) {
    /* Get content of status element within status element */
    return status == null ? null : val(status.getStatus());
  }

  /*
   * Does not set lower ranks, therefore does not cause discrepancies between
   * DefaultClassification and ScientificName.
   */
  private static void addDefaultClassification(Taxon taxon) {
    DefaultClassification dc = new DefaultClassification();
    List<Monomial> monomials = taxon.getSystemClassification();
    if (monomials != null) {
      for (Monomial m : monomials) {
        switch (m.getRank()) {
          case "regnum":
            dc.setKingdom(m.getName());
            break;
          case "phylum":
            dc.setPhylum(m.getName());
            break;
          case "classis":
            dc.setClassName(m.getName());
            break;
          case "ordo":
            dc.setOrder(m.getName());
            break;
          case "superfamilia":
            dc.setSuperFamily(m.getName());
            break;
          case "familia":
            dc.setFamily(m.getName());
            break;
          case "genus":
            dc.setGenus(m.getName());
            break;
          case "subgenus":
            dc.setSubgenus(m.getName());
            break;
        }
      }
    }
    taxon.setDefaultClassification(dc);
  }

  private ScientificName getScientificName(Name name) {
    ScientificName sn = new ScientificName();
    sn.setFullScientificName(val(name.getFullname()));
    sn.setAuthor(val(name.getName_author()));
    sn.setYear(val(name.getAuthorship_year()));
    sn.setAuthorshipVerbatim(val(name.getAuthorship()));
    sn.setGenusOrMonomial(val(name.getUninomial()));
    sn.setSpecificEpithet(val(name.getSpecific_epithet()));
    sn.setInfraspecificEpithet(val(name.getInfra_specific_epithet()));
    sn.setTaxonomicStatus(getTaxonomicStatus(name));
    Reference reference = getReference(name);
    if (reference != null) {
      sn.setReferences(Collections.singletonList(reference));
    }
    setScientificNameGroup(sn);
    return sn;
  }

  private VernacularName getVernacularName(Name name) {
    VernacularName vn = new VernacularName();
    vn.setLanguage(val(name.getLanguage()));
    vn.setName(val(name.getFullname()));
    String nameType = val(name.getNametype());
    vn.setPreferred(nameType.equals("isPreferredNameOf"));
    Reference reference = getReference(name);
    if (reference != null) {
      vn.setReferences(Collections.singletonList(reference));
    }
    return vn;
  }

  private Reference getReference(Name name) {
    String author = val(name.getReference_author());
    String title = val(name.getReference_title());
    if (author != null || title != null) {
      Reference ref = new Reference();
      ref.setTitleCitation(title);
      if (author != null) {
        ref.setAuthor(new Person(author));
      }
      OffsetDateTime refDate = getReferenceDate(name);
      ref.setPublicationDate(refDate);
      return ref;
    }
    return null;
  }

  private OffsetDateTime getReferenceDate(Name name) {
    String date = val(name.getReference_date());
    if (date == null) {
      return null;
    }
    if (date.toLowerCase().startsWith("in prep")) {
      report.addIssue(Issue.INVALID_REFERENCE_DATE, new Sample(date));
      if (logger.isDebugEnabled()) {
        logger.debug("Invalid input for <reference_date>: \"{}\"", date);
      }
      return null;
    }
    OffsetDateTime odt = new ESDateInput(date).parseAsYear();
    if (odt == null) {
      report.addIssue(Issue.INVALID_REFERENCE_DATE, new Sample(date));
      if (!suppressErrors) {
        warn("Invalid input for <reference_date>:", date);
      }
    }
    return odt;
  }

  private TaxonomicStatus getTaxonomicStatus(Name name) {
    String nameType = val(name.getNametype());
    if (nameType == null) {
      report.addIssue(Issue.MISSING_NAME_TYPE, new Sample(input));
      report.recordsRejected++;
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected! Missing or empty <nametype> for name: " + val(name.getFullname()));
      }
      return null;
    }
    TaxonomicStatus status = translations.get(nameType);
    if (status == null) {
      report.addIssue(Issue.INVALID_TAXON_STATUS, new Sample(input));
      stats.recordsRejected++;
      if (!suppressErrors) {
        error("Record rejected! Invalid taxonomic status: " + nameType);
      }
      return null;
    }
    return status;
  }
}
