package nl.naturalis.nba.etl.report;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.File;
import nl.naturalis.nba.common.json.JsonUtil;

/**
 * A Java Bean representing an error that can occur in the ETL Process.
 * It can be used to include processing errors in a Report.
 *
 * @author Tom Gilissen
 */
public class FileProcessingErrorStopUsing {

  private final ProcessingError type;
  private final String fileName;
  @JsonInclude(Include.NON_NULL)
  private String detail;
  @JsonInclude(Include.NON_NULL)
  private Long lineNumber;

  /**
   * Initialise a FileProcessingError with the type of the error and
   * the file being processed.
   *
   * @param type  error type
   * @param file  file that causes the problem
   */
  public FileProcessingErrorStopUsing(ProcessingError type, File file) {
    this.type = type;
    this.fileName = file.getName();
  }

  /**
   * Initialise a FileProcessingError with file and detail of the
   * error that occurred.
   *
   * @param type  type of error
   * @param file  file that causes the problem
   * @param detail  the error message or any other useful comment for the report
   */
  public FileProcessingErrorStopUsing(ProcessingError type, File file, String detail) {
    this.type = type;
    this.fileName = file.getName();
    this.detail = detail;
  }

  /**
   * Initialise a FileProcessingError with file, line number and
   * detail of the error that occurred.
   *
   * @param  type  the error type
   * @param  file  file that causes the problem
   * @param  detail  the error message
   * @param  lineNumber  the line number on which the problem occurred
   */
  public FileProcessingErrorStopUsing(ProcessingError type, File file, Long lineNumber, String detail) {
    this(type, file, detail);
    this.lineNumber = lineNumber;
  }

  public String getFileName() {
    return fileName;
  }

  public String getDetail() {
    return detail;
  }

  public Long getLineNumber() {
    return lineNumber;
  }

  @Override
  public String toString() {
    return JsonUtil.toJson(this);
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + type.getDescription().hashCode();
    result = 31 * result + fileName.hashCode();
    if (detail != null) {
      result = 31 * result + detail.hashCode();
    }
    if (lineNumber != null && lineNumber > 0) {
      int n = lineNumber.intValue();
      result = 31 * n;
    }
    return result;
  }

}
