package nl.naturalis.nba.etl.crs;

import static nl.naturalis.nba.dao.DocumentType.SPECIMEN;

import java.io.IOException;
import nl.naturalis.nba.api.model.Specimen;
import nl.naturalis.nba.etl.ETLStatistics;
import nl.naturalis.nba.etl.JsonNDWriter;
import nl.naturalis.nba.etl.report.Report;

/**
 * The JsonNDWriter component in the ETL cycle for CRS specimen objects.
 */
public class CrsSpecimenJsonNDWriter extends JsonNDWriter<Specimen> {

  public CrsSpecimenJsonNDWriter(String sourceFile, Report report, ETLStatistics stats) throws IOException {
    super(SPECIMEN, "CRS", sourceFile, report, stats);
  }
}
