package nl.naturalis.nba.api;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import nl.naturalis.nba.api.model.IDocumentObject;

/**
 * Specifies a set of common data access methods that can be called against any type of document
 * within the NBA document store.
 *
 * @author Ayco Holleman
 * @param <DOCUMENT_OBJECT> The class representing the Elasticsearch document that you are given
 *     access to.
 */
@SuppressWarnings("CheckStyle")
public interface INbaAccess<DOCUMENT_OBJECT extends IDocumentObject> {

  /**
   * Returns the document with the specified document ID, or {@code null} if there is no document
   * with the specified document ID. Note that the document ID is not part of the document itself.
   * It corresponds with the Elasticsearch {@code _id} field, which is retrieved separately from the
   * document source. You can get the value of this field through {@link IDocumentObject#getId()
   * IDocumentObject.getId}.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET request with the following end point:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/find/{id}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/specimen/find/ZMA.MAM.123456@CRS
   * </code>
   *
   * @see IDocumentObject
   * @param id The NBA system ID of the data model object
   * @return the document object
   */
  DOCUMENT_OBJECT find(String id);

  /**
   * Returns the data model objects with the specified system IDs, or a zero-length array no
   * specimens were found. Note that you cannot look up more than 1024 IDs at a time.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET request with the following end point:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/findByIds/{id-0},{id-1},{id-2},{id-n}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/specimen/findByIds/ZMA.MAM.123,ZMA.MAM.456,ZMA.MAM.789
   * </code>
   *
   * @param ids The NBA system IDs of the requested data model objects
   * @return the document object
   * @throws InvalidQueryException
   */
  DOCUMENT_OBJECT[] findByIds(String[] ids) throws InvalidQueryException;

  /**
   * Returns documents satisfying the provided query.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET and a POST request with the following
   * endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/query
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param querySpec the query spec
   * @return the query result
   * @throws InvalidQueryException when querySpec is invalid
   */
  QueryResult<DOCUMENT_OBJECT> query(QuerySpec querySpec) throws InvalidQueryException;

  /**
   * Returns documents satisfying the provided query. This method is meant to be the first call for
   * a batch-wise retrieval operation. The query result will contain a resumption token to be used
   * for subsequent calls (see {@link #batchQuery(String)}). The resumption token is already
   * URL-encoded so can be used straight away as the value for the {@code _token} query parameter.
   * Make sure it doesn't get accidentally URL-encoded again.
   *
   * <p>Unlike the {@link #query(QuerySpec) query} method, this method is meant to retrieve large
   * datasets in batches. You could do the same with the {@code query} method by repeatedly
   * increasing the {@link QuerySpec#getFrom() from} field in the {@code QuerySpec}, but this
   * gradually becomes very expensive and you will eventually hit a brick wall beyond which the NBA
   * which won't let you go.
   *
   * <p>With the {@code batchQuery} service the {@code from} and {@code sort} fields of the {@code
   * QuerySpec} are ignored. Sorting is done in an implementation-specific way. Also, for perfomance
   * reasons, no document total is given in the query result. The presence of a resumption token
   * means there are more records to retrieve. You are free to specify the batch size using {@code
   * size} field of the {@code QuerySpec}, but there is a hard limit of 5000 documents per batch.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET and a POST request with the following
   * endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/batchQuery
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param querySpec the query spec
   * @return the query result
   * @throws InvalidQueryException when the querySpec is invalid
   */
  BatchQueryResult<DOCUMENT_OBJECT> batchQuery(QuerySpec querySpec) throws InvalidQueryException;

  /**
   * Retrieves the next batch of documents using the specified token. The token should be obtained
   * from the {@code BatchQueryResult} returned by the {@link #batchQuery(QuerySpec) initial query}
   * or by a previous call to <i>this</i> method.
   *
   * @param token the resumptionToken
   * @return the query result
   * @throws InvalidQueryException when an invalid query has been defined
   */
  BatchQueryResult<DOCUMENT_OBJECT> batchQuery(String token) throws InvalidQueryException;

  /**
   * Writes the output satisfying the specified query specification to the specified output stream.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET and POST request with the following end
   * point:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/download/{querySpec}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/specimen/download/?identifications.scientificName.fullScientificName=Limosa%20limosa
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param querySpec the querySpec
   * @param out the output stream used
   * @throws InvalidQueryException when an invalid query is being used
   * @throws IOException when creating the output stream fails
   */
  void downloadQuery(QuerySpec querySpec, OutputStream out)
      throws InvalidQueryException, IOException;

  /**
   * Returns the number of documents conforming to the provided query specification. You may specify
   * {@code null} for the {@code querySpec} argument if you simply want a total document count.
   * Otherwise you should only set the query conditions and (possibly) the {@link LogicalOperator
   * logical operator} on the {@code QuerySpec}. Setting anything else on the {@code QuerySpec} has
   * no effect.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET or POST request with the following
   * endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/count
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/taxon/count<br>
   * https://api.biodiversitydata.nl/v2/specimen/count/?sourceSystem.code=BRAHMS
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param querySpec the query spec
   * @return the number of documents
   * @throws InvalidQueryException when the querySpec is invalid
   */
  long count(QuerySpec querySpec) throws InvalidQueryException;

  /**
   * Returns the distinct number of values for the given field. You may specify a {@code querySpec}
   * argument if you're interested in just the distinct values of a (limited) set of documents. You
   * may specify {@code null} for the {@code querySpec} argument if you simply want a total document
   * count. Otherwise you should only set the query conditions and (possibly) the {@link
   * LogicalOperator logical operator} on the {@code QuerySpec}. Setting anything else on the {@code
   * QuerySpec} has no effect.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET or POST request with the following
   * endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/countDistinctValues/{forField}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/taxon/countDistinctValues/defaultClassification.genus<br>
   * https://api.biodiversitydata.nl/v2/specimen/countDistinctValues/collectionType/?sourceSystem.code=CRS
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param field the field
   * @param querySpec the query spec
   * @return the number of distinct values
   * @throws InvalidQueryException when the querySpec is invalid
   */
  long countDistinctValues(String field, QuerySpec querySpec) throws InvalidQueryException;

  /**
   * Returns the distinct number of values for the given field (<i>forField</i>), grouped by the
   * second field (<i>forGroup</i>) you have specified. The result is a {@link List} in which each
   * item consists of 2 {@link Map}s: the first containing the group name (key) and the distinct
   * group value (value); the second containing the field name (key) and the number of distinct
   * values for this field (value), within this group.
   *
   * <pre>
   * [
   *   {"&lt;<i>forGroup</i>&gt;":"&lt;<i>distinct value a</i>&gt;","&lt;<i>forField</i>&gt;":&lt;<i>distinct count 1</i>&gt;},
   *   {"&lt;<i>forGroup</i>&gt;":"&lt;<i>distinct value b</i>&gt;","&lt;<i>forField</i>&gt;":&lt;<i>distinct count 2</i>&gt;},
   *   [...]
   *   {"&lt;<i>forGroup</i>&gt;":"&lt;<i>distinct value z</i>&gt;","&lt;<i>forField</i>&gt;":&lt;<i>distinct count n</i>&gt;}
   * ]
   * </pre>
   *
   * <p>You may specify a {@code querySpec} argument if you're interested in just the distinct
   * values of a specific set of documents. If you don't include a {@code querySpec} argument, you
   * will get a summary of all documents.
   *
   * <p>By default, the result will be sorted descending by the distinct field value count. You may
   * choose to change the sorting by including the field you're using to group the results, as the
   * sort field in the {@code querySpec}:
   *
   * <pre>
   * "sortFields" : [ { "path" : "[<i>forGroup</i>]", "sortOrder" : "ASC|DESC" } ]
   * </pre>
   *
   * Default result size is 10. You can override this by including the desired result size in the
   * {@code querySpec}:
   *
   * <pre>
   * "size" : <i>n</i>
   * </pre>
   *
   * If you have a large result set, you may choose to combine {@code size} with {@code from}. This
   * will allow you to page through the results, by choosing the start ({@code from}) and the end
   * ({@code size}) of the results you'd like to see. Include {@code from} and {@code size} in the
   * {@code querySpec}:
   *
   * <pre>
   * "from" : <i>n</i>,
   * "size" : <i>m</i>
   * </pre>
   *
   * <p>NOTE: limiting the result view like this, will not reduce the response time. At least, it
   * does at the top end of the result list, but not at the bottom end. A request for the final
   * results of the set, will take approximately as long as a request for the entire result set. The
   * gain you do have, is the recuced amount of data that will be returned by your request.
   *
   * <p>Furthermore, the maximum resultsize is 10 000. If you try to go past that, for instance by
   * using {@code from} and {@code size}, a {@code InvalidQueryException} will be thrown.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET or POST request with the following
   * endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/countDistinctValuesPerGroup/{forGroup}/{forField}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/taxon/countDistinctValuesPerGroup/[<i>field1</i>]/[<i>field2</i>]<br>
   * https://api.biodiversitydata.nl/v2/specimen/countDistinctValuesPerGroup/collectionType/sourceSystem.code
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param forGroup the field used to group
   * @param forField the field to get distinct values from
   * @param querySpec the query spec
   * @return a List of Maps containing the group(s) and the distinct values plus count
   * @throws InvalidQueryException when the querySpec is invalid
   */
  List<Map<String, Object>> countDistinctValuesPerGroup(
      String forGroup, String forField, QuerySpec querySpec) throws InvalidQueryException;

  /**
   * Returns the unique values of the specified field. The result is returned as a {@link Map} with
   * each key specifying one of the unique values and its value the document count (the number of
   * documents for which the specified field has that value).
   *
   * <p>
   *
   * <p>You may specify {@code null} for the {@code querySpec} argument if you simply want a total
   * document count. Otherwise you should only set the query conditions and (possibly) the {@link
   * LogicalOperator logical operator} on the {@code QuerySpec}.
   *
   * <p>By default, the result will be sorted descending by document count. You can choose to sort
   * the result by the value of the field name by including that as sort field in the {@code
   * querySpec}:
   *
   * <pre>
   * "sortFields" : [ { "path" : "[<i>forField</i>]", "sortOrder" : "ASC|DESC" } ]
   * </pre>
   *
   * <p>Note that if the specified field is a {@link Collection} or an array, the sum of the
   * document counts may add up to more than the total number of documents in the index.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET request with the following endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/getDistinctValues/{forField}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/specimen/getDistinctValues/recordBasis<br>
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param forField the field used to collect the distinct values from
   * @param querySpec an optional query spec
   * @return the distinct values and their count
   * @throws InvalidQueryException when the querySpec is invalid
   */
  Map<String, Long> getDistinctValues(String forField, QuerySpec querySpec)
      throws InvalidQueryException;

  /**
   * Returns the distinct values and their document count of the specified field (<i>forField</i>),
   * grouped by a second specified field (<i>forGroup</i>). The result is returned as a {@link List}
   * of {@link Map}s. Each {@code List} item contains 3 {@code Map}s. The first map has as key the
   * name of the field used to group the result, and its value as value. The second map has as key
   * the label "count", and the document count as value. The last map of the {@code List} has as key
   * the label "values", and as value a {@code List} containing the distinct values and their
   * document counts. The structure of the latter {@code List} is similar to that of the {@code
   * List} returned by a {@code countDistinctValuesPerGroup()}: each list item consists of 2 {@code
   * Map}s: the first, having the <i>forField</i> as key, and a distinct value as its value; the
   * second, the label "count" as key, and the document count as value.
   *
   * <pre>
   * [
   *  {
   *   "&lt;<i>forGroup</i>&gt;":"&lt;<i>distinct group value A</i>&gt;",
   *   "count": &lt;<i>distinct group count 1</i>&gt;,
   *   "values": [
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value a</i>&gt;",
   *        "count": &lt;<i>distinct count 1</i>&gt;
   *      },
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value b</i>&gt;",
   *        "count": &lt;<i>distinct count 2</i>&gt;
   *      },
   *      [...]
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value z</i>&gt;",
   *        "count": &lt;<i>distinct count n</i>&gt;
   *      },
   *   ],
   *   "&lt;<i>forGroup</i>&gt;":"&lt;<i>distinct group value B</i>&gt;",
   *   "count": &lt;<i>distinct group count 2</i>&gt;,
   *   "values": [
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value a</i>&gt;",
   *        "count": &lt;<i>distinct count 1</i>&gt;
   *      },
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value b</i>&gt;",
   *        "count": &lt;<i>distinct count 2</i>&gt;
   *      },
   *      [...]
   *      {
   *        "&lt;<i>forField</i>&gt;":"&lt;<i>distinct value z</i>&gt;",
   *        "count": &lt;<i>distinct count n</i>&gt;
   *      }
   *   ],
   *   [...]
   * }
   * </pre>
   *
   * <p>NOTE: you may think that the sum of all counts of the distinct field values, will add up to
   * the count of the distinct field value used to group the results. This is not true though! At
   * least, not necessarily. The count of both summaries is independent of each other. When each
   * record exactly one occurence of both fields (<i>forField</i> and <i>forGroup</i>), then the
   * counts will add up . If the number of occurences of the specified fields differ, then the
   * counts are unrelated.
   *
   * <p>You may specify a {@code querySpec} argument if you're interested in just the distinct
   * values of a specific set of documents. If you don't include a {@code querySpec} argument, you
   * will get a summary of all documents.
   *
   * <p>By default, the result will be sorted descending by the distinct field value count. You can
   * choose to change the sorting by including the <i>forField</i> and/or the <i>forGroup</i> as
   * sort field(s) in the {@code querySpec}:
   *
   * <pre>
   * "sortFields" : [ { "path" : "[<i>forGroup</i>]", "sortOrder" : "ASC|DESC" } ]
   * </pre>
   *
   * <p>The default result size is 10. You can override this by including the desired result size in
   * the {@code querySpec}:
   *
   * <pre>
   * "size" : <i>n</i>
   * </pre>
   *
   * If you have a large result set, you may choose to combine {@code size} with {@code from}. This
   * will allow you to page through the results, by choosing the start ({@code from}) and the end
   * ({@code size}) of the results you'd like to see. Include {@code from} and {@code size} in the
   * {@code querySpec}:
   *
   * <pre>
   * "from" : <i>n</i>,
   * "size" : <i>m</i>
   * </pre>
   *
   * <p>NOTE: limiting the result view like this, will not reduce the response time. At least, it
   * does at the top end of the result list, but not at the bottom end. A request for the final
   * results of the set, will take approximately as long as a request for the entire result set. The
   * gain you do have, is the recuced amount of data that will be returned by your request.
   *
   * <p>Furthermore, the maximum resultsize is 10 000. If you try to go past that, for instance by
   * using {@code from} and {@code size}, a {@code InvalidQueryException} will be thrown.
   *
   * <h5>REST API</h5>
   *
   * <p>The NBA REST API exposes this method through a GET request with the following endpoint:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/&lt;document-type&gt;/getDistinctValuesPerGroup/{forGroup}/{forField}
   * </code>
   *
   * <p>For example:
   *
   * <p><code>
   * https://api.biodiversitydata.nl/v2/specimen/getDistinctValuesPerGroup/sourceSystem.code/recordBasis<br>
   * </code>
   *
   * <p>See {@link QuerySpec} for an explanation of how to encode the {@code QuerySpec} object in
   * the request.
   *
   * @param forGroup the field used to group by
   * @param forField the field used to collect the distinct values
   * @param querySpec an (optional) query spec
   * @return a list containing all distinct values and their count grouped by the specified field
   * @throws InvalidQueryException when the querySpec is invalid
   */
  List<Map<String, Object>> getDistinctValuesPerGroup(
      String forGroup, String forField, QuerySpec querySpec) throws InvalidQueryException;
}
