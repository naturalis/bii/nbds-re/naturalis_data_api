package nl.naturalis.nba.api.model;

import static nl.naturalis.nba.api.annotations.Analyzer.CASE_INSENSITIVE;
import static nl.naturalis.nba.api.annotations.Analyzer.DEFAULT;
import static nl.naturalis.nba.api.annotations.Analyzer.CONTAINS;

import java.util.List;
import java.util.Objects;
import nl.naturalis.nba.api.annotations.Analyzers;

/**
 * Encapsulates a taxon's full scientific name and the components it is composed of. Only the full scientific name will always be set.
 * Individual name components will only be set if they were provided separately by the source system.
 *
 * @author Ayco Holleman
 */
public class ScientificName implements INbaModelObject {

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String fullScientificName;

  private TaxonomicStatus taxonomicStatus;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String genusOrMonomial;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String subgenus;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String specificEpithet;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String infraspecificEpithet;

  private String infraspecificMarker;
  private String nameAddendum;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String authorshipVerbatim;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String author;

  private String year;

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String scientificNameGroup;

  private List<Reference> references;
  private List<Expert> experts;

  public String getFullScientificName() {
    return fullScientificName;
  }

  public void setFullScientificName(String fullScientificName) {
    this.fullScientificName = fullScientificName;
  }

  public TaxonomicStatus getTaxonomicStatus() {
    return taxonomicStatus;
  }

  public void setTaxonomicStatus(TaxonomicStatus taxonomicStatus) {
    this.taxonomicStatus = taxonomicStatus;
  }

  public String getGenusOrMonomial() {
    return genusOrMonomial;
  }

  public void setGenusOrMonomial(String genusOrMonomial) {
    this.genusOrMonomial = genusOrMonomial;
  }

  public String getSubgenus() {
    return subgenus;
  }

  public void setSubgenus(String subgenus) {
    this.subgenus = subgenus;
  }

  public String getSpecificEpithet() {
    return specificEpithet;
  }

  public void setSpecificEpithet(String specificEpithet) {
    this.specificEpithet = specificEpithet;
  }

  public String getInfraspecificEpithet() {
    return infraspecificEpithet;
  }

  public void setInfraspecificEpithet(String infraspecificEpithet) {
    this.infraspecificEpithet = infraspecificEpithet;
  }

  public String getInfraspecificMarker() {
    return infraspecificMarker;
  }

  public void setInfraspecificMarker(String infraspecificMarker) {
    this.infraspecificMarker = infraspecificMarker;
  }

  public String getNameAddendum() {
    return nameAddendum;
  }

  public void setNameAddendum(String nameAddendum) {
    this.nameAddendum = nameAddendum;
  }

  public String getAuthorshipVerbatim() {
    return authorshipVerbatim;
  }

  public void setAuthorshipVerbatim(String authorshipVerbatim) {
    this.authorshipVerbatim = authorshipVerbatim;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  /**
   * Returns the concatenation (space-separated and in lower case) of the scientific name's genus, specific epithet and infraspecific
   * epithet.
   *
   * @return
   */
  public String getScientificNameGroup() {
    return scientificNameGroup;
  }

  public void setScientificNameGroup(String scientificNameGroup) {
    this.scientificNameGroup = scientificNameGroup;
  }

  public List<Reference> getReferences() {
    return references;
  }

  public void setReferences(List<Reference> references) {
    this.references = references;
  }

  public List<Expert> getExperts() {
    return experts;
  }

  public void setExperts(List<Expert> experts) {
    this.experts = experts;
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        author,
        authorshipVerbatim,
        experts,
        fullScientificName,
        genusOrMonomial,
        infraspecificEpithet,
        infraspecificMarker,
        nameAddendum,
        references,
        scientificNameGroup,
        specificEpithet,
        subgenus,
        taxonomicStatus,
        year);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ScientificName other = (ScientificName) obj;
    return Objects.equals(author, other.author)
        && Objects.equals(authorshipVerbatim, other.authorshipVerbatim)
        && Objects.equals(experts, other.experts)
        && Objects.equals(fullScientificName, other.fullScientificName)
        && Objects.equals(genusOrMonomial, other.genusOrMonomial)
        && Objects.equals(infraspecificEpithet, other.infraspecificEpithet)
        && Objects.equals(infraspecificMarker, other.infraspecificMarker)
        && Objects.equals(nameAddendum, other.nameAddendum)
        && Objects.equals(references, other.references)
        && Objects.equals(scientificNameGroup, other.scientificNameGroup)
        && Objects.equals(specificEpithet, other.specificEpithet)
        && Objects.equals(subgenus, other.subgenus)
        && taxonomicStatus == other.taxonomicStatus
        && Objects.equals(year, other.year);
  }
}
