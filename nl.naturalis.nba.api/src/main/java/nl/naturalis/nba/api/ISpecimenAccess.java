package nl.naturalis.nba.api;

import java.io.OutputStream;
import nl.naturalis.nba.api.model.Specimen;

/**
 * Specifies methods for accessing specimen-related data.
 *
 * @author Ayco Holleman
 *
 */
public interface ISpecimenAccess extends INbaAccess<Specimen> {

  /**
   * <p>
   * Retrieves a {@link Specimen} by its UnitID. Since the UnitID is not strictly
   * specified to be unique across all of the NBA's data sources, a theoretical
   * chance exists that multiple specimens are retrieved for a given UnitID.
   * Therefore this method returns an array of specimens. If no specimen with the
   * specified UnitID exists, a zero-length array is returned.
   * </p>
   * <h5>REST API</h5>
   * <p>
   * The NBA REST API exposes this method through a GET request with the following
   * end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/findByUnitID/{unitID}
   * </code>
   * </p>
   * <p>
   * For example:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/findByUnitID/ZMA.MAM.123456
   * </code>
   * </p>
   *
   * @param unitID The UnitID of the specimen occurence
   * @return
   */
  Specimen[] findByUnitID(String unitID);

  /**
   * <p>
   * Returns whether or not the specified string is a valid UnitID (i&#46;e&#46;
   * is the UnitID of at least one specimen record).
   * </p>
   * <h5>REST API</h5>
   * <p>
   * The NBA REST API exposes this method through a GET request with the following
   * end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/exists/{unitID}
   * </code>
   * </p>
   * <p>
   * For example:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/exists/ZMA.MAM.123456
   * </code>
   * </p>
   *
   * @param unitID
   * @return
   */
  boolean exists(String unitID);

  /**
   * <p>
   * Writes a DarwinCore Archive with taxa satisfying the specified query
   * specification to the specified output stream.
   * </p>
   * <h5>REST API</h5>
   * <p>
   * The NBA REST API exposes this method through a GET and POST request with the
   * following end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/dwca/query
   * </code>
   * </p>
   * <p>
   * See {@link QuerySpec} for an explanation of how to encode the
   * {@code QuerySpec} object in the request.
   * </p>
   *
   * @param querySpec
   * @param out
   * @throws InvalidQueryException
   */
  void dwcaQuery(QuerySpec querySpec, OutputStream out) throws InvalidQueryException;

  /**
   * <p>
   * Writes a DarwinCore Archive with specimens from a predefined data set to the
   * specified output stream. To get the names of all currently defined data sets,
   * call {@link #dwcaGetDataSetNames() dwcaGetDataSetNames}.
   * </p>
   * <p>
   * The NBA REST API exposes this method through a GET request with the following
   * end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/dwca/getDataSet/{name}
   * </code>
   * </p>
   * <p>
   * For example:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/dwca/getDataSet/hymenoptera
   * </code>
   * </p>
   *
   * @param name The name of the predefined data set
   * @param out The output stream to write to
   * @throws InvalidQueryException
   */
  void dwcaGetDataSet(String name, OutputStream out) throws NoSuchDataSetException;

  /**
   * <p>
   * Returns the names of all predefined data sets with specimen data.
   * </p>
   * <h5>REST API</h5>
   * <p>
   * The NBA REST API exposes this method through a GET request with the following
   * end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/dwca/getDataSetNames
   * </code>
   * </p>
   *
   * @return
   */
  String[] dwcaGetDataSetNames();

  /**
   * Searches for specimens while providing extra options to resolve a search
   * phrase to a scientific name. This is meant to be implemented as a two-phase
   * query. In the first phase the {@link NameResolutionRequest} within the
   * provided {@link NameResolutionQuerySpec} is used to generate a query against
   * a taxon database. This can be either the NBA's own taxon index or it can be
   * ColPlus. The scientific names coming back from that query are then used to
   * construct an extra query condition for the specimen index. In the second
   * phase that query condition is added to the other query conditions of the
   * provided <code>querySpec</code>. The enriched query is executed and the
   * result is returned to the client.
   *
   * @param querySpec
   * @return
   * @throws InvalidQueryException
   */
  QueryResult<Specimen> queryWithNameResolution(NameResolutionQuerySpec querySpec)
      throws InvalidQueryException;

  /**
   * <p>
   * Groups specimens by their scientific name. Although this method will
   * optionally also retrieve the taxa associated with a scientific name, any
   * query conditions and sort fields specified through the {@link QuerySpec} must
   * reference {@link Specimen} fields only.
   * </p>
   * <h5>REST API</h5>
   * <p>
   * The NBA REST API exposes this method through a GET request with the following
   * end point:
   * </p>
   * <p>
   * <code>
   * https://api.biodiversitydata.nl/v2/specimen/groupByScientificName
   * </code>
   * </p>
   *
   * @param querySpec
   * @return
   * @throws InvalidQueryException
   */
  GroupByScientificNameQueryResult groupByScientificName(GroupByScientificNameQuerySpec querySpec)
      throws InvalidQueryException;
}
