package nl.naturalis.nba.api;

/**
 * Extension of {@link QuerySpec} used for the {@link ISpecimenAccess#queryWithNameResolution(NameResolutionQuerySpec)
 * queryWithNameResolution} service.
 *
 * @author Ayco Holleman
 *
 */
public class NameResolutionQuerySpec extends QuerySpec {

  private NameResolutionRequest nameResolutionRequest;

  public NameResolutionRequest getNameResolutionRequest() {
    return nameResolutionRequest;
  }

  public void setNameResolutionRequest(NameResolutionRequest nameResolutionRequest) {
    this.nameResolutionRequest = nameResolutionRequest;
  }
}
