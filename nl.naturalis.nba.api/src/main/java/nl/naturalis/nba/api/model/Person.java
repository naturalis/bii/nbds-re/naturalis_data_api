package nl.naturalis.nba.api.model;

import static nl.naturalis.nba.api.annotations.Analyzer.CASE_INSENSITIVE;
import static nl.naturalis.nba.api.annotations.Analyzer.CONTAINS;
import static nl.naturalis.nba.api.annotations.Analyzer.DEFAULT;

import java.util.Objects;
import nl.naturalis.nba.api.annotations.Analyzers;

public class Person extends Agent {

  @Analyzers({CASE_INSENSITIVE, DEFAULT, CONTAINS})
  private String fullName;

  private Organization organization;

  public Person() {}

  public Person(String fullName) {
    this.fullName = fullName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  @Override
  public String toString() {
    return "{fullName: " + quote(fullName) + ", organization: " + organization + "}";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(fullName, organization);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Person other = (Person) obj;
    return Objects.equals(fullName, other.fullName)
        && Objects.equals(organization, other.organization);
  }

  private static String quote(Object obj) {
    return obj == null ? "null" : '"' + String.valueOf(obj) + '"';
  }
}
