package nl.naturalis.nba.api;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import nl.naturalis.nba.api.model.IDocumentObject;

/**
 * A {@code QueryResult} represents the result from a {@link QuerySpec search
 * request}.
 *
 * @author Ayco Holleman
 *
 * @param <T> The type of object returned by the search request. This can be a
 *        plain, unmodified Elasticsearch document (i.e. an implementation of
 *        {@link IDocumentObject}), but that is required by this class.
 */
public class QueryResult<T> implements Iterable<QueryResultItem<T>> {

  protected List<QueryResultItem<T>> resultSet;
  protected Long totalSize;

  public QueryResult() {
    this.resultSet = Collections.emptyList();
  }

  /**
   * Returns the number of documents in this {@code QueryResult}.
   *
   * @return
   */
  public int size() {
    return resultSet.size();
  }

  /**
   * Returns the document with the specified index.
   *
   * @param index
   * @return
   */
  public QueryResultItem<T> get(int index) {
    return resultSet.get(index);
  }

  /**
   * Returns the total number of documents conforming to the {@link QuerySpec
   * query specification} that produced this query result.
   *
   * @return
   */
  public Long getTotalSize() {
    return totalSize == null ? 0L : totalSize;
  }

  /**
   * Sets the total number of documents conforming to the {@link QuerySpec query
   * specification} that produced this query result. Not meant to be called by
   * clients.
   *
   * @param totalSize
   */
  public void setTotalSize(Long totalSize) {
    this.totalSize = totalSize;
  }

  /**
   * Sets the result set of this {@code QueryResult}. Not meant to be called by
   * clients.
   *
   * @param items
   */
  public void setResultSet(List<QueryResultItem<T>> items) {
    this.resultSet = items;
  }

  /**
   * Returns a {@code List} of result set items, each one contain one document.
   *
   * @return
   */
  public List<QueryResultItem<T>> getResultSet() {
    return resultSet;
  }

  /**
   * Returns an {@code Iterator} over the result set items.
   */
  @Override
  public Iterator<QueryResultItem<T>> iterator() {
    return resultSet.iterator();
  }

  /**
   * Returns a {@code Stream} of result set items.
   *
   * @return
   */
  public Stream<QueryResultItem<T>> stream() {
    return resultSet.stream();
  }
}
