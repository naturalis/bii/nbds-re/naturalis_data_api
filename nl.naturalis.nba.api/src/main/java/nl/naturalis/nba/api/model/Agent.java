package nl.naturalis.nba.api.model;

import java.util.Objects;

public class Agent implements INbaModelObject {

  private String agentText;

  public Agent() {}

  public Agent(String agentText) {
    this.agentText = agentText;
  }

  public String getAgentText() {
    return agentText;
  }

  public void setAgentText(String agentText) {
    this.agentText = agentText;
  }

  @Override
  public int hashCode() {
    return Objects.hash(agentText);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Agent other = (Agent) obj;
    return Objects.equals(agentText, other.agentText);
  }
}
