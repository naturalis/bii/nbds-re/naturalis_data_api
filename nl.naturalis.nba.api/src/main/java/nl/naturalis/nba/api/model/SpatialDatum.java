package nl.naturalis.nba.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum representing the reference system upon which the geographic coordinates
 * given in latitudeDecimal and longituDedecimal as based.
 *
 * {@see ABCD: <a href="https://terms.tdwg.org/wiki/abcd2:Gathering-SpatialDatum">
 *   https://terms.tdwg.org/wiki/abcd2:Gathering-SpatialDatum</a>}, or
 * {@see DWC: <a href="https://terms.tdwg.org/wiki/dwc:geodeticDatum">
 *   https://terms.tdwg.org/wiki/dwc:geodeticDatum</a>}
 *
 */
public enum SpatialDatum implements INbaModelObject {
  WGS84,
  NAD83,
  NAD27;

  private final String name = name();

  /**
   * Parse the string give to a SpatialDatum enum.
   *
   * @param name  the spatial datum as string
   * @return the matching SpatialDatum
   */
  @JsonFormat(shape = Shape.OBJECT)
  public static SpatialDatum parse(@JsonProperty("name") String name) {
    if (name == null) {
      return null;
    }
    for (SpatialDatum spatialDatum : SpatialDatum.values()) {
      if (spatialDatum.name.equals(name)) {
        return spatialDatum;
      }
    }
    throw new IllegalArgumentException("Invalid spatial datum: " + name);
  }

  @JsonValue
  @Override
  public String toString() {
    return name;
  }
}
