package nl.naturalis.nba.api;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@SuppressWarnings("unused")
@JsonPropertyOrder({"token", "resultSet"})
public class BatchQueryResult<T> implements Iterable<QueryResultItem<T>> {

  private String token;
  private List<QueryResultItem<T>> resultSet;

  public BatchQueryResult() {
    this.resultSet = Collections.emptyList();
  }

  public BatchQueryResult(List<QueryResultItem<T>> resultSet) {
    this.resultSet = resultSet;
  }

  public BatchQueryResult(QueryResult<T> result) {
    this.resultSet = result.getResultSet();
  }

  /**
   * Returns the token to be used for retrieval of the next batch.
   *
   * @return the token
   */
  public String getToken() {
    return token;
  }

  /**
   * Sets the token to be used for retrieval of the next batch.
   */
  public void setToken(String token) {
    this.token = token;
  }

  /**
   * Sets the result set of this {@code QueryResult}. clients.
   *
   * @param items  a list containing the query result items
   */
  public void setResultSet(List<QueryResultItem<T>> items) {
    this.resultSet = items;
  }

  /**
   * Returns a {@code List} of result set items, each one contain one document.
   *
   * @return the result set
   */
  public List<QueryResultItem<T>> getResultSet() {
    return resultSet;
  }

  /**
   * Returns the size of the result set.
   *
   * @return the size of the resultset
   */
  public int size() {
    return resultSet.size();
  }

  /**
   * Returns an {@code Iterator} over the result set items.
   */
  @Override
  public Iterator<QueryResultItem<T>> iterator() {
    return resultSet.iterator();
  }

  /**
   * Returns a {@code Stream} of result set items.
   *
   * @return the result items as a stream
   */
  public Stream<QueryResultItem<T>> stream() {
    return resultSet.stream();
  }

  @Override
  public int hashCode() {
    return Objects.hash(resultSet, token);
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof BatchQueryResult)) {
      return false;
    }
    BatchQueryResult<T> other = (BatchQueryResult<T>) obj;
    return Objects.equals(resultSet, other.resultSet) && Objects.equals(token, other.token);
  }
}
