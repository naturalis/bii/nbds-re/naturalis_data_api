package nl.naturalis.nba.utils;

public class ObjectUtil {

  /**
   * Null-tolerant comparison of two objects.
   *
   * @param obj0  the first object
   * @param obj1  the second object
   * @return 0 if both objects are null, 1 if only the first is null, and -1 if only the second object is null
   */
  public static <T extends Comparable<T>> int compare(T obj0, T obj1) {
    if (obj0 == null) {
      if (obj1 == null) {
        return 0;
      }
      return 1;
    }
    if (obj1 == null) {
      return -1;
    }
    return obj0.compareTo(obj1);
  }

  private ObjectUtil() {}
}
