package nl.naturalis.nba.utils;

import static nl.naturalis.nba.utils.TimeUtil.formatMilliseconds;
import static nl.naturalis.nba.utils.TimeUtil.getDuration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.OffsetDateTime;
import java.util.concurrent.TimeUnit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TimeUtilTest {

  @Test
  public void test() {}

  @Before
  public void setUp() {}

  @After
  public void tearDown() {}

  /**
   * Test method for {@link nl.naturalis.nba.utils.TimeUtil#getDuration(long)}.
   *
   * Test to see if the gerDuration() returns a string in time format 00:00:00
   */
  @Test
  public void testGetDuration() {

    String actual = getDuration(OffsetDateTime.now().toInstant().toEpochMilli());

    Pattern patternFormat = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
    //Pattern validFormat = Pattern.compile("[0-2]|[0-9]:[0-5]|[0-9]:[0-5]|[0-9]");
    Pattern validFormat = Pattern.compile("[0-2]|[0-9]:[0-5]|[0-9]");
    Matcher matcherFormat = patternFormat.matcher(actual);
    Matcher matcherValidFormat = validFormat.matcher(actual);

    assertTrue("01", matcherFormat.find());
    assertTrue("02", matcherValidFormat.find());
  }

  /**
   * Test method for {@link nl.naturalis.nba.utils.TimeUtil#getDuration(long, long)}.
   *
   * Test to verify getDuration method returns a correct time duration format 00:00:00 .
   */
  @Test
  public void testGetDurationLong() {

    String actual = getDuration(
            System.currentTimeMillis() - TimeUnit.HOURS.toMillis(1),
            OffsetDateTime.now().toInstant().toEpochMilli());
    Pattern patternFormat = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
    //Pattern ifValidFormat = Pattern.compile("[0-2]|[0-9]:[0-5]|[0-9]:[0-5]|[0-9]");
    Pattern ifValidFormat = Pattern.compile("[0-2]|[0-9]:[0-5]|[0-9]");
    Matcher matcherFormat = patternFormat.matcher(actual);
    Matcher matcherValidFormat = ifValidFormat.matcher(actual);

    assertTrue("01", matcherFormat.find());
    assertTrue("02", matcherValidFormat.find());
  }

  /**
   * Test method for {@link nl.naturalis.nba.utils.TimeUtil#formatMilliseconds(long)}.
   *
   * Test to verify if some test values are being formatted as expected
   */
  @Test
  public void testFormatMilliseconds() {

    long millis = 184612024L;
    String expected = "2 days, 3 hours, 16 minutes, 52 seconds and 24 milliseconds";
    String actual = formatMilliseconds(millis);
    assertEquals("01", expected, actual);

    millis = 46849540L;
    expected = "13 hours, 49 seconds and 540 milliseconds";
    actual = formatMilliseconds(millis);
    assertEquals("02", expected, actual);

    millis = 0L;
    expected = "0 milliseconds";
    actual = formatMilliseconds(millis);
    assertEquals("03", expected, actual);

    millis = -1001L;
    expected = "1 second and 1 millisecond";
    actual = formatMilliseconds(millis);
    assertEquals("04", expected, actual);
  }

}
