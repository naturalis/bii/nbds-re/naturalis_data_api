package nl.naturalis.nba.utils;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

import org.junit.Test;

/**
 * Test class for ObjectUtilTest.java
 */
@SuppressWarnings("StringOperationCanBeSimplified")
public class ObjectUtilTest {
  /**
   * Test method for
   * {@link nl.naturalis.nba.utils.ObjectUtilTest#compare(java.lang.Comparable, java.lang.Comparable)}.
   *
   * Test to compare two objects
   */
  @Test
  public void testCompare() {

    int check;
    String testString_1 = new String("TestString_01");
    String testString_2 = new String("TestString_01");
    String testString_3 = new String("TestString_02");

    check = ObjectUtil.compare(testString_1, testString_2);
    assertThat(check, is(equalTo(0)));

    check = ObjectUtil.compare(testString_1, testString_3);
    assertThat(check, is(lessThan(0)));

    check = ObjectUtil.compare(testString_3, testString_1);
    assertThat(check, is(greaterThan(0)));

    check = ObjectUtil.compare(null, null);
    assertThat(check, is(equalTo(0)));
  }

}
