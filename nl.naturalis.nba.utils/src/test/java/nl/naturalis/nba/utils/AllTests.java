package nl.naturalis.nba.utils;

import nl.naturalis.nba.utils.reflect.ReflectionUtilTest;
import nl.naturalis.nba.utils.xml.XmlFileUpdaterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    ArrayUtilTest.class,
    ClassUtilTest.class,
    CollectionUtilTest.class,
    ConfigObjectTest.class,
    ExceptionUtilTest.class,
    FileUtilTest.class,
    IOUtilTest.class,
    ObjectUtilTest.class,
    StringUtilTest.class,
    TimeUtilTest.class,
    ReflectionUtilTest.class,
    XmlFileUpdaterTest.class
})

public class AllTests {}
