package nl.naturalis.nba.rest.resource;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;
import io.swagger.annotations.*;
import nl.naturalis.log.appenders.LiveLogStream;
import nl.naturalis.nba.api.InvalidQueryException;
import nl.naturalis.nba.api.NameResolutionQuerySpec;
import nl.naturalis.nba.api.QueryResult;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.api.model.MultiMediaObject;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.dao.MultiMediaObjectDao;
import static java.nio.charset.StandardCharsets.UTF_8;
import static nl.naturalis.common.ObjectMethods.ifNull;
import static nl.naturalis.nba.common.json.JsonUtil.deserialize;
import static nl.naturalis.nba.rest.util.ResourceUtil.*;

@Stateless
@LocalBean
@Api(value = "multimedia")
@Path("/multimedia")
@Produces({"application/json", "application/xml"})
public class MultiMediaObjectResource extends NbaResource<MultiMediaObject, MultiMediaObjectDao> {

  private static final Logger logger = LogManager.getLogger(MultiMediaObjectResource.class);

  MultiMediaObjectResource() {
    super(new MultiMediaObjectDao());
  }

  @Override
  @GET
  @GZIP
  @Path("/download")
  @ApiOperation(
      value =
          "Dynamic download service: Query for multimedia objects and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public Response downloadQueryHttpGet(@Context UriInfo uriInfo) {
    return super.downloadQueryHttpGet(uriInfo);
  }

  @Override
  @GZIP
  @POST
  @Path("/download")
  @ApiOperation(
      value =
          "Dynamic download service: Query for multimedia objects and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public Response downloadQueryHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.downloadQueryHttpPostForm(form, uriInfo);
  }

  @Override
  @GZIP
  @POST
  @Path("/download")
  @ApiOperation(
      value =
          "Dynamic download service: Query for multimedia objects and return result as a stream ...",
      response = Response.class,
      notes = "Query with query parameters or querySpec JSON. ...")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(NDJSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public Response downloadQueryHttpPostJson(
      @ApiParam(value = "querySpec", required = false) QuerySpec qs, @Context UriInfo uriInfo) {
    return super.downloadQueryHttpPostJson(qs, uriInfo);
  }

  @Override
  @GET
  @Path("/find/{id}")
  @ApiOperation(
      value = "Find a multimedia document by id",
      response = MultiMediaObject.class,
      notes = "If found, returns a single multimedia document")
  @Produces(JSON_CONTENT_TYPE)
  public MultiMediaObject find(
      @ApiParam(
              value = "id of multimedia document",
              required = true,
              defaultValue = "L.4169766_1307658521@BRAHMS")
          @PathParam("id")
          String id,
      @Context UriInfo uriInfo) {
    return super.find(id, uriInfo);
  }

  @Override
  @GET
  @Path("/findByIds/{ids}")
  @ApiOperation(
      value = "Find multimedia document by ids",
      response = MultiMediaObject[].class,
      notes = "Given multiple ids, returns a list of multimedia documents")
  @Produces(JSON_CONTENT_TYPE)
  public MultiMediaObject[] findByIds(
      @ApiParam(
              value = "ids of multiple multimedia documents, separated by comma",
              required = true,
              defaultValue = "U.1475914_2059926060@BRAHMS,L.2454256_0837498402@BRAHMS",
              allowMultiple = true)
          @PathParam("ids")
          String ids,
      @Context UriInfo uriInfo) {
    return super.findByIds(ids, uriInfo);
  }

  @Override
  @GET
  @Path("/query")
  @ApiOperation(
      value = "Query for multimedia documents",
      response = QueryResult.class,
      notes = "Search for multimedia documents with query parameters or QuerySpec JSON string")
  @Produces(JSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public QueryResult<MultiMediaObject> queryHttpGet(@Context UriInfo uriInfo) {
    return super.queryHttpGet(uriInfo);
  }

  @Override
  @POST
  @Path("/query")
  @ApiOperation(
      hidden = true,
      value = "Query for multimedia documents",
      response = QueryResult.class,
      notes = "Search for multimedia documents with query parameters or QuerySpec JSON string")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public QueryResult<MultiMediaObject> queryHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.queryHttpPostForm(form, uriInfo);
  }

  @Override
  @POST
  @Path("/query")
  @ApiOperation(
      value = "Query for multimedia documents",
      response = QueryResult.class,
      notes = "Search for multimedia documents with query parameters or QuerySpec JSON string")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public QueryResult<MultiMediaObject> queryHttpPostJson(
      @ApiParam(value = "querySpec", required = false) QuerySpec qs, @Context UriInfo uriInfo) {
    return super.queryHttpPostJson(qs, uriInfo);
  }

  @GET
  @Path("/queryWithNameResolution")
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpGet(
      @QueryParam("_querySpec") String json, @Context UriInfo uriInfo) {
    try {
      NameResolutionQuerySpec qs = deserialize(ifNull(json, "{}"), NameResolutionQuerySpec.class);
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(qs, stream);
        }
      }
      return executeNameResQuery(qs);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/queryWithNameResolution")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpPostForm(
      @FormParam("_querySpec") String json, @Context UriInfo uriInfo) {
    try {
      NameResolutionQuerySpec qs = deserialize(ifNull(json, "{}"), NameResolutionQuerySpec.class);
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(qs, stream);
        }
      }
      return executeNameResQuery(qs);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @POST
  @Path("/queryWithNameResolution")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces({JSON_CONTENT_TYPE, MediaType.TEXT_PLAIN})
  public Response queryWithNameResolutionHttpPostJson(
      @ApiParam(value = "query", required = true) NameResolutionQuerySpec query,
      @Context UriInfo uriInfo) {
    try {
      if (isExplainRequest(uriInfo)) {
        try (LiveLogStream stream = createLiveLogStream(uriInfo)) {
          return explainNameResQuery(query, stream);
        }
      }
      return executeNameResQuery(query);
    } catch (Throwable t) {
      throw handleError(uriInfo, t);
    }
  }

  @Override
  @GET
  @Path("/count")
  @ApiOperation(
      value = "Get the number of multimedia documents matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or QuerySpec JSON")
  @Produces(JSON_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public long countHttpGet(@Context UriInfo uriInfo) {
    return super.countHttpGet(uriInfo);
  }

  @Override
  @POST
  @Path("/count")
  @ApiOperation(
      hidden = true,
      value = "Get the number of multimedia documents matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or QuerySpec JSON")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public long countHttpPostForm(
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.countHttpPostForm(form, uriInfo);
  }

  @Override
  @POST
  @Path("/count")
  @ApiOperation(
      value = "Get the number of multimedia documents matching a given condition",
      response = long.class,
      notes = "Conditions given as query parameters or QuerySpec JSON")
  @Produces(JSON_CONTENT_TYPE)
  @Consumes(JSON_CONTENT_TYPE)
  public long countHttpPostJson(
      @ApiParam(value = "querySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    return super.countHttpPostJson(qs, uriInfo);
  }

  @Override
  @GET
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Produces(TEXT_CONTENT_TYPE)
  @ApiImplicitParams({
    @ApiImplicitParam(
        name = "querySpec",
        paramType = "query",
        dataType = "QuerySpec",
        dataTypeClass = QuerySpec.class,
        value = "Object of type QuerySpec or its JSON representation")
  })
  public long countDistinctValuesHttpGet(
      @ApiParam(
              value = "Name of field in multimedia object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpGet(field, uriInfo);
  }

  @POST
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(TEXT_CONTENT_TYPE)
  public long countDistinctValuesHttpPost(
      @ApiParam(
              value = "Name of field in the multimedia object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "Query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpPostForm(field, form, uriInfo);
  }

  @POST
  @Path("/countDistinctValues/{field}")
  @ApiOperation(
      value = "Count the distinct number of values that exist for a given field",
      response = long.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(TEXT_CONTENT_TYPE)
  public long countDistinctValuesHttpJson(
      @ApiParam(
              value = "Name of field in the multimedia object",
              required = true,
              defaultValue = "identifications.defaultClassification.family")
          @PathParam("field")
          String field,
      @ApiParam(value = "QuerySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValues/" + field);
    return super.countDistinctValuesHttpPostJson(field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = Map.class,
      notes = "")
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpGet(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpGet(group, field, uriInfo);
  }

  @Override
  @POST
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = Map.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpPostForm(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpPostForm(group, field, form, uriInfo);
  }

  @Override
  @POST
  @Path("/countDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Count the distinct number of field values that exist per the given field to group by",
      response = Map.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> countDistinctValuesPerGroupHttpPostJson(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "collectionType")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "identifications.typeStatus")
          @PathParam("field")
          String field,
      @ApiParam(value = "querySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("countDistinctValuesPerGroup/" + group + "/" + field);
    return super.countDistinctValuesPerGroupHttpPostJson(group, field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that can be found for one field",
      response = Map.class,
      notes =
          "A list of all fields for multimedia documents can be retrieved with /metadata/getFieldInfo")
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpGet(
      @ApiParam(value = "field", required = true, defaultValue = "gatheringEvents.worldRegion")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpGet(field, uriInfo);
  }

  @Override
  @POST
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that exist for a field",
      response = Map.class,
      notes =
          "A list of all fields for multimedia documents can be retrieved with /metadata/getFieldInfo")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpPostForm(
      @ApiParam(
              value = "name of field in a multimedia object",
              required = true,
              defaultValue = "gatheringEvents.worldRegion")
          @PathParam("field")
          String field,
      @ApiParam(value = "POST payload", required = false) MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpPostForm(field, form, uriInfo);
  }

  @Override
  @POST
  @Path("/getDistinctValues/{field}")
  @ApiOperation(
      value = "Get all different values that exist for a field",
      response = Map.class,
      notes =
          "A list of all fields for multimedia documents can be retrieved with /metadata/getFieldInfo")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public Map<String, Long> getDistinctValuesHttpPostJson(
      @ApiParam(
              value = "name of field in a multimedia object",
              required = true,
              defaultValue = "gatheringEvents.worldRegion")
          @PathParam("field")
          String field,
      @ApiParam(value = "querySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    return super.getDistinctValuesHttpPostJson(field, qs, uriInfo);
  }

  @Override
  @GET
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroupHttpGet(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "identifications.scientificName.genusOrMonomial")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "collectionType")
          @PathParam("field")
          String field,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpGet(group, field, uriInfo);
  }

  @POST
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroupHttpPostForm(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "identifications.scientificName.genusOrMonomial")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "collectionType")
          @PathParam("field")
          String field,
      @ApiParam(value = "query object in POST form", required = false)
          MultivaluedMap<String, String> form,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpPost(group, field, form, uriInfo);
  }

  @POST
  @Path("/getDistinctValuesPerGroup/{group}/{field}")
  @ApiOperation(
      value =
          "Get all distinct values (and their document count) for the field given divided per distinct value of the field to group by",
      response = List.class,
      notes = "")
  @Consumes(JSON_CONTENT_TYPE)
  @Produces(JSON_CONTENT_TYPE)
  public List<Map<String, Object>> getDistinctValuesPerGroup(
      @ApiParam(
              value = "name of field in the multimedia object you want to group by",
              required = true,
              defaultValue = "identifications.scientificName.genusOrMonomial")
          @PathParam("group")
          String group,
      @ApiParam(
              value = "name of field in the multimedia object",
              required = true,
              defaultValue = "collectionType")
          @PathParam("field")
          String field,
      @ApiParam(value = "querySpec JSON", required = false) QuerySpec qs,
      @Context UriInfo uriInfo) {
    logger.info("getDistinctValuesPerGroup/" + group + "/" + field);
    return super.getDistinctValuesPerGroupHttpJson(group, field, qs, uriInfo);
  }

  private Response executeNameResQuery(NameResolutionQuerySpec qs) throws InvalidQueryException {
    return Response.status(Status.OK)
        .type(JSON_CONTENT_TYPE)
        .entity(dao.queryWithNameResolution(qs))
        .build();
  }

  private Response explainNameResQuery(NameResolutionQuerySpec qs, LiveLogStream stream)
      throws InvalidQueryException, IOException {
    QueryResult<MultiMediaObject> res = dao.queryWithNameResolution(qs);
    String divider = "\n\n----------------------- END OF LOG -----------------------\n\n";
    stream.getOutputStream().write(divider.getBytes(UTF_8));
    JsonUtil.toPrettyJson(stream.getOutputStream(), res);
    return Response.status(Status.OK)
        .type(MediaType.TEXT_PLAIN_TYPE)
        .entity(stream.getOutputStream())
        .build();
  }
}
