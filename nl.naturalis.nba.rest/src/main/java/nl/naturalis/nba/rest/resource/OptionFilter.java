package nl.naturalis.nba.rest.resource;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * A filter that catches all OPTIONS calls before path matching.
 *
 */
@Provider
@PreMatching
public class OptionFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (requestContext.getMethod().contentEquals("OPTIONS")) {
            requestContext.abortWith(Response.status(Response.Status.NO_CONTENT).build());
        }
    }
}
