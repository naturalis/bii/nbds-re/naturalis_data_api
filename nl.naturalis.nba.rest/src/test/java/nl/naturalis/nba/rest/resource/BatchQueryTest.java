package nl.naturalis.nba.rest.resource;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Ignore;
import org.junit.Test;
import nl.naturalis.nba.api.Path;
import nl.naturalis.nba.api.QueryCondition;
import nl.naturalis.nba.api.QuerySpec;
import nl.naturalis.nba.common.json.JsonUtil;
import nl.naturalis.nba.utils.http.SimpleHttpGet;
import nl.naturalis.nba.utils.http.SimpleHttpPost;

@Ignore
public class BatchQueryTest {

  @Test
  public void test01() {
    SimpleHttpPost post = new SimpleHttpPost();
    post.setBaseUrl("http://localhost:8080/v2");
    post.setPath("/multimedia/batchQuery");
    QuerySpec qs = new QuerySpec();
    qs.setFields(Arrays.asList(new Path("unitID"), new Path("gatheringEvents.country")));
    QueryCondition qc = new QueryCondition("sourceSystem.code", "=", "BRAHMS");
    qs.addCondition(qc);
    qs.setSize(10);
    post.addFormParam("_querySpec", JsonUtil.toJson(qs));
    Map<String, Object> res = JsonUtil.deserialize(post.execute().getResponseBody());
    System.out.println(JsonUtil.toPrettyJson(res));
  }

  @Test
  public void test02() throws ClientProtocolException, IOException {
    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost post =
        new HttpPost(
            "http://localhost:8080/v2/multimedia/batchQuery?_token=Y2PgYWBgYGQuzi8tSk4NriwuSX2bnJ%2FyFImfqwcSYGBkYHYKcvTwvczAJMZsZGBquRMA");
    post.addHeader("Content-Type", "application/x-www-form-urlencoded");
    CloseableHttpResponse res = client.execute(post);
    Map<String, Object> map = JsonUtil.deserialize(res.getEntity().getContent());
    System.out.println(JsonUtil.toPrettyJson(map));
  }

  @Test
  public void test03() {
    SimpleHttpGet get = new SimpleHttpGet();
    get.setBaseUrl("http://localhost:8080/v2");
    get.setPath("/multimedia/batchQuery");
    get.addQueryParam(
        "_token",
        "Y+dlKs3LLPE8AiF5mdMTSzJSizLz0l3LUvNKPifnl+aVFP1EFS3WgwrzMDAwMDIX55cWJacGVxaXpL5Nzk95isTP1QMJMDAyMDsFOXr4XmZgEmN29HXRMzQwMDA0jTc0MTU1sDAyNt4MAA==");
    Map<String, Object> res = JsonUtil.deserialize(get.execute().getResponseBody());
    System.out.println(JsonUtil.toPrettyJson(res));
  }
}
