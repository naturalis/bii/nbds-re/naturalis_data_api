# README

This is the source code for the Netherlands Biodoversity API (NBA). The
NBA lets you access specimen and species data from Naturalis and affiliated
institutions using the REST protocol.

User manual: [http://docs.biodiversitydata.nl/en/latest/](http://docs.biodiversitydata.nl/en/latest/)

Javadocs: [http://naturalis.github.io/naturalis_data_api/javadoc/v2/all](http://naturalis.github.io/naturalis_data_api/javadoc/v2/all)


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`